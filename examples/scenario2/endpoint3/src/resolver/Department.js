function workingAt(parent, _, context) {
    return context.departmentRepository.getEmployeesByDepartmentId(parent.id).then((employees) => {
        return employees.map((employee) => transformEmployeeToPartner(employee, context))
    }).catch((err) => {
        console.log(`Error occurred while the employees working at department ${parent.id}!`);
        console.log(JSON.stringify(err))
    })
}

function manager(parent, _, context) {
    return context.employeeRepository.getById(parent.manager).then((employee) => {
        return transformEmployeeToPartner(employee, context)
    }).catch((err) => {
        console.log(`Error occurred while fetching the manager of department ${parent.id}!`);
        console.log(JSON.stringify(err))
    })
}

function transformEmployeeToPartner(employee,context) {
    return {
        __typename : "Partner",
        id : employee.id,
        firstname: employee.firstname,
        lastname: employee.lastname,
        hiredAt: employee.hiredAt,
        phone: employee.phone,
        worksAt : function () {
            return context.departmentRepository.getById(employee.department).then((department) => {
                return department
            }).catch((err) => {
                console.log(`Error occurred while fetching the department from employee ${parent.id}!`);
                console.log(JSON.stringify(err))
            })
        }
    }
}

module.exports = {
    workingAt,
    manager
};