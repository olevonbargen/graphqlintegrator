package de.fhdw.hfp418ie.vo.graphql.expression;

public interface ExpressionVisitor {

    boolean handleConstant(final Constant constant);

    boolean handleVariable(final VariableExpression variable);

    boolean handleEquality(final EqualityExpression equality);

    boolean handleConjunction(final ConjunctionExpression conjunction);

    boolean handleDisjunction(final DisjunctionExpression disjunction);

    boolean handleConcatenation(final ConcatenationExpression concatenation);

}
