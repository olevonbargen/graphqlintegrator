package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.Map;

public class FloatConstant extends Constant implements FloatExpression {

    public FloatConstant() {
        this.setValue(0);
    }

    public FloatConstant(final float value) {
        this.setValue(value);
    }

    private float value;

    @Override
    public Float evaluate(Map<VariableExpression, Object> variables) {
        return this.getValue();
    }

    public float getValue() {
        return this.value;
    }

    public void setValue(final float value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FloatConstant that = (FloatConstant) o;
        return Float.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return "FloatConstant{" +
                "value=" + value +
                '}';
    }
}
