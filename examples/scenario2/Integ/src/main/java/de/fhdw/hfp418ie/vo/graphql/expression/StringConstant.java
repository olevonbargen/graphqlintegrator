package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.Map;

public class StringConstant extends Constant implements StringExpression {

    public StringConstant() {
        this.setValue(null);
    }

    public StringConstant(final String value) {
        this.setValue(value);
    }

    private String value;

    @Override
    public String evaluate(Map<VariableExpression, Object> variables) {
        return this.getValue();
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringConstant that = (StringConstant) o;
        return Objects.equal(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return "StringConstant{" +
                "value='" + value + '\'' +
                '}';
    }
}
