package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.Map;

public class DisjunctionExpression extends BooleanCompositeExpression {

    public DisjunctionExpression() {
        super();
    }

    @Override
    public Boolean evaluate(Map<VariableExpression, Object> variables) {
        boolean result = false;
        boolean skipped = false;
        outer: for (BooleanExpression expression : this.getExpressions()) {
            for (VariableExpression variable : expression.involvedVariables()) {
                if (!variables.containsKey(variable)) {
                    skipped = true;
                    continue outer;
                }
            }
            result = expression.evaluate(variables);
            if (result) {
                break;
            }
        }
        if (!result && skipped) {
            throw new Error("Could not evaluate " + this + " completely!");
        }
        return result;
    }

    @Override
    public boolean accept(ExpressionVisitor visitor) {
        return visitor.handleDisjunction(this);
    }

    @Override
    public String toString() {
        return "DisjunctionExpression{" +
                "expressions=" + this.getExpressions() +
                '}';
    }
}
