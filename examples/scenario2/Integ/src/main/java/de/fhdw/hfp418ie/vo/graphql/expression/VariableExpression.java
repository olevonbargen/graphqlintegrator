package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;

public class VariableExpression implements Expression {

    public VariableExpression() {
        this.setModelName(null);
        this.setTypeName(null);
        this.setFieldName(null);
    }

    public VariableExpression(final String modelName, final String typeName, final String fieldName) {
        this.setModelName(modelName);
        this.setTypeName(typeName);
        this.setFieldName(fieldName);
    }

    private String modelName;
    private String typeName;
    private String fieldName;

    @Override
    public Object evaluate(Map<VariableExpression, Object> variables) {
        return variables.get(this);
    }

    @Override
    public Collection<VariableExpression> involvedVariables() {
        final Collection<VariableExpression> result = new ArrayList<>();
        result.add(this);
        return result;
    }

    @Override
    public boolean accept(ExpressionVisitor visitor) {
        return visitor.handleVariable(this);
    }

    public String getModelName() {
        return this.modelName;
    }

    public void setModelName(final String modelName) {
        this.modelName = modelName;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(final String typeName) {
        this.typeName = typeName;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VariableExpression that = (VariableExpression) o;
        return Objects.equal(getModelName(), that.getModelName()) &&
                Objects.equal(getTypeName(), that.getTypeName()) &&
                Objects.equal(getFieldName(), that.getFieldName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getModelName(), getTypeName(), getFieldName());
    }
}
