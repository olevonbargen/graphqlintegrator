package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.Map;

public class IntegerConstant extends Constant implements IntegerExpression {

    public IntegerConstant() {
        this.setValue(0);
    }

    public IntegerConstant(final int value) {
        this.setValue(value);
    }

    private int value;

    @Override
    public Integer evaluate(Map<VariableExpression, Object> variables) {
        return this.getValue();
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(final int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerConstant that = (IntegerConstant) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return "IntegerConstant{" +
                "value=" + value +
                '}';
    }
}
