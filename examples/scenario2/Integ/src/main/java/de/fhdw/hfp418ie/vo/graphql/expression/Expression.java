package de.fhdw.hfp418ie.vo.graphql.expression;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Collection;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
public interface Expression {

    Object evaluate(Map<VariableExpression, Object> variables);

    Collection<VariableExpression> involvedVariables();

    boolean accept(final ExpressionVisitor visitor);

}
