package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.Map;

public interface IntegerExpression extends Expression {

    @Override
    Integer evaluate(Map<VariableExpression, Object> variables);
}
