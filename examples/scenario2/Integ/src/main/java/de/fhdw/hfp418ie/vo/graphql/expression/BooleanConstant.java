package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.Map;

public class BooleanConstant extends Constant implements BooleanExpression {

    public BooleanConstant() {
        this.setValue(false);
    }

    public BooleanConstant(final boolean value) {
        this.setValue(value);
    }

    private boolean value;

    @Override
    public Boolean evaluate(Map<VariableExpression, Object> variables) {
        return this.isValue();
    }

    public boolean isValue() {
        return this.value;
    }

    public void setValue(final boolean value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BooleanConstant that = (BooleanConstant) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return "BooleanConstant{" +
                "value=" + value +
                '}';
    }
}
