package com.example.graphql.model;

import com.example.graphql.model.output.*;
import com.example.graphql.util.GraphQLCaller;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;

public class Partner {

    public static final OutputGlobalType globaleType = new OutputGlobalType("Partner", null);
    public static PartnerMerger merger = null;

    public Partner() {
    }

    public Partner(final JsonNode input, final OutputType outputType, final String endpoint) {
        final Iterator<String> fieldNames = input.fieldNames();
        while (fieldNames.hasNext()) {
            final String currentFieldName = fieldNames.next();
            switch (currentFieldName) {
                case "hiredAt":
                    this.setHiredAt(input.get(currentFieldName).asText());
                    break;
                case "phone":
                    this.setPhone(input.get(currentFieldName).asText());
                    break;
                case "lastname":
                    this.setLastname(input.get(currentFieldName).asText());
                    break;
                case "invoices":
                    this.setInvoices(new ArrayList<>());
                    if (input.get(currentFieldName).isArray()) {
                        input.get(currentFieldName).forEach(element -> this.getInvoices().add(new Invoice(element, GraphQLCaller.INSTANCE.getResultType(endpoint, "Partner", "invoices"), endpoint)));
                    } else {
                        this.getInvoices().add(new Invoice(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Partner", "invoices"), endpoint));
                    }
                    break;
                case "name":
                    this.setName(input.get(currentFieldName).asText());
                    break;
                case "worksAt":
                    this.setWorksAt(new Department(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Partner", "worksAt"), endpoint));
                    break;
                case "id":
                    this.setId(new ArrayList<>());
                    if (input.get(currentFieldName).isArray()) {
                        input.get(currentFieldName).forEach(element -> this.getId().add(element.asLong()));
                    } else {
                        this.getId().add(input.get(currentFieldName).asLong());
                    }
                    break;
                case "purchases":
                    this.setPurchases(new ArrayList<>());
                    if (input.get(currentFieldName).isArray()) {
                        input.get(currentFieldName).forEach(element -> this.getPurchases().add(new Purchase(element, GraphQLCaller.INSTANCE.getResultType(endpoint, "Partner", "purchases"), endpoint)));
                    } else {
                        this.getPurchases().add(new Purchase(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Partner", "purchases"), endpoint));
                    }
                    break;
                case "email":
                    this.setEmail(input.get(currentFieldName).asText());
                    break;
                case "firstname":
                    this.setFirstname(input.get(currentFieldName).asText());
                    break;
                default:
                    throw new Error("Cannot convert field " + currentFieldName + " within the type Partner");
            }
        }
        globaleType.getTypes().put(endpoint, outputType);
        if (outputType.getEqualityExpression() != null) {
            globaleType.setExpression(outputType.getEqualityExpression());
            merger = new PartnerMerger(outputType.getEqualityExpression());
        }
        this.setEndpoint(endpoint);
    }

    private String hiredAt;

    private String phone;

    private String lastname;

    private Collection<Invoice> invoices;

    private String name;

    private Department worksAt;

    private Collection<Long> id;

    private Collection<Purchase> purchases;

    private String email;

    private String firstname;

    private String endpoint;

    public String getHiredAt() {
        return this.hiredAt;
    }

    public void setHiredAt(final String hiredAt) {
        this.hiredAt = hiredAt;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    public Collection<Invoice> getInvoices() {
        return this.invoices;
    }

    public void setInvoices(final Collection<Invoice> invoices) {
        this.invoices = invoices;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Department getWorksAt() {
        return this.worksAt;
    }

    public void setWorksAt(final Department worksAt) {
        this.worksAt = worksAt;
    }

    public Collection<Long> getId() {
        return this.id;
    }

    public void setId(final Collection<Long> id) {
        this.id = id;
    }

    public Collection<Purchase> getPurchases() {
        return this.purchases;
    }

    public void setPurchases(final Collection<Purchase> purchases) {
        this.purchases = purchases;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(final String endpoint) {
        this.endpoint = endpoint;
    }

}
