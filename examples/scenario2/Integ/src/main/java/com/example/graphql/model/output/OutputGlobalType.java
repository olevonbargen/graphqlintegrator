package com.example.graphql.model.output;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;

import java.util.HashMap;
import java.util.Map;

public class OutputGlobalType extends OutputEntity {

    public OutputGlobalType() {
        super();
        this.types = new HashMap<>();
        this.setExpression(null);
    }

    public OutputGlobalType(final String name, final EqualityExpression equalityExpression) {
        super(name);
        this.types = new HashMap<>();
        this.setExpression(equalityExpression);
    }

    public OutputGlobalType(final String name, final Map<String, OutputType> types, final EqualityExpression equalityExpression) {
        super(name);
        this.types = types;
        this.setExpression(equalityExpression);
    }

    private final Map<String, OutputType> types;
    private EqualityExpression expression;

    public Map<String, OutputType> getTypes() {
        return types;
    }

    public EqualityExpression getExpression() {
        return expression;
    }

    public void setExpression(EqualityExpression expression) {
        this.expression = expression;
    }
}
