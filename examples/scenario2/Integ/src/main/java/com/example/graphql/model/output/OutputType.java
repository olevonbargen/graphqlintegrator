package com.example.graphql.model.output;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;

import java.util.ArrayList;
import java.util.Collection;

public class OutputType extends OutputEntity {

    private final Collection<OutputField> fields;
    private EqualityExpression equalityExpression;

    public OutputType() {
        this.fields = new ArrayList<>();
    }

    OutputType(String name, EqualityExpression equalityExpression) {
            super(name);
            this.fields = new ArrayList<>();
            this.setEqualityExpression(equalityExpression);
        }

    public OutputField addField(String name, String originalName, String resultTypeName) {
        final OutputField result = new OutputField(name, originalName, resultTypeName);
        this.getFields().add(result);
        return result;
    }

    public Collection<OutputField> getFields() {
        return fields;
    }

    public EqualityExpression getEqualityExpression() {
        return this.equalityExpression;
    }

    public void setEqualityExpression(final EqualityExpression equalityExpression) {
        this.equalityExpression = equalityExpression;
    }
}
