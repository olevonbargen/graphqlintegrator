package com.example.graphql.model;

import com.example.graphql.model.output.*;
import com.example.graphql.util.GraphQLCaller;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Iterator;


public class Purchase {

    public static final OutputGlobalType globaleType = new OutputGlobalType("Purchase", null);
    public static PurchaseMerger merger = null;

    public Purchase() {
    }

    public Purchase(final JsonNode input, final OutputType outputType, final String endpoint) {
final Iterator<String> fieldNames = input.fieldNames();
while (fieldNames.hasNext()) {
final String currentFieldName = fieldNames.next();
switch (currentFieldName) {
case "date":
this.setDate(input.get(currentFieldName).asText());
break;
case "customer":
this.setCustomer(new Partner(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Purchase", "customer"), endpoint));
break;
case "store":
this.setStore(new Store(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Purchase", "store"), endpoint));
break;
case "id":
this.setId(input.get(currentFieldName).asLong());
break;
default: 
throw new Error("Cannot convert field " + currentFieldName + " within the type Purchase");
}
}
globaleType.getTypes().put(endpoint, outputType);
       if (outputType.getEqualityExpression() != null) {
            globaleType.setExpression(outputType.getEqualityExpression());
            merger = new PurchaseMerger(outputType.getEqualityExpression());
        }
        this.setEndpoint(endpoint);
}

    private String date;

private Partner customer;

private Store store;

private Long id;

private String endpoint;
public String getDate() {
    return this.date;
}

public void setDate(final String date) {
    this.date = date;
}

public Partner getCustomer() {
    return this.customer;
}

public void setCustomer(final Partner customer) {
    this.customer = customer;
}

public Store getStore() {
    return this.store;
}

public void setStore(final Store store) {
    this.store = store;
}

public Long getId() {
    return this.id;
}

public void setId(final Long id) {
    this.id = id;
}

public String getEndpoint() {
    return this.endpoint;
}

public void setEndpoint(final String endpoint) {
    this.endpoint = endpoint;
}

}
