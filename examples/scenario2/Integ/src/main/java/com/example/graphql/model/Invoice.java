package com.example.graphql.model;

import com.example.graphql.model.output.*;
import com.example.graphql.util.GraphQLCaller;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Iterator;


public class Invoice {

    public static final OutputGlobalType globaleType = new OutputGlobalType("Invoice", null);
    public static InvoiceMerger merger = null;

    public Invoice() {
    }

    public Invoice(final JsonNode input, final OutputType outputType, final String endpoint) {
final Iterator<String> fieldNames = input.fieldNames();
while (fieldNames.hasNext()) {
final String currentFieldName = fieldNames.next();
switch (currentFieldName) {
case "createdAt":
this.setCreatedAt(input.get(currentFieldName).asText());
break;
case "payedAt":
this.setPayedAt(input.get(currentFieldName).asText());
break;
case "total":
this.setTotal(input.get(currentFieldName).asInt());
break;
case "client":
this.setClient(new Partner(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Invoice", "client"), endpoint));
break;
case "dueAt":
this.setDueAt(input.get(currentFieldName).asText());
break;
case "id":
this.setId(input.get(currentFieldName).asLong());
break;
default: 
throw new Error("Cannot convert field " + currentFieldName + " within the type Invoice");
}
}
globaleType.getTypes().put(endpoint, outputType);
       if (outputType.getEqualityExpression() != null) {
            globaleType.setExpression(outputType.getEqualityExpression());
            merger = new InvoiceMerger(outputType.getEqualityExpression());
        }
        this.setEndpoint(endpoint);
}

    private String createdAt;

private String payedAt;

private Integer total;

private Partner client;

private String dueAt;

private Long id;

private String endpoint;
public String getCreatedAt() {
    return this.createdAt;
}

public void setCreatedAt(final String createdAt) {
    this.createdAt = createdAt;
}

public String getPayedAt() {
    return this.payedAt;
}

public void setPayedAt(final String payedAt) {
    this.payedAt = payedAt;
}

public Integer getTotal() {
    return this.total;
}

public void setTotal(final Integer total) {
    this.total = total;
}

public Partner getClient() {
    return this.client;
}

public void setClient(final Partner client) {
    this.client = client;
}

public String getDueAt() {
    return this.dueAt;
}

public void setDueAt(final String dueAt) {
    this.dueAt = dueAt;
}

public Long getId() {
    return this.id;
}

public void setId(final Long id) {
    this.id = id;
}

public String getEndpoint() {
    return this.endpoint;
}

public void setEndpoint(final String endpoint) {
    this.endpoint = endpoint;
}

}
