package com.example.graphql.util;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import de.fhdw.hfp418ie.vo.graphql.expression.VariableExpression;

import com.example.graphql.model.output.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class GraphQLCallerImpl implements GraphQLCaller {

    private final Collection<OutputModel> models;
    private static final String modelPath = "config/models.json";

    private HttpURLConnection connection;
    static final String containerJsonFieldName = "data";

    private static final String queryStart = "{ ";
    private static final String queryEnd = System.lineSeparator() + "}";
    private static final String argumentsStart = "(";
    private static final String argumentsEnd = ")";
    private static final String argumentsSeparator = ", ";
    private static final String argumentValueStartSign = ": ";

    GraphQLCallerImpl() {
        URL path = GraphQLCallerImpl.class.getClassLoader().getResource(modelPath);
        if (path == null) {
            throw new Error("Could not find model configuration at " + modelPath);
        }
        File file = new File(path.getFile());
        ObjectMapper mapper = new ObjectMapper();
        final Collection<OutputModel> models = new ArrayList<>();
        try {
            JsonNode modelsJson = mapper.readTree(file);
            modelsJson.elements().forEachRemaining(node -> {
                try {
                    models.add(mapper.readValue(node.toString(), OutputModel.class));
                } catch (IOException e) {
                    throw new Error("Could not load model configuration at " + path.toString(), e);
                }
            });
        } catch (IOException e) {
            throw new Error("Could not load model configuration at " + path.toString(), e);
        }
        this.models = models;
    }

    private JsonNode normalizeResult(OutputModel model, OutputType owner, JsonNode response, Set<String> selectionSetArguments) {
        JsonNode result;
        final ObjectMapper mapper = new ObjectMapper();
        if (response.isArray()) {
            ArrayNode arrayNode = mapper.createArrayNode();
            response.forEach(element -> arrayNode.add(this.normalizeResult(model, owner, element, selectionSetArguments)));
            result = arrayNode;
        } else if (response.isObject()) {
            final ObjectNode objectNode = mapper.createObjectNode();
            final Iterator<String> fieldNames = response.fieldNames();
            while (fieldNames.hasNext()) {
                final String fieldName = fieldNames.next();
                final Optional<OutputField> searchField = owner.getFields().stream().filter(field -> field.getOriginalName().equals(fieldName)).findAny();
                if (!searchField.isPresent()) {
                    throw new Error("Could not retrieve field with name " + fieldName + " within type " + owner.getName());
                }
                final OutputField currentField = searchField.get();
                final Optional<OutputType> searchTarget = model.getTypes().stream().filter(type -> type.getName().equals(currentField.getResultTypeName())).findAny();
                if (!searchTarget.isPresent()) {
                    throw new Error("Could not retrieve type of field with name " + fieldName + " from type " + owner.getName());
                }
                final OutputType target = searchTarget.get();
                Set<String> nextLevel = new HashSet<>();
                for (String current : selectionSetArguments) {
                    if (current.startsWith(currentField.getName() + "/")) {
                        nextLevel.add(current.substring(current.indexOf("/" + 1)));
                    }
                }
                objectNode.set(currentField.getName(), this.normalizeResult(model, target, response.get(currentField.getOriginalName()), nextLevel));
            }
            result = objectNode;
        } else {
            result = response;
        }
        return result;
    }

    @Override
    public JsonNode normalizeResults(String endpoint, String type, JsonNode response, Set<String> selectionSetArguments) {
        Optional<OutputModel> searchModel = this.models.stream().filter(model -> model.getEndpoint().equals(endpoint)).findAny();
        if (!searchModel.isPresent()) {
            throw new Error("Could not retrieve model for endpoint " + endpoint);
        }
        OutputModel model = searchModel.get();
        Optional<OutputType> searchType = model.getTypes().stream().filter(currentType -> currentType.getName().equals(type)).findAny();
        if (!searchType.isPresent()) {
            throw new Error("Could not retrieve type with name " + type + " within model " + endpoint);
        }
        return this.normalizeResult(model, searchType.get(), response, selectionSetArguments);
    }

    @Override
    public JsonNode executeQuery(final String endpoint, final String query) throws IOException {
        this.setupConnection(new URL(endpoint));
        this.getConnection().connect();
        BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(this.getConnection().getOutputStream()));
        outputWriter.write(JsonNodeFactory.instance.objectNode().put("query", query).toString());
        outputWriter.flush();
        outputWriter.close();
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(this.getConnection().getInputStream()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = inputReader.readLine()) != null) {
            result.append(line);
            result.append(System.lineSeparator());
        }
        inputReader.close();
        this.getConnection().disconnect();
        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode root = mapper.readTree(result.toString());
        return root.get(GraphQLCallerImpl.containerJsonFieldName);
    }

    private Set<String> translate(final OutputModel model, final OutputField field, final Set<String> selectionSetArguments) {
            Set<String> result = new HashSet<>();
            Set<String> selectedFields = new HashSet<>();
            selectedFields.addAll(selectionSetArguments);
            Optional<OutputType> searchResultType = model.getTypes().stream().filter(type -> type.getName().equals(field.getResultTypeName())).findAny();
            if (!searchResultType.isPresent()) {
                throw new Error("Could not retrieve result type for field " + field.getName());
            }
            final OutputType resultType = searchResultType.get();
            for (String current : selectedFields) {
                Optional<OutputField> resultField = resultType.getFields().stream().filter(resultTypeFields -> resultTypeFields.getName().equals(current)).findAny();
                if (resultField.isPresent()) {
                    String resultFieldName = resultField.get().getName();
                    String targetFieldName = resultField.get().getOriginalName();
                    result.add(targetFieldName);
                    Set<String> nextLevel = new HashSet<>();
                    for (String fieldName : selectedFields) {
                        if (fieldName.startsWith(resultFieldName + "/")) {
                            nextLevel.add(fieldName.substring(fieldName.indexOf("/") + 1));
                        }
                    }
                    result.addAll(this.translate(model, resultField.get(), nextLevel).stream().map(str -> targetFieldName + "/" + str).collect(Collectors.toList()));
                }
            }
            if (resultType.getEqualityExpression() != null) {
                Collection<VariableExpression> variables = resultType.getEqualityExpression().involvedVariables();
                for (VariableExpression variable : variables) {
                    if (variable.getModelName().equals(model.getName())) {
                        if (!result.contains(variable.getFieldName())) {
                            result.add(variable.getFieldName());
                        }
                    }
                }
            }
            return result;
        }

    @Override
    public Map<String, String> generateQueries(final String rootType, final String fieldName, final Map<String, Object> arguments, final Set<String> selectionSetArguments) {
        Map<OutputField, Set<String>> sets = new HashMap<>();
        Collection<OutputType> types = this.models.stream().flatMap(model -> model.getTypes().stream()).filter(type -> type.getName().equals(rootType)).collect(Collectors.toList());
        Collection<OutputField> fields = types.stream().flatMap(type -> type.getFields().stream()).filter(field -> field.getName().equals(fieldName)).collect(Collectors.toList());
        for (OutputField field : fields) {
            Optional<OutputModel> searchModel = this.models.stream().filter(model -> model.getTypes().stream().anyMatch(type -> type.getFields().contains(field))).findAny();
            if (!searchModel.isPresent()) {
                throw new Error("Could not retrieve model for field " + field.getName());
            }
            OutputModel model = searchModel.get();
            sets.put(field, this.translate(model, field, selectionSetArguments));
        }
        Map<String, String> result = new HashMap<>();
        for (OutputField field : sets.keySet()) {
            Optional<OutputModel> searchModel = this.models.stream().filter(model -> model.getTypes().stream()
                    .anyMatch(type -> type.getFields().contains(field))).findAny();
            if (!searchModel.isPresent()) {
                throw new Error("Could not find output model for the field " + field.getName());
            }
            OutputModel model = searchModel.get();
            String query = rootType.toLowerCase() +
                    queryStart +
                    System.lineSeparator() +
                    this.generateQuery(field.getOriginalName(), sets.get(field), field,
                            arguments) +
                    queryEnd;
            result.put(model.getEndpoint(), query);
        }
        return result;
    }

    private String generateQuery(final String baseType, final Set<String> selectionSetArguments,
                                 final OutputField field, final Map<String, Object> arguments) {
        StringBuilder result = new StringBuilder();
        result.append(baseType);
        if (!arguments.isEmpty()) {
            result.append(argumentsStart);
        }
        boolean first = true;
        for (String key : arguments.keySet()) {
            if (!first) {
                result.append(argumentsSeparator);
            }
            first = false;
            Optional<OutputArgument> argument = field.getArguments().stream()
                    .filter(arg -> arg.getName().equals(key)).findAny();
            if (!argument.isPresent()) {
                throw new Error("Could not resolve field with name " + key + " within field " + field.getName());
            }
            result.append(argument.get().getOriginalName());
            result.append(argumentValueStartSign);
            if (argument.get().isString()) {
                result.append("\"");
            }
            result.append(arguments.get(key).toString());
            if (argument.get().isString()) {
                result.append("\"");
            }
        }
        if (!arguments.isEmpty()) {
            result.append(argumentsEnd);
        }
        if (!selectionSetArguments.isEmpty()) {
            result.append(queryStart);
            for (String current : selectionSetArguments) {
                if (current.contains("/"))
                    continue;
                result.append(System.lineSeparator());
                Set<String> children = selectionSetArguments.stream()
                        .filter(selection -> selection.startsWith(current + "/"))
                        .map(child -> child.substring(child.indexOf("/") + 1)).collect(Collectors.toSet());
                if (children.size() > 0) {
                    result.append(this.generateQuery(current, children, field, new HashMap<>()));
                } else {
                    result.append(current);
                }
            }
            result.append(queryEnd);
        }
        return result.toString();
    }

    @Override
    public OutputType getResultType(String endpoint, String rootType, String fieldName) {
        for (OutputModel model : this.models) {
            if (!model.getEndpoint().equals(endpoint))
                continue;
            for (OutputType type : model.getTypes()) {
                if (!type.getName().equals(rootType))
                    continue;
                for (OutputField field : type.getFields()) {
                    if (!field.getName().equals(fieldName))
                        continue;
                    for (OutputType resultType : model.getTypes()) {
                        if (resultType.getName().equals(field.getResultTypeName()))
                            return resultType;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getModelName(final String endpoint) {
        for (OutputModel model : this.models) {
            if (model.getEndpoint().equals(endpoint))
                return model.getName();
        }
        return null;
    }

    private void setupConnection(final URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        this.setConnection(connection);
    }

    private void setConnection(final HttpURLConnection connection) {
        this.connection = connection;
    }

    private HttpURLConnection getConnection() {
        return connection;
    }
}
