package com.example.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.fasterxml.jackson.databind.JsonNode;
import com.example.graphql.model.*;
import com.example.graphql.util.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

import graphql.schema.DataFetchingEnvironment;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.ArrayList;

@Component
public class Query implements GraphQLQueryResolver {

    public Collection<Purchase> getPurchases(DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "purchases", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        Map<String, Collection<Purchase>> endpointResults = new HashMap<>();
        final Map<String, JsonNode> results = new HashMap<>();
        for (String endpoint : queries.keySet()) {
            JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
            response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
            results.put(endpoint, response.get("purchases"));
        }
        for (String endpoint : results.keySet()) {
            final Collection<Purchase> result = new ArrayList<>();
            if (results.get(endpoint).isArray()) {
                results.get(endpoint).forEach(response -> {
                    result.add(new Purchase(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "purchases"), endpoint));
                });
            } else {
                final JsonNode response = results.get(endpoint);
                result.add(new Purchase(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "purchases"), endpoint));
            }
            endpointResults.put(endpoint, result);
        }
        Collection<Purchase> result = new ArrayList<>();
        if (Purchase.merger == null) {
            for (String endpoint : endpointResults.keySet()) {
                result.addAll(endpointResults.get(endpoint));
            }
        } else {
            result = Purchase.merger.merge(endpointResults);
        }

        return result;
    }

    public Collection<Invoice> getInvoices(DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "invoices", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        Map<String, Collection<Invoice>> endpointResults = new HashMap<>();
        final Map<String, JsonNode> results = new HashMap<>();
        for (String endpoint : queries.keySet()) {
            JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
            response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
            results.put(endpoint, response.get("invoices"));
        }
        for (String endpoint : results.keySet()) {
            final Collection<Invoice> result = new ArrayList<>();
            if (results.get(endpoint).isArray()) {
                results.get(endpoint).forEach(response -> {
                    result.add(new Invoice(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "invoices"), endpoint));
                });
            } else {
                final JsonNode response = results.get(endpoint);
                result.add(new Invoice(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "invoices"), endpoint));
            }
            endpointResults.put(endpoint, result);
        }
        Collection<Invoice> result = new ArrayList<>();
        if (Invoice.merger == null) {
            for (String endpoint : endpointResults.keySet()) {
                result.addAll(endpointResults.get(endpoint));
            }
        } else {
            result = Invoice.merger.merge(endpointResults);
        }

        return result;
    }

    public Collection<Partner> getPartners(DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "partners", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        Map<String, Collection<Partner>> endpointResults = new HashMap<>();
        final Map<String, JsonNode> results = new HashMap<>();
        for (String endpoint : queries.keySet()) {
            JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
            response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
            results.put(endpoint, response.get("partners"));
        }
        for (String endpoint : results.keySet()) {
            final Collection<Partner> result = new ArrayList<>();
            if (results.get(endpoint).isArray()) {
                results.get(endpoint).forEach(response -> {
                    result.add(new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "partners"), endpoint));
                });
            } else {
                final JsonNode response = results.get(endpoint);
                result.add(new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "partners"), endpoint));
            }
            endpointResults.put(endpoint, result);
        }
        Collection<Partner> result = new ArrayList<>();
        if (Partner.merger == null) {
            for (String endpoint : endpointResults.keySet()) {
                result.addAll(endpointResults.get(endpoint));
            }
        } else {
            result = Partner.merger.merge(endpointResults);
        }

        return result;
    }

    public Collection<Store> getStores(DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "stores", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        Map<String, Collection<Store>> endpointResults = new HashMap<>();
        final Map<String, JsonNode> results = new HashMap<>();
        for (String endpoint : queries.keySet()) {
            JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
            response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
            results.put(endpoint, response.get("stores"));
        }
        for (String endpoint : results.keySet()) {
            final Collection<Store> result = new ArrayList<>();
            if (results.get(endpoint).isArray()) {
                results.get(endpoint).forEach(response -> {
                    result.add(new Store(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "stores"), endpoint));
                });
            } else {
                final JsonNode response = results.get(endpoint);
                result.add(new Store(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "stores"), endpoint));
            }
            endpointResults.put(endpoint, result);
        }
        Collection<Store> result = new ArrayList<>();
        if (Store.merger == null) {
            for (String endpoint : endpointResults.keySet()) {
                result.addAll(endpointResults.get(endpoint));
            }
        } else {
            result = Store.merger.merge(endpointResults);
        }

        return result;
    }

    public Partner getClient(Long client, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "client", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("client");
        return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "client"), endpoint);
    }

    public Store getStore(Long store, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "store", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("store");
        return new Store(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "store"), endpoint);
    }

    public Partner getEmployee(Long employee, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "employee", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("employee");
        return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "employee"), endpoint);
    }

    public Department getDepartment(Long department, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "department", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("department");
        return new Department(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "department"), endpoint);
    }

    public Partner getCustomer(Long customer, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "customer", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("customer");
        return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "customer"), endpoint);
    }

    public Invoice getInvoice(Long invoice, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "invoice", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("invoice");
        return new Invoice(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "invoice"), endpoint);
    }

    public Collection<Department> getDepartments(DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "departments", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        Map<String, Collection<Department>> endpointResults = new HashMap<>();
        final Map<String, JsonNode> results = new HashMap<>();
        for (String endpoint : queries.keySet()) {
            JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
            response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
            results.put(endpoint, response.get("departments"));
        }
        for (String endpoint : results.keySet()) {
            final Collection<Department> result = new ArrayList<>();
            if (results.get(endpoint).isArray()) {
                results.get(endpoint).forEach(response -> {
                    result.add(new Department(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "departments"), endpoint));
                });
            } else {
                final JsonNode response = results.get(endpoint);
                result.add(new Department(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "departments"), endpoint));
            }
            endpointResults.put(endpoint, result);
        }
        Collection<Department> result = new ArrayList<>();
        if (Department.merger == null) {
            for (String endpoint : endpointResults.keySet()) {
                result.addAll(endpointResults.get(endpoint));
            }
        } else {
            result = Department.merger.merge(endpointResults);
        }

        return result;
    }

    public Purchase getPurchase(Long purchase, DataFetchingEnvironment environment) throws IOException {
        Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Query", "purchase", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
        if (queries.keySet().size() > 1) {
            throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
        }
        final String endpoint = queries.keySet().iterator().next();
        JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
        response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Query", response, environment.getSelectionSet().getArguments().keySet());
        response = response.get("purchase");
        return new Purchase(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Query", "purchase"), endpoint);
    }


}
