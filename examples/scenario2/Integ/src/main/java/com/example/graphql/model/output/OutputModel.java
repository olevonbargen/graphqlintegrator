package com.example.graphql.model.output;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;

import java.util.ArrayList;
import java.util.Collection;

public class OutputModel extends OutputEntity {

    private String endpoint;
    private Collection<OutputType> types;

    public OutputModel() {
        this.types = new ArrayList<>();
    }

    public OutputModel(String name, String endpoint) {
        super(name);
        this.endpoint = endpoint;
        this.types = new ArrayList<>();
    }

    public OutputType addType(final String name, final EqualityExpression equalityExpression) {
        final OutputType result = new OutputType(name, equalityExpression);
        this.types.add(result);
        return result;
    }

    public Collection<OutputType> getTypes() {
        return types;
    }

    public String getEndpoint() {
        return this.endpoint;
    }
}
