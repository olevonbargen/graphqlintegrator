package com.example.graphql.model;

import com.example.graphql.model.output.OutputField;
import com.example.graphql.model.output.OutputType;
import com.example.graphql.util.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.VariableExpression;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;

public class DepartmentMerger extends MergerImpl<Department> {

    public DepartmentMerger(EqualityExpression equalityExpression) {
        super(equalityExpression);
    }

    @Override
    public Pair<String, Department> extractKeys(String endpoint, Department element) {
        return null;
    }

    @Override
    protected Department createMerge(Department x, Department y) {
        final Department result = new Department();
        result.setManager(x.getManager() == null ? y.getManager() : x.getManager());
result.setName(x.getName() == null ? y.getName() : x.getName());
result.setId(x.getId() == null ? y.getId() : x.getId());
Collection<Partner> listWorkingAt = new ArrayList<>();
if (x.getWorkingAt() != null) {
    listWorkingAt.addAll(x.getWorkingAt());
} if (y.getWorkingAt() != null) {
    listWorkingAt.addAll(y.getWorkingAt());
} if (Partner.merger != null) {
    listWorkingAt = Partner.merger.merge(listWorkingAt);
}
result.setWorkingAt(listWorkingAt);

        return result;
    }

    @Override
    protected Object resolveVariable(VariableExpression variable, Collection<Department> inputs) {
        Department element = null;
        for (Department current : inputs) {
            if (this.getModelName(current).equals(variable.getModelName())) {
                element = current;
                break;
            }
        }
        if (element == null) {
            throw new Error("Could not resolve " + variable);
        }
        OutputType type = this.getOutputType(element);
        if (type == null) {
            throw new Error("Could not resolve " + variable + " with given instances " + inputs);
        }
        OutputField field = null;
        for (OutputField current : type.getFields()) {
            if (current.getOriginalName().equals(variable.getFieldName())) {
                field = current;
                break;
            }
        }
        if (field == null) {
            throw new Error("Could not resolve " + variable + " within " + variable.getModelName());
        }
        return this.getField(element, field.getName());
    }

    private Object getField(Department element, String field) {
        if (field.equals("manager")) {
    return element.getManager();
}
if (field.equals("name")) {
    return element.getName();
}
if (field.equals("id")) {
    return element.getId();
}
if (field.equals("workingAt")) {
    return element.getWorkingAt();
}

        throw new Error("Could not resolve field " + field + " within type Department");
    }

    @Override
    protected String getModelName(Department element) {
        return GraphQLCaller.INSTANCE.getModelName(element.getEndpoint());
    }

    @Override
    protected OutputType getOutputType(Department element) {
        return Department.globaleType.getTypes().get(element.getEndpoint());
    }
}
