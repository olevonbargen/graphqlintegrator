package com.example.graphql.model;

import com.example.graphql.model.output.OutputField;
import com.example.graphql.model.output.OutputType;
import com.example.graphql.util.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.VariableExpression;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;

public class StoreMerger extends MergerImpl<Store> {

    public StoreMerger(EqualityExpression equalityExpression) {
        super(equalityExpression);
    }

    @Override
    public Pair<String, Store> extractKeys(String endpoint, Store element) {
        return null;
    }

    @Override
    protected Store createMerge(Store x, Store y) {
        final Store result = new Store();
        result.setManager(x.getManager() == null ? y.getManager() : x.getManager());
result.setId(x.getId() == null ? y.getId() : x.getId());
Collection<Purchase> listPurchases = new ArrayList<>();
if (x.getPurchases() != null) {
    listPurchases.addAll(x.getPurchases());
} if (y.getPurchases() != null) {
    listPurchases.addAll(y.getPurchases());
} if (Purchase.merger != null) {
    listPurchases = Purchase.merger.merge(listPurchases);
}
result.setPurchases(listPurchases);

        return result;
    }

    @Override
    protected Object resolveVariable(VariableExpression variable, Collection<Store> inputs) {
        Store element = null;
        for (Store current : inputs) {
            if (this.getModelName(current).equals(variable.getModelName())) {
                element = current;
                break;
            }
        }
        if (element == null) {
            throw new Error("Could not resolve " + variable);
        }
        OutputType type = this.getOutputType(element);
        if (type == null) {
            throw new Error("Could not resolve " + variable + " with given instances " + inputs);
        }
        OutputField field = null;
        for (OutputField current : type.getFields()) {
            if (current.getOriginalName().equals(variable.getFieldName())) {
                field = current;
                break;
            }
        }
        if (field == null) {
            throw new Error("Could not resolve " + variable + " within " + variable.getModelName());
        }
        return this.getField(element, field.getName());
    }

    private Object getField(Store element, String field) {
        if (field.equals("manager")) {
    return element.getManager();
}
if (field.equals("id")) {
    return element.getId();
}
if (field.equals("purchases")) {
    return element.getPurchases();
}

        throw new Error("Could not resolve field " + field + " within type Store");
    }

    @Override
    protected String getModelName(Store element) {
        return GraphQLCaller.INSTANCE.getModelName(element.getEndpoint());
    }

    @Override
    protected OutputType getOutputType(Store element) {
        return Store.globaleType.getTypes().get(element.getEndpoint());
    }
}
