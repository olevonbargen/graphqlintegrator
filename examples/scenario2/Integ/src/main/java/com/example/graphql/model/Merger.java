package com.example.graphql.model;

import java.util.Collection;
import java.util.Map;

public interface Merger<T> {

    Collection<T> merge(Map<String, Collection<T>> elements);

    Collection<T> merge(Collection<T> elements);

}
