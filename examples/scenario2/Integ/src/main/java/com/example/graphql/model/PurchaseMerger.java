package com.example.graphql.model;

import com.example.graphql.model.output.OutputField;
import com.example.graphql.model.output.OutputType;
import com.example.graphql.util.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.VariableExpression;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;

public class PurchaseMerger extends MergerImpl<Purchase> {

    public PurchaseMerger(EqualityExpression equalityExpression) {
        super(equalityExpression);
    }

    @Override
    public Pair<String, Purchase> extractKeys(String endpoint, Purchase element) {
        return null;
    }

    @Override
    protected Purchase createMerge(Purchase x, Purchase y) {
        final Purchase result = new Purchase();
        result.setDate(x.getDate() == null ? y.getDate() : x.getDate());
result.setCustomer(x.getCustomer() == null ? y.getCustomer() : x.getCustomer());
result.setStore(x.getStore() == null ? y.getStore() : x.getStore());
result.setId(x.getId() == null ? y.getId() : x.getId());

        return result;
    }

    @Override
    protected Object resolveVariable(VariableExpression variable, Collection<Purchase> inputs) {
        Purchase element = null;
        for (Purchase current : inputs) {
            if (this.getModelName(current).equals(variable.getModelName())) {
                element = current;
                break;
            }
        }
        if (element == null) {
            throw new Error("Could not resolve " + variable);
        }
        OutputType type = this.getOutputType(element);
        if (type == null) {
            throw new Error("Could not resolve " + variable + " with given instances " + inputs);
        }
        OutputField field = null;
        for (OutputField current : type.getFields()) {
            if (current.getOriginalName().equals(variable.getFieldName())) {
                field = current;
                break;
            }
        }
        if (field == null) {
            throw new Error("Could not resolve " + variable + " within " + variable.getModelName());
        }
        return this.getField(element, field.getName());
    }

    private Object getField(Purchase element, String field) {
        if (field.equals("date")) {
    return element.getDate();
}
if (field.equals("customer")) {
    return element.getCustomer();
}
if (field.equals("store")) {
    return element.getStore();
}
if (field.equals("id")) {
    return element.getId();
}

        throw new Error("Could not resolve field " + field + " within type Purchase");
    }

    @Override
    protected String getModelName(Purchase element) {
        return GraphQLCaller.INSTANCE.getModelName(element.getEndpoint());
    }

    @Override
    protected OutputType getOutputType(Purchase element) {
        return Purchase.globaleType.getTypes().get(element.getEndpoint());
    }
}
