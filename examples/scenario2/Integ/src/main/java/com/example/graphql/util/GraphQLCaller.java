package com.example.graphql.util;

import com.fasterxml.jackson.databind.JsonNode;

import com.example.graphql.model.output.OutputType;

import java.util.Map;
import java.util.Set;
import java.io.IOException;

public interface GraphQLCaller {

    GraphQLCaller INSTANCE = new GraphQLCallerImpl();

    JsonNode executeQuery(String endpoint, String query) throws IOException;

    Map<String, String> generateQueries(String rootType, String fieldName, Map<String, Object> arguments, Set<String> selectionSetArguments);

    JsonNode normalizeResults(final String endpoint, final String type, JsonNode response, Set<String> selectionSetArguments);

    OutputType getResultType(final String endpoint, final String typeName, final String fieldName);

    String getModelName(final String endpoint);
}
