package com.example.graphql.model;

import com.example.graphql.model.output.OutputField;
import com.example.graphql.model.output.OutputType;
import com.example.graphql.util.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.VariableExpression;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;

public class PartnerMerger extends MergerImpl<Partner> {

    public PartnerMerger(EqualityExpression equalityExpression) {
        super(equalityExpression);
    }

    @Override
    public Pair<String, Partner> extractKeys(String endpoint, Partner element) {
        if (element.getId().size() == 1) {
            return Pair.of(element.getId().iterator().next().toString(), element);
        }
        return Pair.of(element.getId().stream().reduce("", (old, current) -> old + "" + current.toString(), (left,right) -> left + right), element);
    }

    @Override
    protected Partner createMerge(Partner x, Partner y) {
        final Partner result = new Partner();
        result.setHiredAt(x.getHiredAt() == null ? y.getHiredAt() : x.getHiredAt());
        result.setPhone(x.getPhone() == null ? y.getPhone() : x.getPhone());
        result.setLastname(x.getLastname() == null ? y.getLastname() : x.getLastname());
        Collection<Invoice> listInvoices = new ArrayList<>();
        if (x.getInvoices() != null) {
            listInvoices.addAll(x.getInvoices());
        }
        if (y.getInvoices() != null) {
            listInvoices.addAll(y.getInvoices());
        }
        if (Invoice.merger != null) {
            listInvoices = Invoice.merger.merge(listInvoices);
        }
        result.setInvoices(listInvoices);
        result.setName(x.getName() == null ? y.getName() : x.getName());
        result.setWorksAt(x.getWorksAt() == null ? y.getWorksAt() : x.getWorksAt());
        Collection<Long> listId = new ArrayList<>();
        if (x.getId() != null) {
            for (Long current : x.getId()) {
                if (!listId.contains(current)) {
                    listId.add(current);
                }
            }
        }
        if (y.getId() != null) {
            for (Long current : y.getId()) {
                if (!listId.contains(current)) {
                    listId.add(current);
                }
            }
        }
        result.setId(listId);
        Collection<Purchase> listPurchases = new ArrayList<>();
        if (x.getPurchases() != null) {
            listPurchases.addAll(x.getPurchases());
        }
        if (y.getPurchases() != null) {
            listPurchases.addAll(y.getPurchases());
        }
        if (Purchase.merger != null) {
            listPurchases = Purchase.merger.merge(listPurchases);
        }
        result.setPurchases(listPurchases);
        result.setEmail(x.getEmail() == null ? y.getEmail() : x.getEmail());
        result.setFirstname(x.getFirstname() == null ? y.getFirstname() : x.getFirstname());

        return result;
    }

    @Override
    protected Object resolveVariable(VariableExpression variable, Collection<Partner> inputs) {
        Partner element = null;
        for (Partner current : inputs) {
            if (this.getModelName(current).equals(variable.getModelName())) {
                element = current;
                break;
            }
        }
        if (element == null) {
            throw new Error("Could not resolve " + variable);
        }
        OutputType type = this.getOutputType(element);
        if (type == null) {
            throw new Error("Could not resolve " + variable + " with given instances " + inputs);
        }
        OutputField field = null;
        for (OutputField current : type.getFields()) {
            if (current.getOriginalName().equals(variable.getFieldName())) {
                field = current;
                break;
            }
        }
        if (field == null) {
            throw new Error("Could not resolve " + variable + " within " + variable.getModelName());
        }
        return this.getField(element, field.getName());
    }

    private Object getField(Partner element, String field) {
        if (field.equals("hiredAt")) {
            return element.getHiredAt();
        }
        if (field.equals("phone")) {
            return element.getPhone();
        }
        if (field.equals("lastname")) {
            return element.getLastname();
        }
        if (field.equals("invoices")) {
            return element.getInvoices();
        }
        if (field.equals("name")) {
            return element.getName();
        }
        if (field.equals("worksAt")) {
            return element.getWorksAt();
        }
        if (field.equals("id")) {
            return element.getId().iterator().next();
        }
        if (field.equals("purchases")) {
            return element.getPurchases();
        }
        if (field.equals("email")) {
            return element.getEmail();
        }
        if (field.equals("firstname")) {
            return element.getFirstname();
        }

        throw new Error("Could not resolve field " + field + " within type Partner");
    }

    @Override
    protected String getModelName(Partner element) {
        return GraphQLCaller.INSTANCE.getModelName(element.getEndpoint());
    }

    @Override
    protected OutputType getOutputType(Partner element) {
        return Partner.globaleType.getTypes().get(element.getEndpoint());
    }
}
