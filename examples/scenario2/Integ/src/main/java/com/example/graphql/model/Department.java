package com.example.graphql.model;

import com.example.graphql.model.output.*;
import com.example.graphql.util.GraphQLCaller;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;

public class Department {

    public static final OutputGlobalType globaleType = new OutputGlobalType("Department", null);
    public static DepartmentMerger merger = null;

    public Department() {
    }

    public Department(final JsonNode input, final OutputType outputType, final String endpoint) {
final Iterator<String> fieldNames = input.fieldNames();
while (fieldNames.hasNext()) {
final String currentFieldName = fieldNames.next();
switch (currentFieldName) {
case "manager":
this.setManager(new Partner(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Department", "manager"), endpoint));
break;
case "name":
this.setName(input.get(currentFieldName).asText());
break;
case "id":
this.setId(input.get(currentFieldName).asLong());
break;
case "workingAt":
this.setWorkingAt(new ArrayList<>());
if (input.get(currentFieldName).isArray()) {
input.get(currentFieldName).forEach(element -> this.getWorkingAt().add(new Partner(element, GraphQLCaller.INSTANCE.getResultType(endpoint, "Department", "workingAt"), endpoint)));
} else {
this.getWorkingAt().add(new Partner(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Department", "workingAt"), endpoint));
}break;
default: 
throw new Error("Cannot convert field " + currentFieldName + " within the type Department");
}
}
globaleType.getTypes().put(endpoint, outputType);
       if (outputType.getEqualityExpression() != null) {
            globaleType.setExpression(outputType.getEqualityExpression());
            merger = new DepartmentMerger(outputType.getEqualityExpression());
        }
        this.setEndpoint(endpoint);
}

    private Partner manager;

private String name;

private Long id;

private Collection<Partner> workingAt;

private String endpoint;
public Partner getManager() {
    return this.manager;
}

public void setManager(final Partner manager) {
    this.manager = manager;
}

public String getName() {
    return this.name;
}

public void setName(final String name) {
    this.name = name;
}

public Long getId() {
    return this.id;
}

public void setId(final Long id) {
    this.id = id;
}

public Collection<Partner> getWorkingAt() {
    return this.workingAt;
}

public void setWorkingAt(final Collection<Partner> workingAt) {
    this.workingAt = workingAt;
}

public String getEndpoint() {
    return this.endpoint;
}

public void setEndpoint(final String endpoint) {
    this.endpoint = endpoint;
}

}
