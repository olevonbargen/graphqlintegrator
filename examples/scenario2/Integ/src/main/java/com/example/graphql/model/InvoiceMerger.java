package com.example.graphql.model;

import com.example.graphql.model.output.OutputField;
import com.example.graphql.model.output.OutputType;
import com.example.graphql.util.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.VariableExpression;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;

public class InvoiceMerger extends MergerImpl<Invoice> {

    public InvoiceMerger(EqualityExpression equalityExpression) {
        super(equalityExpression);
    }

    @Override
    public Pair<String, Invoice> extractKeys(String endpoint, Invoice element) {
        return null;
    }

    @Override
    protected Invoice createMerge(Invoice x, Invoice y) {
        final Invoice result = new Invoice();
        result.setCreatedAt(x.getCreatedAt() == null ? y.getCreatedAt() : x.getCreatedAt());
result.setPayedAt(x.getPayedAt() == null ? y.getPayedAt() : x.getPayedAt());
result.setTotal(x.getTotal() == null ? y.getTotal() : x.getTotal());
result.setClient(x.getClient() == null ? y.getClient() : x.getClient());
result.setDueAt(x.getDueAt() == null ? y.getDueAt() : x.getDueAt());
result.setId(x.getId() == null ? y.getId() : x.getId());

        return result;
    }

    @Override
    protected Object resolveVariable(VariableExpression variable, Collection<Invoice> inputs) {
        Invoice element = null;
        for (Invoice current : inputs) {
            if (this.getModelName(current).equals(variable.getModelName())) {
                element = current;
                break;
            }
        }
        if (element == null) {
            throw new Error("Could not resolve " + variable);
        }
        OutputType type = this.getOutputType(element);
        if (type == null) {
            throw new Error("Could not resolve " + variable + " with given instances " + inputs);
        }
        OutputField field = null;
        for (OutputField current : type.getFields()) {
            if (current.getOriginalName().equals(variable.getFieldName())) {
                field = current;
                break;
            }
        }
        if (field == null) {
            throw new Error("Could not resolve " + variable + " within " + variable.getModelName());
        }
        return this.getField(element, field.getName());
    }

    private Object getField(Invoice element, String field) {
        if (field.equals("createdAt")) {
    return element.getCreatedAt();
}
if (field.equals("payedAt")) {
    return element.getPayedAt();
}
if (field.equals("total")) {
    return element.getTotal();
}
if (field.equals("client")) {
    return element.getClient();
}
if (field.equals("dueAt")) {
    return element.getDueAt();
}
if (field.equals("id")) {
    return element.getId();
}

        throw new Error("Could not resolve field " + field + " within type Invoice");
    }

    @Override
    protected String getModelName(Invoice element) {
        return GraphQLCaller.INSTANCE.getModelName(element.getEndpoint());
    }

    @Override
    protected OutputType getOutputType(Invoice element) {
        return Invoice.globaleType.getTypes().get(element.getEndpoint());
    }
}
