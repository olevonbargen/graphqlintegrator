package com.example.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.fasterxml.jackson.databind.JsonNode;
import com.example.graphql.model.*;
import com.example.graphql.util.*;
import org.springframework.stereotype.Component;
import java.io.IOException;
import graphql.schema.DataFetchingEnvironment;

import java.util.Map;
import java.util.HashMap;


@Component
public class Mutation implements GraphQLMutationResolver {

    public Partner getDeleteCustomer(Long customer,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deleteCustomer", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deleteCustomer");
    return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deleteCustomer"), endpoint);
}
public Invoice getCreateInvoice(Long client, String createdAt, String dueAt, String payedAt, Integer total,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createInvoice", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createInvoice");
    return new Invoice(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createInvoice"), endpoint);
}
public Partner getCreateEmployee(Long department, String firstname, String lastname, String hiredAt, String phone,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createEmployee", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createEmployee");
    return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createEmployee"), endpoint);
}
public Purchase getCreatePurchase(Long customer, String date, Long store,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createPurchase", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createPurchase");
    return new Purchase(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createPurchase"), endpoint);
}
public Invoice getDeleteInvoice(Long invoice,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deleteInvoice", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deleteInvoice");
    return new Invoice(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deleteInvoice"), endpoint);
}
public Department getDeleteDepartment(Long department,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deleteDepartment", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deleteDepartment");
    return new Department(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deleteDepartment"), endpoint);
}
public Partner getCreateClient(String name,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createClient", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createClient");
    return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createClient"), endpoint);
}
public Store getCreateStore(Long manager,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createStore", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createStore");
    return new Store(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createStore"), endpoint);
}
public Store getDeleteStore(Long store,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deleteStore", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deleteStore");
    return new Store(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deleteStore"), endpoint);
}
public Partner getDeleteEmployee(Long employee,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deleteEmployee", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deleteEmployee");
    return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deleteEmployee"), endpoint);
}
public Department getCreateDepartment(String name, Long manager,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createDepartment", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createDepartment");
    return new Department(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createDepartment"), endpoint);
}
public Partner getDeleteClient(Long client,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deleteClient", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deleteClient");
    return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deleteClient"), endpoint);
}
public Purchase getDeletePurchase(Long purchase,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "deletePurchase", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("deletePurchase");
    return new Purchase(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "deletePurchase"), endpoint);
}
public Partner getCreateCustomer(String name, String email,  DataFetchingEnvironment environment) throws IOException {
    Map<String, String> queries = GraphQLCaller.INSTANCE.generateQueries("Mutation", "createCustomer", environment.getArguments(), environment.getSelectionSet().getArguments().keySet());
    if (queries.keySet().size() > 1) {
        throw new Error("Non-deterministic result found due to suitable resolvers in more than one base model!");
    }
    final String endpoint = queries.keySet().iterator().next();
    JsonNode response = GraphQLCaller.INSTANCE.executeQuery(endpoint, queries.get(endpoint));
    response = GraphQLCaller.INSTANCE.normalizeResults(endpoint, "Mutation", response, environment.getSelectionSet().getArguments().keySet());
    response = response.get("createCustomer");
    return new Partner(response, GraphQLCaller.INSTANCE.getResultType(endpoint, "Mutation", "createCustomer"), endpoint);
}


}
