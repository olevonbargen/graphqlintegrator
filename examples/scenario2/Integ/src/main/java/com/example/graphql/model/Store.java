package com.example.graphql.model;

import com.example.graphql.model.output.*;
import com.example.graphql.util.GraphQLCaller;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;

public class Store {

    public static final OutputGlobalType globaleType = new OutputGlobalType("Store", null);
    public static StoreMerger merger = null;

    public Store() {
    }

    public Store(final JsonNode input, final OutputType outputType, final String endpoint) {
final Iterator<String> fieldNames = input.fieldNames();
while (fieldNames.hasNext()) {
final String currentFieldName = fieldNames.next();
switch (currentFieldName) {
case "manager":
this.setManager(input.get(currentFieldName).asLong());
break;
case "id":
this.setId(input.get(currentFieldName).asLong());
break;
case "purchases":
this.setPurchases(new ArrayList<>());
if (input.get(currentFieldName).isArray()) {
input.get(currentFieldName).forEach(element -> this.getPurchases().add(new Purchase(element, GraphQLCaller.INSTANCE.getResultType(endpoint, "Store", "purchases"), endpoint)));
} else {
this.getPurchases().add(new Purchase(input.get(currentFieldName), GraphQLCaller.INSTANCE.getResultType(endpoint, "Store", "purchases"), endpoint));
}break;
default: 
throw new Error("Cannot convert field " + currentFieldName + " within the type Store");
}
}
globaleType.getTypes().put(endpoint, outputType);
       if (outputType.getEqualityExpression() != null) {
            globaleType.setExpression(outputType.getEqualityExpression());
            merger = new StoreMerger(outputType.getEqualityExpression());
        }
        this.setEndpoint(endpoint);
}

    private Long manager;

private Long id;

private Collection<Purchase> purchases;

private String endpoint;
public Long getManager() {
    return this.manager;
}

public void setManager(final Long manager) {
    this.manager = manager;
}

public Long getId() {
    return this.id;
}

public void setId(final Long id) {
    this.id = id;
}

public Collection<Purchase> getPurchases() {
    return this.purchases;
}

public void setPurchases(final Collection<Purchase> purchases) {
    this.purchases = purchases;
}

public String getEndpoint() {
    return this.endpoint;
}

public void setEndpoint(final String endpoint) {
    this.endpoint = endpoint;
}

}
