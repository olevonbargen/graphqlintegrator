const {ApolloServer, gql} = require('apollo-server');
const {makeExecutableSchema} = require('graphql-tools');
const { buildFederatedSchema } = require('@apollo/federation');

const Query = require('./resolver/Query');
const Mutation = require('./resolver/Mutation');
const Invoice = require('./resolver/Invoice');

const DAO = require('./dao/AppDAO');
const AddressRepository = require('./repositories/AddressRepository');
const PaymentDetailsRepository = require('./repositories/PaymentDetailsRepository');
const ClientRepository = require('./repositories/ClientRepository');
const InvoiceRepository = require('./repositories/InvoiceRepository');

const dbFilePath = './invoices.db';
const appDao = new DAO.AppDAO(dbFilePath);
const addressRepository = new AddressRepository.AddressRepository(appDao);
const paymentDetailsRepository = new PaymentDetailsRepository.PaymentDetailsRepository(appDao);
const clientRepository = new ClientRepository.ClientRepository(appDao);
const invoiceRepository = new InvoiceRepository.InvoiceRepository(appDao);


const typeDefs = gql`
    type Query {
        clients: [Partner]
        client(client: ID!): Partner
        invoice(invoice: ID!): Invoice
        invoices: [Invoice]
    }

    type Mutation {
        createClient(name: String): Partner
        deleteClient(client: ID!): Partner
        createInvoice(client: ID!, createdAt: String, dueAt: String, payedAt: String, total: Int): Invoice
        deleteInvoice(invoice: ID!): Invoice
    }
    
    type Partner @key(fields: "id") {
        id: ID!
        name: String
        invoices: [Invoice]
    }

    type Invoice {
        id: ID!
        client: Partner!
        createdAt: String
        dueAt: String
        payedAt: String
        total: Int
    }
`;
const resolvers = {
    Query,
    Mutation,
    Invoice,
    Partner : {
        __resolveReference(partner){
            return clientRepository.getById(partner.id)
        },
        invoices(parent, _, context) {
            return context.clientRepository.getInvoicesByClientId(parent.id).then((invoices) => {
                return invoices
            }).catch((err) => {
                console.log(`Error retrieving the invoices of client ${parent.id}!`);
                console.log(JSON.stringify(err))
            })
        }
    }
};

const schema = buildFederatedSchema({typeDefs, resolvers});

const server = new ApolloServer({
    schema: schema,
    context: {
        addressRepository: addressRepository,
        paymentDetailsRepository: paymentDetailsRepository,
        clientRepository: clientRepository,
        invoiceRepository: invoiceRepository,
    }
});

addressRepository.createTable().then(()=> {
    paymentDetailsRepository.createTable().then(() => {
        clientRepository.createTable().then(() => {
            invoiceRepository.createTable().then(() => {
                server.listen({port: 4012}).then(({url}) => {
                    console.log(`Server is running at ${url}`)
                })
            })
        })
    })
});


