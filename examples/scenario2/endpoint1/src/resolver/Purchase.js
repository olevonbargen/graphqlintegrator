function customer(parent, _, context) {
    return context.purchaseRepository.getCustomerByPurchaseId(parent.id).then((customer) => {
        return {
            __typename : "Partner",
            id : customer.id,
            email : customer.id,
            purchases : function () {
                return context.customerRepository.getPurchasesByCustomerId(customer.id).then((purchases) => {
                    return purchases
                }).catch((err) => {
                    console.log(`Error occurred while fetching the purchases for customer ${parent.id}!`);
                    console.log(JSON.stringify(err))
                })
            }
        }
    }).catch((err) => {
        console.log(`Error occurred while fetching the customer ${parent.id}!`);
        console.log(JSON.stringify(err))
    })
}

function store(parent, _, context) {
    return context.purchaseRepository.getStoreByPurchaseId(parent.id).then((store) => {
        return store
    }).catch((err) => {
        console.log(`Error occurred while fetching the store from ${parent.id}!`);
        console.log(JSON.stringify(err))
    })
}

function items(parent, _, context) {
    return context.purchaseRepository.getPurchaseItemsByPurchaseId(parent.id).then((items) => {
        return items
    }).catch((err) => {
        console.log(`Error occured while fetching the purchase items from purchase ${parent.id}!`);
        console.log(JSON.stringify(err))
    })
}

function transformCustomerToPartner(customer) {
    
}

module.exports = {
    customer,
    store,
};
