const {ApolloServer, gql} = require('apollo-server');
const {makeExecutableSchema} = require('graphql-tools');
const { buildFederatedSchema } = require('@apollo/federation');

const Query = require('./resolver/Query');
const Mutation = require('./resolver/Mutation');
const Customer = require('./resolver/Customer');
const Store = require('./resolver/Store');
const Purchase = require('./resolver/Purchase');

const DAO = require('./dao/AppDAO');
const AddressRepository = require('./repositories/AddressRepository');
const CustomerRepository = require('./repositories/CustomerRepository');
const PurchaseRepository = require('./repositories/PurchaseRepository');
const StoreRepository = require('./repositories/StoreRepository');
const PurchaseItemRepository = require('./repositories/PurchaseItemRepository');


const dbFilePath = './sales.db';
const appDao = new DAO.AppDAO(dbFilePath);
const customerRepository = new CustomerRepository.CustomerRepository(appDao);
const purchaseRepository = new PurchaseRepository.PurchaseRepository(appDao);
const storeRepository = new StoreRepository.StoreRepository(appDao);
const addressRepository = new AddressRepository.AddressRepository(appDao);
const purchaseItemRepository = new PurchaseItemRepository.PurchaseItemRepository(appDao);

const typeDefs = gql`
    type Query {
        customer(customer: ID!): Partner
        customers: [Partner]
        purchase(purchase: ID!): Purchase
        purchases: [Purchase]
        store(store: ID!): Store
        stores: [Store]
    }

    type Mutation {
        createCustomer(name: String!, email: String): Partner!
        deleteCustomer(customer: ID!): Partner
        createPurchase(customer: ID!, date: String!, store: ID!): Purchase
        deletePurchase(purchase: ID!): Purchase
        createStore(manager: ID): Store
        deleteStore(store: ID!): Store
    }

    extend type Partner @key(fields: "id") {
        id: ID! @external
        email: String
        purchases: [Purchase]
    }

    type Purchase {
        id: ID!
        date: String
        customer: Partner!
        store: Store!
    }

    type Store {
        id: ID!
        manager: ID!
        purchases: [Purchase]
    }

`;
const resolvers = {
    Query,
    Mutation,
    Store,
    Purchase
};

const schema = buildFederatedSchema({typeDefs, resolvers});

const server = new ApolloServer({
    schema: schema,
    context: {
        customerRepository: customerRepository,
        purchaseRepository: purchaseRepository,
        storeRepository: storeRepository,
        addressRepository : addressRepository,
        purchaseItemRepository : purchaseItemRepository
    }
});

addressRepository.createTable().then(() => {
    customerRepository.createTable().then(() => {
        storeRepository.createTable().then(() => {
                purchaseRepository.createTable().then(() => {
                    purchaseItemRepository.createTable().then(() => {
                        server.listen({port: 4011}).then(({url}) => {
                            console.log(`Server is running at ${url}`)
                        })
                    })
            })
        })
    })
});


