const { ApolloServer } = require("apollo-server");
const { ApolloGateway } = require("@apollo/gateway");

const server = new ApolloServer({
  gateway: new ApolloGateway({
    serviceList: [
      { name: "invoices", url: "http://localhost:4012" },
      { name: "sales", url: "http://localhost:4011" },
      { name: "employees", url: "http://localhost:4013" }

    ]
  }),
  subscriptions: false
});

server.listen({port:4021}).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

