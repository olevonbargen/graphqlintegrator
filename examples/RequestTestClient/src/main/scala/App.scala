import akka.actor.ActorSystem
import akka.http.javadsl.unmarshalling.Unmarshaller
import akka.http.scaladsl.Http
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethod, HttpMethods, HttpRequest}
import akka.http.scaladsl.server.ContentNegotiator.Alternative.ContentType
import akka.stream.ActorMaterializer
import spray.json.DefaultJsonProtocol

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object App {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem()
    implicit val materizalizer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    implicit val jsonStreamingSupport: JsonEntityStreamingSupport =
      EntityStreamingSupport.json()

    var start = System.currentTimeMillis()

    val req =
      """{ "query" : "{\n customers\n {\n name \n purchases {\n id \n} \n invoices {\n dueAt \n } \n worksAt { \n name \n }\n } \n}" }"""

    send(1000, req)
  }

    def send(noReqs: Int, req: String)(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer, executionContext: ExecutionContext): Unit = {
      if (noReqs == 0) {
        System.out.println("Results of " + Recorder.noOfRequests() + " requests")
        System.out.println("Min " + Recorder.min() + " ms")
        System.out.println("Max " + Recorder.max() + " ms")
        System.out.println("Median " + Recorder.median() + " ms")
        System.out.println("Average " + Recorder.average() + " ms")
      } else {
        val response = Http()
          .singleRequest(buildOneReq(req))
        response.onComplete{
          case Success(value) => {
          //  System.out.println(value.status)
          //  val res  = Unmarshaller.entityToString.unmarshal(value.entity,materizalizer)
         //   System.out.println(res.whenComplete((s,e) => {
         //     System.out.println(s)
         //   }))
            Recorder.receiveResponse()
            send(noReqs - 1,req)
          }
          case Failure(exception) => {
            send(noReqs - 1,req)
          }
        }
      }
    }




  private def buildOneReq(req: String) =  {
    val result = HttpRequest(uri = "http://localhost:4021", method = HttpMethods.POST, entity = HttpEntity(ContentTypes.`application/json`, req))
    Recorder.sendRequest()
    result
  }
}
