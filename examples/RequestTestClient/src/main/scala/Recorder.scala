object Recorder {

  private var start : Long = 0
  private var durations : List[Long] = List()

  def min() = durations.sortWith(_ < _).head

  def max() = durations.sortWith(_ < _).last

  def median() = durations.sortWith(_ < _).apply(durations.length / 2)

  def average() = durations.foldLeft(0.0)((l :Double, r: Long) => l + r) / durations.length

  def noOfRequests(): Int = durations.length

  def sendRequest(): Unit = {start = System.currentTimeMillis()}

  def receiveResponse(): Unit = {
    val end = System.currentTimeMillis()
    val duration = end - start
    start = 0
    durations = duration :: durations
  }


}
