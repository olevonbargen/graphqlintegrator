object GraphQuery {

  def atomic(field: String): GraphQuery = GraphQuery(field, List(), List())

  def atomicWithArgs(field: String, args: List[(String,String)]) = GraphQuery(field, args, List())

  def complex(field: String, children: List[GraphQuery]) = GraphQuery(field, List(), children)

}

case class GraphQuery(name: String, arguments: List[(String, String)], children: List[GraphQuery]) {

  def print(): String = {
    var result = name
    if (arguments.nonEmpty) {
      result = result + " ("
      result = result + arguments.map( kv => kv._1 + " : \"" + kv._2 +"\"").mkString(",")
      result = result + ")"
    }
    if (children.nonEmpty) {
      result = result + " {\n"
      result = result + children.map(_.print()).mkString("\n")
      result = result + "}\n"
    }
    result
  }
}
