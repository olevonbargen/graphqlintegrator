grammar Correspondences;

correspondences: endpointDefinition modelCorrespondences EOF;

endpointDefinition: 'endpoints' '{' endpoint* '}';
endpoint: url 'as' modelAlias;
url: URL_STRING;

modelCorrespondences: 'correspondences' '{' modelCorrespondence* '}';
modelCorrespondence: 'relate' typeIdentifier (',' typeIdentifier)+ ('as' correspondenceAlias)? ('with' '{' fieldCorrespondence+ instanceCorrespondenceExpression? '}')?;

fieldCorrespondence: 'relate' fieldIdentifier (',' fieldIdentifier)+ ('as' correspondenceAlias)?;

instanceCorrespondenceExpression: 'identify' 'where' '{' equalityDescription '}';

equalityDescription: equalityExpression;
expression: equalityExpression || concatenationExpression || conjunctionExpression || disjunctionExpression || constant || variable;
equalityExpression: '==' '(' expression (',' expression)+ ')';
concatenationExpression: 'concat' '(' expression ',' expression ')';
conjunctionExpression: 'AND' '(' expression ',' expression ')';
disjunctionExpression: 'OR' '(' expression ',' expression ')';
variable: fieldIdentifier;
constant: integerConstant || stringConstant || floatConstant || boolConstant;
integerConstant: DIGIT+;
floatConstant: DIGIT+ '.' DIGIT+;
stringConstant: STRING;
boolConstant: 'true' || 'false';

fieldIdentifier: typeIdentifier '.' fieldName;
typeIdentifier: modelAlias '.' typeName;

modelAlias: IDENTIFIER;
typeName: IDENTIFIER;
fieldName: IDENTIFIER;
correspondenceAlias: IDENTIFIER;

STRING: STRING_CONSTANT_SEPARATOR LETTER+ STRING_CONSTANT_SEPARATOR;
STRING_CONSTANT_SEPARATOR: '"';

IDENTIFIER: LETTER_OR_UNDERSCORE LETTER_OR_DIGIT*;

LETTER_OR_UNDERSCORE: LETTER | '_';
LETTER_OR_DIGIT: LETTER | DIGIT;

URL_STRING: [a-zA-Z~0-9] [a-zA-Z0-9.-/:]*;
LETTER: [a-zA-Z];
DIGIT: [0-9];

WHITESPACE: [ \u000B\t\r\n] -> channel(HIDDEN);
