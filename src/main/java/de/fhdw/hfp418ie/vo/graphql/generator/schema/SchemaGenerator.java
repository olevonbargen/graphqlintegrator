package de.fhdw.hfp418ie.vo.graphql.generator.schema;

import de.fhdw.hfp418ie.vo.graphql.model.Model;

/**
 * Generator which uses an internal model and generates an GraphQL schema.
 */
public interface SchemaGenerator {

    SchemaGenerator DEFAULT_GENERATOR = new SchemaGeneratorImpl();

    /**
     * Generates a GraphQL schema definition out of a given internal model.
     *
     * @param model Given internal model.
     * @return GraphQL schema definition as string.
     */
    String generateSchemaDefinition(final Model model);

}
