package de.fhdw.hfp418ie.vo.graphql;

import de.fhdw.hfp418ie.vo.graphql.generator.endpoint.EndpointGenerator;
import de.fhdw.hfp418ie.vo.graphql.generator.endpoint.JavaSpringEndpointGenerator;
import de.fhdw.hfp418ie.vo.graphql.generator.schema.SchemaGenerator;
import de.fhdw.hfp418ie.vo.graphql.integration.IntegratorImpl;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.parser.CorrespondenceFileParser;
import de.fhdw.hfp418ie.vo.graphql.parser.DefaultCorrespondenceListener;
import de.fhdw.hfp418ie.vo.graphql.util.Pair;

import java.io.IOException;

/**
 * This facade provides direct access to the main tasks of the integration tool.
 */
public class GraphQLMergeFacade {

    /**
     * This method calculates the integrated internal model and the corresponding GraphQL schema definition.
     *
     * @param pathToCorrespondences Given path to the correspondence definition file.
     * @return Pair of the integrated internal model and the GraphQL schema definition as string.
     * @throws IOException If the original GraphQL endpoints can not be queried or the given path is not accessible.
     */
    public Pair<Model, String> calculateGraphQLSchemaAfterMerge(final String pathToCorrespondences, boolean pathisRelative) throws IOException {
        CorrespondenceFileParser parser = new DefaultCorrespondenceListener(pathToCorrespondences, pathisRelative);
        parser.parse();
        Model mergedModel = new IntegratorImpl().calculateModelIntegration("Integrated", "endpoint",
                parser.getModels(), parser.getCorrespondences());
        String result = SchemaGenerator.DEFAULT_GENERATOR.generateSchemaDefinition(mergedModel);
        return new Pair<>(mergedModel, result);
    }

    /**
     * This method generates the integrated endpoint at the given path, based on the given correspondence description.
     *
     * @param generationPath        Given root path for the generated endpoint.
     * @param pathToCorrespondences Given path to the correspondences description file.
     * @param rootPackageName       Given name for the root package of the generated endpoint.
     * @param projektName           Given name for the generated endpoint.
     * @throws IOException If the original GraphQL endpoints can not be queried or the given path is not accessible.
     */
    public void generateIntegratedEndpoint(final String generationPath, final String pathToCorrespondences,
                                           final String rootPackageName, final String projektName)
            throws IOException {
        Pair<Model, String> resultPair = this.calculateGraphQLSchemaAfterMerge(pathToCorrespondences, false);
        EndpointGenerator generator = new JavaSpringEndpointGenerator();
        generator.generateEndpoint(generationPath, resultPair.getSecond(), resultPair.getFirst(),
                rootPackageName, projektName);
    }
}
