package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.ArrayList;
import java.util.Collection;

public class ModelCorrespondenceImpl extends CorrespondenceImpl implements ModelCorrespondence {

    private final Collection<Type> relatedTypes;
    private final Collection<FieldCorrespondence> fieldCorrespondences;
    private final EqualityExpression equalityExpression;

    public ModelCorrespondenceImpl(Collection<Type> relatedTypes) {
        this(null, relatedTypes, new ArrayList<>(), null);
    }

    public ModelCorrespondenceImpl(String name, Collection<Type> relatedTypes) {
        this(name, relatedTypes, new ArrayList<>(), null);
    }

    public ModelCorrespondenceImpl(String name, Collection<Type> relatedTypes,
                                   Collection<FieldCorrespondence> fieldCorrespondences,
                                   EqualityExpression equalityExpression) {
        super(name);
        this.relatedTypes = relatedTypes;
        this.fieldCorrespondences = fieldCorrespondences;
        this.equalityExpression = equalityExpression;
    }

    @Override
    public Collection<Type> getRelatedTypes() {
        return relatedTypes;
    }

    @Override
    public Collection<FieldCorrespondence> getFieldCorrespondences() {
        return fieldCorrespondences;
    }

    @Override
    public EqualityExpression getEqualityExpression() {
        return equalityExpression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelCorrespondenceImpl that = (ModelCorrespondenceImpl) o;
        return Objects.equal(getRelatedTypes(), that.getRelatedTypes()) &&
                Objects.equal(getFieldCorrespondences(), that.getFieldCorrespondences()) &&
                Objects.equal(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRelatedTypes(), getFieldCorrespondences(), getName());
    }

    @Override
    public String toString() {
        return "ModelCorrespondence{" +
                "relatedItems=" + this.getRelatedTypes() +
                ", name=" + this.getName() +
                '}';
    }
}
