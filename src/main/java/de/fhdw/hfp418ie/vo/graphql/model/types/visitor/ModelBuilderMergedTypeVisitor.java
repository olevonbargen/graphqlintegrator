package de.fhdw.hfp418ie.vo.graphql.model.types.visitor;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.arguments.Argument;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.fields.MergedField;
import de.fhdw.hfp418ie.vo.graphql.model.fields.MergedFieldImpl;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ModelBuilderMergedTypeVisitor implements TypeVisitor {

    private final Model model;

    public ModelBuilderMergedTypeVisitor(final Model model) {
        this.model = model;
    }

    private void aggregateFields(final MergedType mergedType) {
        for (Type baseType : mergedType.getBaseTypes()) {
            for (Field field : baseType.getFields()) {
                if (mergedType.getCorrespondences().stream().anyMatch(corr -> corr.getRelatedFields().contains(field)))
                    continue;
                mergedType.addField(field);
                field.setType(mergedType);
                field.setNullable(true);
            }
        }
    }

    private void mergeFields(final MergedType mergedType) {
        for (FieldCorrespondence current : mergedType.getCorrespondences()) {
            Set<String> duplicateArgumentNames = new HashSet<>();
            Collection<String> argumentNames = current.getRelatedFields().stream()
                    .flatMap(field -> field.getArguments().stream()).map(Argument::getName)
                    .collect(Collectors.toList());
            for (String name : argumentNames) {
                if (!duplicateArgumentNames.contains(name)
                        && argumentNames.stream().filter(arg -> arg.equals(name)).count() > 1) {
                    duplicateArgumentNames.add(name);
                }
            }
            MergedField result = new MergedFieldImpl(current.getRelatedFields());
            for (Field field : result.getBaseFields()) {
                field.setName(current.getName());
                mergedType.getFields().remove(field);
                for (Argument argument : field.getArguments()) {
                    if (duplicateArgumentNames.contains(argument.getName())) {
                        argument.setName(argument.getType().getModel().getName() + "_" + argument.getType().getName()
                                + "_" + argument.getName());
                    }
                    result.getArguments().add(argument);
                }
            }
            mergedType.getFields().add(result);
            result.setType(mergedType);
            result.setName(current.getName());
            result.setResultType(this.calculateMergedResultType(mergedType, current));
            for (Field field : result.getBaseFields()) {
                field.setResultType(result.getResultType());
            }
            boolean nullable = false;
            for (Field field : current.getRelatedFields()) {
                nullable = nullable || field.isNullable();
            }
            result.setNullable(nullable);
        }
    }

    private Type calculateMergedResultType(final MergedType mergedType, final FieldCorrespondence correspondence) {
        Type result = null;
        boolean first = true;
        for (Field field : correspondence.getRelatedFields()) {
            Optional<Type> searchedResultType = mergedType.getModel().getTypes().stream()
                    .filter(type -> type.getBaseTypes().contains(field.getResultType())).findAny();
            if (!searchedResultType.isPresent()) {
                throw new RuntimeException("Could not resolve type " + field.getResultType().toString()
                        + " of field correspondence!");
            }
            if (first) {
                result = searchedResultType.get();
                first = false;
            } else if (!result.equals(searchedResultType.get())) {
                throw new RuntimeException("Field correspondence tries to merge fields with not matching " +
                        "result types " + result.toString() + " and " + searchedResultType.get().toString());
            }
        }
        return result;
    }

    private void calculateName(final MergedType mergedType) {
        StringBuilder nameBuilder = new StringBuilder();
        mergedType.getTypes().forEach(current -> {
            if (nameBuilder.length() > 0) {
                nameBuilder.append(MergedType.SUBTYPE_NAME_SEPARATOR);
            }
            nameBuilder.append(current.toString());
        });
        mergedType.setName(nameBuilder.toString());
    }

    @Override
    public void handleMergedType(MergedType type) {
        type.setModel(this.getModel());
        if (type.getName() == null) {
            this.calculateName(type);
        }
        this.aggregateFields(type);
        this.mergeFields(type);
    }

    @Override
    public void handleBaseType(BaseType type) {
        // nothing to do here
    }

    private Model getModel() {
        return this.model;
    }
}
