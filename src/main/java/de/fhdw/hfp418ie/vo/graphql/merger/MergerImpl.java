package de.fhdw.hfp418ie.vo.graphql.merger;


import de.fhdw.hfp418ie.vo.graphql.expression.*;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputType;

import java.util.*;
import java.util.stream.Collectors;

public abstract class MergerImpl<T> implements Merger<T> {

    private final EqualityExpression expression;


    public MergerImpl(final EqualityExpression equalityExpression) {
        this.expression = equalityExpression;
    }

    @Override
    public Collection<T> merge(Map<String, Collection<T>> elements) {
        Collection<T> result = new HashSet<>();
        final Map<T, Collection<T>> mergedElements = new HashMap<>();
        List<T> currentElements = new ArrayList<>();
        currentElements.addAll(elements.values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
        result.addAll(currentElements);
        boolean converged = false;
        while (!converged) {
            currentElements.clear();
            currentElements.addAll(result);
            Collection<T> preResult = new HashSet<>();
            outer:
            for (int i = 0; i < currentElements.size(); i++) {
                T inner = currentElements.get(i);
                for (int j = i + 1; j < currentElements.size(); j++) {
                    T outer = currentElements.get(j);
                    if (shallMerge(inner, outer, mergedElements)) {
                        T t = this.merge(inner, outer, mergedElements);
                        preResult.add(t);
                        for (int k = 0; k < currentElements.size(); k++) {
                            if (k != i && k != j) {
                                preResult.add(currentElements.get(k));
                            }
                        }
                        break outer;
                    }
                }
                preResult.add(inner);
            }
            if (preResult.equals(result)) {
                converged = true;
            } else {
                result = preResult;
            }
        }
        return result;
    }


    private boolean shallMerge(final T inner, final T outer,
                               final Map<T, Collection<T>> mergedElements) {
        return this.canEvaluate(inner, outer, this.getExpression(), mergedElements)
                && this.evaluate(inner, outer, this.getExpression(), mergedElements);
    }

    private T merge(final T inner, final T outer, Map<T, Collection<T>> mergedElements) {
        final T result = this.createMerge(inner, outer);
        final Collection<T> merge = new HashSet<>();
        merge.add(inner);
        merge.add(outer);
        if (mergedElements.containsKey(inner)) {
            merge.addAll(mergedElements.get(inner));
        }
        if (mergedElements.containsKey(outer)) {
            merge.addAll(mergedElements.get(outer));
        }
        mergedElements.put(result, merge);
        return result;
    }

    private Map<String, OutputType> getOriginalTypes(T t, Map<T, Collection<T>> mergedElements) {
        Map<String, OutputType> result = new HashMap<>();
        if (mergedElements.containsKey(t)) {
            for (T part : mergedElements.get(t)) {
                Map<String, OutputType> types = this.getOriginalTypes(part, mergedElements);
                for (String modelName : types.keySet()) {
                    if (result.containsKey(modelName)) {
                        if (!result.get(modelName).equals(types.get(modelName))) {
                            throw new Error("Found multiple types within the same merged Element " + t);
                        }
                    } else {
                        result.put(modelName, types.get(modelName));
                    }
                }
            }
        } else {
            result.put(this.getModelName(t), this.getOutputType(t));
        }
        return result;
    }

    private boolean isPartOf(final T x, final Expression expression, final Map<T, Collection<T>> mergedElements) {
        return expression.accept(new ExpressionVisitor() {
            @Override
            public boolean handleConstant(Constant constant) {
                return false;
            }

            @Override
            public boolean handleVariable(VariableExpression variable) {
                for (T current : getOriginalInstances(x, mergedElements)) {
                    if (getModelName(current).equals(variable.getModelName())) {
                        OutputType type = getOutputType(current);
                        if (type.getFields().stream()
                                .anyMatch(field -> field.getOriginalName().equals(variable.getFieldName()))) {
                            return true;
                        }
                    }
                }
                return false;
            }

            @Override
            public boolean handleEquality(EqualityExpression equality) {
                Iterator<Expression> expressionIterator = equality.getExpressions().iterator();
                int counter = 0;
                while (expressionIterator.hasNext()) {
                    final Expression expression = expressionIterator.next();
                    if (isPartOf(x, expression, mergedElements)) {
                        counter++;
                    }
                }
                return counter > 0;
            }

            @Override
            public boolean handleConjunction(ConjunctionExpression conjunction) {
                Iterator<BooleanExpression> expressionIterator = conjunction.getExpressions().iterator();
                int counter = 0;
                while (expressionIterator.hasNext()) {
                    final Expression expression = expressionIterator.next();
                    if (isPartOf(x, expression, mergedElements)) {
                        counter++;
                    }
                }
                return counter > 0;
            }

            @Override
            public boolean handleDisjunction(DisjunctionExpression disjunction) {
                Iterator<BooleanExpression> expressionIterator = disjunction.getExpressions().iterator();
                int counter = 0;
                while (expressionIterator.hasNext()) {
                    final Expression expression = expressionIterator.next();
                    if (isPartOf(x, expression, mergedElements)) {
                        counter++;
                    }
                }
                return counter > 0;
            }

            @Override
            public boolean handleConcatenation(ConcatenationExpression concatenation) {
                Iterator<StringExpression> expressionIterator = concatenation.getExpressions().iterator();
                int counter = 0;
                while (expressionIterator.hasNext()) {
                    final Expression expression = expressionIterator.next();
                    if (isPartOf(x, expression, mergedElements)) {
                        counter++;
                    }
                }
                return counter > 0;
            }
        });
    }

    private boolean canEvaluate(final T x, final T y, final Expression expression,
                                final Map<T, Collection<T>> mergedElements) {
        return expression.accept(new ExpressionVisitor() {
            @Override
            public boolean handleConstant(Constant constant) {
                return true;
            }

            @Override
            public boolean handleVariable(VariableExpression variable) {
                final Map<String, OutputType> typesX = getOriginalTypes(x, mergedElements);
                final Map<String, OutputType> typesY = getOriginalTypes(y, mergedElements);
                if (typesX.containsKey(variable.getModelName())) {
                    if (typesX.get(variable.getModelName()).getFields().stream()
                            .anyMatch(field -> field.getOriginalName().equals(variable.getFieldName()))) {
                        return true;
                    }
                }
                if (typesY.containsKey(variable.getModelName())) {
                    return typesY.get(variable.getModelName()).getFields().stream()
                            .anyMatch(field -> field.getOriginalName().equals(variable.getFieldName()));
                }
                return false;
            }

            @Override
            public boolean handleEquality(EqualityExpression equality) {
                Map<VariableExpression, Object> variablesX = resolveVariables(equality, x, mergedElements);
                Map<VariableExpression, Object> variablesY = resolveVariables(equality, y, mergedElements);
                if (variablesX.keySet().equals(variablesY.keySet())) {
                    boolean foundOverlap = false;
                    for (VariableExpression variable : variablesX.keySet()) {
                        if (variablesX.get(variable).equals(variablesY.get(variable))) {
                            foundOverlap = true;
                        }
                    }
                    if (!foundOverlap) {
                        return false;
                    }
                }
                Iterator<Expression> expressionIterator = equality.getExpressions().iterator();
                int counter = 0;
                while (expressionIterator.hasNext()) {
                    final Expression expression = expressionIterator.next();
                    if (canEvaluate(x, y, expression, mergedElements)) {
                        counter++;
                    }
                }
                return counter > 1 && isPartOf(x, equality, mergedElements) && isPartOf(y, equality, mergedElements);
            }

            @Override
            public boolean handleConjunction(ConjunctionExpression conjunction) {
                if (conjunction.getExpressions().size() > 2)
                    throw new Error("The prototype supports currently only expressions with max 2 " +
                            "direct sub expressions");
                Iterator<BooleanExpression> expressionIterator = conjunction.getExpressions().iterator();
                final BooleanExpression first = expressionIterator.hasNext() ? expressionIterator.next() : null;
                final BooleanExpression second = expressionIterator.hasNext() ? expressionIterator.next() : null;
                if (first != null && !canEvaluate(x, y, first, mergedElements)) {
                    if (second != null) {
                        if (canEvaluate(x, y, second, mergedElements)) {
                            return !evaluate(x, y, second, mergedElements);
                        }
                        return second != null && canEvaluate(x, y, second, mergedElements)
                                && !evaluate(x, y, second, mergedElements);
                    } else {
                        return false;
                    }
                }
                if (second != null && !canEvaluate(x, y, second, mergedElements)) {
                    return !evaluate(x, y, first, mergedElements);
                }
                return true;
            }

            @Override
            public boolean handleDisjunction(DisjunctionExpression disjunction) {
                if (disjunction.getExpressions().size() > 2)
                    throw new Error("The prototype supports currently only expressions with max 2 " +
                            "direct sub expressions");
                Iterator<BooleanExpression> expressionIterator = disjunction.getExpressions().iterator();
                final BooleanExpression first = expressionIterator.hasNext() ? expressionIterator.next() : null;
                final BooleanExpression second = expressionIterator.hasNext() ? expressionIterator.next() : null;
                if (first != null && !canEvaluate(x, y, first, mergedElements)) {
                    if (second != null) {
                        if (canEvaluate(x, y, second, mergedElements)) {
                            return evaluate(x, y, second, mergedElements);
                        }
                        return second != null && canEvaluate(x, y, second, mergedElements)
                                && evaluate(x, y, second, mergedElements);
                    } else {
                        return false;
                    }
                }
                if (second != null && !canEvaluate(x, y, second, mergedElements)) {
                    return evaluate(x, y, first, mergedElements);
                }
                return true;
            }

            @Override
            public boolean handleConcatenation(ConcatenationExpression concatenation) {
                if (concatenation.getExpressions().size() > 2)
                    throw new Error("The prototype supports currently only expressions with max 2 " +
                            "direct sub expressions");
                final Iterator<StringExpression> expressionIterator = concatenation.getExpressions().iterator();
                if (expressionIterator.hasNext()) {
                    final Expression first = expressionIterator.next();
                    if (!canEvaluate(x, y, first, mergedElements)) {
                        return false;
                    }
                    if (expressionIterator.hasNext()) {
                        return canEvaluate(x, y, expressionIterator.next(), mergedElements);
                    }
                }
                return true;
            }
        });
    }

    private Map<VariableExpression, Object> resolveVariables(Expression expression, T t,
                                                             Map<T, Collection<T>> mergedElements) {
        final Map<VariableExpression, Object> result = new HashMap<>();
        for (VariableExpression variable : expression.involvedVariables()) {
            try {
                Object resolvedVariable = resolveVariable(variable, getOriginalInstances(t, mergedElements));
                result.put(variable, resolvedVariable);
            } catch (Error e) {
                if (!e.getMessage().startsWith("Could not resolve ")) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }


    private Collection<T> getOriginalInstances(T t, Map<T, Collection<T>> mergedElements) {
        Collection<T> result = new HashSet<>();
        if (mergedElements.containsKey(t)) {
            for (T current : mergedElements.get(t)) {
                result.addAll(this.getOriginalInstances(current, mergedElements));
            }
        } else {
            result.add(t);
        }
        return result;
    }

    private boolean evaluate(final T x, final T y, BooleanExpression expression,
                             final Map<T, Collection<T>> mergedElements) {
        Map<VariableExpression, Object> variablesX = resolveVariables(expression, x, mergedElements);
        Map<VariableExpression, Object> variablesY = resolveVariables(expression, y, mergedElements);
        Map<VariableExpression, Object> variables = new HashMap<>();
        boolean putX = false;
        boolean putY = false;
        for (VariableExpression variable : expression.involvedVariables()) {
            if (variablesX.containsKey(variable)) {
                if (variablesY.containsKey(variable)) {
                    if (variablesX.get(variable).equals(variablesY.get(variable))) {
                        putX = true;
                        putY = true;
                        variables.put(variable, variablesX.get(variable));
                    } else {
                        // two different options for resolving the variable
                    }
                } else {
                    putX = true;
                    variables.put(variable, variablesX.get(variable));
                }
            } else if (variablesY.containsKey(variable)) {
                putY = true;
                variables.put(variable, variablesY.get(variable));
            }
        }
        return putX && putY && expression.evaluate(variables);
    }

    protected abstract T createMerge(T inner, T outer);

    protected abstract Object resolveVariable(VariableExpression variable, Collection<T> inputs);

    protected abstract String getModelName(T t);

    protected abstract OutputType getOutputType(T t);

    private EqualityExpression getExpression() {
        return this.expression;
    }
}
