package de.fhdw.hfp418ie.vo.graphql.integration;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilder;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilderImpl;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;

import java.util.*;

public class IntegratorImpl implements Integrator {

    @Override
    public Model calculateModelIntegration(String name, String endpoint, Collection<Model> models,
                                           Collection<ModelCorrespondence> correspondences) {
        final Collection<Type> types = this.calculateTypes(correspondences, this.aggregateAllTypes(models));
        ModelBuilder builder = new ModelBuilderImpl(name, endpoint);
        return builder.addAllTypes(types).buildMerged();
    }

    private Collection<Type> aggregateAllTypes(final Collection<Model> models) {
        Collection<Type> result = new ArrayList<>();
        for (final Model model : models) {
            result.addAll(model.getTypes());
        }
        return result;
    }

    private Collection<Type> calculateTypes(final Collection<ModelCorrespondence> correspondences,
                                            final Collection<Type> unmergedTypes) {
        Collection<Type> result = new HashSet<>();
        Map<Type, MergedType> mergedTypeMap = new HashMap<>();
        for (ModelCorrespondence correspondence : correspondences) {
            this.calculateMergedType(correspondence, unmergedTypes, mergedTypeMap);
        }
        for (Type key : mergedTypeMap.keySet()) {
            result.add(mergedTypeMap.get(key));
        }
        result.addAll(unmergedTypes);
        return result;
    }

    private void calculateMergedType(final ModelCorrespondence correspondence, final Collection<Type> unmergedTypes,
                                     final Map<Type, MergedType> mergedTypeMap) {
        MergedType mergedType = TypeFactory.FACTORY.createMergedType(correspondence.getName(),
                correspondence.getFieldCorrespondences(), correspondence.getEqualityExpression());
        Collection<Type> types = new HashSet<>();
        for (Type type : correspondence.getRelatedTypes()) {
            if (mergedTypeMap.containsKey(type)) {
                types.addAll(this.handleAlreadyMergedType(type, mergedType, mergedTypeMap));
            } else {
                types.add(this.handleUnmergedType(type, mergedType, unmergedTypes, mergedTypeMap));
            }
        }
        mergedType.addTypes(types);
        correspondence.getFieldCorrespondences().stream()
                .filter(corr -> !mergedType.getCorrespondences().contains(corr))
                .forEach(corr -> mergedType.getCorrespondences().add(corr));
    }

    private Collection<Type> handleAlreadyMergedType(final Type type, final MergedType newType,
                                                     final Map<Type, MergedType> mergedTypeMap) {
        final Collection<Type> result = new HashSet<>();
        mergedTypeMap.get(type).getCorrespondences().stream()
                .filter(corr -> !newType.getCorrespondences().contains(corr))
                .forEach(corr -> newType.getCorrespondences().add(corr));
        final EqualityExpression old = mergedTypeMap.get(type).getEqualityExpression();
        if (newType.getEqualityExpression() == null && old != null) {
            newType.setEqualityExpression(old);
        }
        for (Type current : mergedTypeMap.get(type).getTypes()) {
            mergedTypeMap.put(current, newType);
            result.add(current);
        }
        return result;
    }

    private Type handleUnmergedType(final Type type, final MergedType newType, final Collection<Type> unmergedTypes,
                                    final Map<Type, MergedType> mergedTypeMap) {
        if (!unmergedTypes.contains(type)) {
            throw new Error("Found unknown type " + type.toString());
        }
        unmergedTypes.remove(type);
        mergedTypeMap.put(type, newType);
        return type;
    }


}
