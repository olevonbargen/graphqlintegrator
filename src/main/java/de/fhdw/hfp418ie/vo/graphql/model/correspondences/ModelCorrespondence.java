package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.Collection;

public interface ModelCorrespondence extends Correspondence {

    Collection<Type> getRelatedTypes();

    Collection<FieldCorrespondence> getFieldCorrespondences();

    EqualityExpression getEqualityExpression();

}
