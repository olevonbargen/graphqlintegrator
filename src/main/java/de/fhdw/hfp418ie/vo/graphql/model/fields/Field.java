package de.fhdw.hfp418ie.vo.graphql.model.fields;

import de.fhdw.hfp418ie.vo.graphql.model.arguments.Argument;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.Collection;

public interface Field {

    String getOriginalName();

    String getName();

    void setName(String name);

    Type getType();

    void setType(Type type);

    void setResultType(Type type);

    Type getResultType();

    void setNullable(boolean nullable);

    boolean isNullable();

    String generateDescription(boolean duplicateName);

    Collection<Argument> getArguments();

    void addArgument(Argument argument);

    boolean isList();

    boolean isMerged();

}
