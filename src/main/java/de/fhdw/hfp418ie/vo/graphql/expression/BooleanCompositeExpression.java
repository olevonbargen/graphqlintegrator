package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;

public abstract class BooleanCompositeExpression implements BooleanExpression, CompositeExpression {

    public BooleanCompositeExpression() {
        this.expressions = new ArrayList<>();
    }

    private Collection<BooleanExpression> expressions;

    @Override
    public void addExpression(final Expression expression) {
        if (expression instanceof BooleanExpression) {
            this.getExpressions().add((BooleanExpression) expression);
        } else {
            throw new Error("Subexpression from BooleanCompositeExpression must be BooleanExpression but found "
                    + expression);
        }
    }

    @Override
    public Collection<VariableExpression> involvedVariables() {
        Collection<VariableExpression> result = new ArrayList<>();
        for (Expression current : this.getExpressions()) {
            result.addAll(current.involvedVariables());
        }
        return result;
    }

    public Collection<BooleanExpression> getExpressions() {
        return this.expressions;
    }

    public void setExpressions(final Collection<BooleanExpression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BooleanCompositeExpression that = (BooleanCompositeExpression) o;
        return Objects.equal(getExpressions(), that.getExpressions());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getExpressions());
    }
}
