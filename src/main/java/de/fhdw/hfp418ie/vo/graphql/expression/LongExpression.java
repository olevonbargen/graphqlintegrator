package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.Map;

public interface LongExpression extends Expression {

    @Override
    Long evaluate(Map<VariableExpression, Object> variables);

}
