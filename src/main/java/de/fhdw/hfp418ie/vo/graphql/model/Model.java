package de.fhdw.hfp418ie.vo.graphql.model;

import de.fhdw.hfp418ie.vo.graphql.model.output.OutputModel;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.Collection;

public interface Model {

    String getName();

    String getEndpoint();

    Collection<Type> getTypes();

    Collection<OutputModel> generateOutput();

}
