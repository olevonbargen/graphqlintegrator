package de.fhdw.hfp418ie.vo.graphql.model.output;

public abstract class OutputEntity {

    private String name;

    OutputEntity() {
    }

    OutputEntity(final String name) {
        this.name = name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
