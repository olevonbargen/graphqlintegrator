package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class ConcatenationExpression implements StringExpression, CompositeExpression {

    public ConcatenationExpression() {
        this.expressions = new ArrayList<>();
    }

    private Collection<StringExpression> expressions;

    @Override
    public String evaluate(Map<VariableExpression, Object> variables) {
        StringBuilder result = new StringBuilder();
        for (StringExpression expression : this.getExpressions()) {
            result.append(expression.evaluate(variables));
        }
        return result.toString();
    }

    @Override
    public Collection<VariableExpression> involvedVariables() {
        Collection<VariableExpression> result = new ArrayList<>();
        for (Expression current : this.getExpressions()) {
            result.addAll(current.involvedVariables());
        }
        return result;
    }

    @Override
    public void addExpression(final Expression expression) {
        if (expression instanceof StringExpression) {
            this.getExpressions().add((StringExpression) expression);
        } else {
            throw new Error("Subexpressions from ConcatenationExpressions must be StringExpressions but found " +
                    expression);
        }
    }

    @Override
    public boolean accept(ExpressionVisitor visitor) {
        return visitor.handleConcatenation(this);
    }

    public Collection<StringExpression> getExpressions() {
        return this.expressions;
    }

    public void setExpressions(final Collection<StringExpression> expressions) {
        this.expressions = expressions;
    }
}
