package de.fhdw.hfp418ie.vo.graphql.model.fields;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;

public abstract class BaseFieldImpl extends FieldImpl implements BaseField {

    private final BaseType baseType;
    private String originalName;

    BaseFieldImpl(final BaseType baseType) {
        super();
        this.baseType = baseType;
        this.originalName = null;
    }

    @Override
    public String getOriginalName() {
        if (this.originalName == null) {
            return this.getName();
        }
        return this.originalName;
    }

    @Override
    public BaseType getBaseType() {
        return this.baseType;
    }

    @Override
    protected String handleDuplicateName() {
        return this.getBaseType().getModel().getName() + typeFieldNameSeparator + this.getName();
    }

    @Override
    protected void handleNameChange(String old) {
        if (originalName == null) {
            this.originalName = old;
        }
    }

    @Override
    public boolean isMerged() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseFieldImpl field = (BaseFieldImpl) o;
        return Objects.equal(this.toString(), field.toString());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(toString());
    }

    @Override
    public String toString() {
        String typeModelName = this.getType() == null || this.getType().getModel() == null ?
                "currently not merged" : this.getType().getModel().getName();
        String baseTypeModelName = this.getBaseType().getModel() == null ?
                "model of base type not set yet" : this.getBaseType().getModel().getName();
        return typeModelName + "." + baseTypeModelName + "." + this.getBaseType().getName() + "." + this.getName();
    }
}
