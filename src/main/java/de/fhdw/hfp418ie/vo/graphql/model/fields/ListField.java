package de.fhdw.hfp418ie.vo.graphql.model.fields;

public interface ListField extends BaseField {

    boolean isElementNullable();

    void setElementNullable(boolean nullable);

}
