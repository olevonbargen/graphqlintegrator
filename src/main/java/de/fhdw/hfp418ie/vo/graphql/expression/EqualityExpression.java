package de.fhdw.hfp418ie.vo.graphql.expression;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class EqualityExpression implements BooleanExpression, CompositeExpression {

    public EqualityExpression() {
        this.expressions = new ArrayList<>();
    }

    private Collection<Expression> expressions;

    @Override
    public Boolean evaluate(Map<VariableExpression, Object> variables) {
        boolean first = true;
        boolean compared = false;
        Expression current = null;
        outer:
        for (Expression element : this.getExpressions()) {
            for (VariableExpression variable : element.involvedVariables()) {
                if (!variables.containsKey(variable)) {
                    continue outer;
                }
            }
            if (first) {
                current = element;
                first = false;
            } else {
                compared = true;
                if (!current.evaluate(variables).equals(element.evaluate(variables))) {
                    return false;
                }
            }
        }
        return compared;
    }

    @Override
    public Collection<VariableExpression> involvedVariables() {
        Collection<VariableExpression> result = new ArrayList<>();
        for (Expression current : this.getExpressions()) {
            result.addAll(current.involvedVariables());
        }
        return result;
    }

    @Override
    public void addExpression(final Expression expression) {
        this.getExpressions().add(expression);
    }

    @Override
    public boolean accept(ExpressionVisitor visitor) {
        return visitor.handleEquality(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EqualityExpression that = (EqualityExpression) o;
        return Objects.equal(getExpressions(), that.getExpressions());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getExpressions());
    }

    @Override
    public String toString() {
        return "EqualityExpression{" +
                "expressions=" + expressions +
                '}';
    }

    public Collection<Expression> getExpressions() {
        return this.expressions;
    }

    public void setExpressions(final Collection<Expression> expressions) {
        this.expressions = expressions;
    }
}
