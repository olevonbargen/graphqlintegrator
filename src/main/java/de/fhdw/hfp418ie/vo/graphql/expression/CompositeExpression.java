package de.fhdw.hfp418ie.vo.graphql.expression;

public interface CompositeExpression extends Expression {

    void addExpression(final Expression expression);

}
