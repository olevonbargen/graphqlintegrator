package de.fhdw.hfp418ie.vo.graphql.model.fields;

import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;

public class ElementBaseFieldImpl extends BaseFieldImpl implements ElementField {

    public ElementBaseFieldImpl(BaseType baseType) {
        super(baseType);
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public String generateFieldType() {
        return this.getResultType().getName();
    }
}
