package de.fhdw.hfp418ie.vo.graphql;

import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCallerFactory;

import java.io.IOException;

public class Main {

    private static final String pathToCorrespondences = "/Users/past/Documents/dev/graphqlintegrator/src/demoEndpoints/Example.corr";
    private static final String pathForGeneration = "/Users/past/Documents/dev/graphqlintegrator/gen/";
    private static final String rootPackageName = "com.example.graphql";
    private static final String projectName = "Integ";

    public static void main(String[] args) {
        GraphQLCallerFactory.initalize();
        GraphQLMergeFacade facade = new GraphQLMergeFacade();
        try {
            facade.generateIntegratedEndpoint(pathForGeneration, pathToCorrespondences, rootPackageName, projectName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Generation of the integrated endpoint to " + pathForGeneration + projectName +
                " finished successfully!");
    }

}
