package de.fhdw.hfp418ie.vo.graphql.generator.endpoint;

import de.fhdw.hfp418ie.vo.graphql.model.Model;

import java.io.IOException;

/**
 * This generator generates the integrated endpoint after the calculation of the integration.
 */
public interface EndpointGenerator {

    /**
     * Generates the endpoint into a given root path, with a given schema and a given model.
     * Additionally the root package name and the endpoints name have to be specified.
     *
     * @param rootPath        Given root path.
     * @param graphQlSchema   Given GraphQL schema of the integration.
     * @param model           Given model of the integration.
     * @param rootPackageName Given root package name.
     * @param name            Given name of the integrated endpoint.
     * @throws IOException If the endpoint generator can not write the necessary files or read the configuration file.
     */
    void generateEndpoint(String rootPath, String graphQlSchema, Model model, String rootPackageName, String name)
            throws IOException;

}
