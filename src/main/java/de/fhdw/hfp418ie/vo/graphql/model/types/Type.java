package de.fhdw.hfp418ie.vo.graphql.model.types;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.TypeVisitor;

import java.util.Collection;

public interface Type {

    String MODEL_NAME_SEPARATOR = ".";

    void addField(Field field);

    Collection<Field> getFields();

    void setModel(Model model);

    Model getModel();

    String getName();

    Collection<Type> getBaseTypes();

    void accept(final TypeVisitor visitor);
}
