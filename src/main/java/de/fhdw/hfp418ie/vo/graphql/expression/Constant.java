package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.ArrayList;
import java.util.Collection;

public abstract class Constant implements Expression {

    @Override
    public Collection<VariableExpression> involvedVariables() {
        return new ArrayList<>();
    }

    @Override
    public boolean accept(ExpressionVisitor visitor) {
        return visitor.handleConstant(this);
    }
}
