package de.fhdw.hfp418ie.vo.graphql.model.arguments;

import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

public interface Argument {

    String getName();

    void setName(String name);

    Type getType();

    void setType(final Type type);

    boolean isNullable();

    void setNullable(boolean nullable);

    String getOriginalName();

}
