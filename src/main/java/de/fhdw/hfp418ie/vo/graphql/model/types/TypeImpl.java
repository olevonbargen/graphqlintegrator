package de.fhdw.hfp418ie.vo.graphql.model.types;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;

import java.util.Collection;
import java.util.HashSet;

public abstract class TypeImpl implements Type {

    private Model model;
    private Collection<Field> fields;

    TypeImpl() {
        this.fields = new HashSet<>();
    }

    @Override
    public void addField(Field field) {
        this.getFields().add(field);
    }

    @Override
    public Collection<Field> getFields() {
        return this.fields;
    }

    @Override
    public void setModel(Model model) {
        this.model = model;
    }

    @Override
    public Model getModel() {
        return this.model;
    }
}
