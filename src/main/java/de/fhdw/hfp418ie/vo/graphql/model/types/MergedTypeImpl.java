package de.fhdw.hfp418ie.vo.graphql.model.types;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.TypeVisitor;

import java.util.Collection;
import java.util.HashSet;

public class MergedTypeImpl extends TypeImpl implements MergedType {

    private String name;
    private final Collection<Type> baseTypes;
    private final Collection<FieldCorrespondence> correspondences;
    private EqualityExpression equalityExpression;

    public MergedTypeImpl(final Collection<FieldCorrespondence> correspondences, EqualityExpression equalityExpression) {
        this.correspondences = correspondences;
        this.baseTypes = new HashSet<>();
        this.equalityExpression = equalityExpression;
    }

    public MergedTypeImpl(final String name, final Collection<FieldCorrespondence> correspondences, EqualityExpression equalityExpression) {
        this(correspondences, equalityExpression);
        this.setName(name);
    }

    @Override
    public void accept(final TypeVisitor visitor) {
        visitor.handleMergedType(this);
    }

    @Override
    public Collection<Type> getBaseTypes() {
        Collection<Type> result = new HashSet<>();
        for (Type type : this.getTypes()) {
            result.addAll(type.getBaseTypes());
        }
        return result;
    }

    @Override
    public void addTypes(Collection<Type> types) {
        this.getTypes().addAll(types);
    }

    @Override
    public Collection<Type> getTypes() {
        return this.baseTypes;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Collection<FieldCorrespondence> getCorrespondences() {
        return correspondences;
    }

    @Override
    public EqualityExpression getEqualityExpression() {
        return equalityExpression;
    }

    @Override
    public void setEqualityExpression(EqualityExpression equalityExpression) {
        this.equalityExpression = equalityExpression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MergedTypeImpl that = (MergedTypeImpl) o;
        return Objects.equal(getModel(), that.getModel()) &&
                Objects.equal(getName(), that.getName()) &&
                Objects.equal(getBaseTypes(), that.getBaseTypes());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getBaseTypes());
    }

    @Override
    public String toString() {
        if (this.getModel() == null) {
            return "MergedType under construction";
        }
        return this.getModel().toString() + Type.MODEL_NAME_SEPARATOR + this.getName();
    }
}
