package de.fhdw.hfp418ie.vo.graphql.model.fields;

import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;

public interface BaseField extends Field {
    BaseType getBaseType();
}
