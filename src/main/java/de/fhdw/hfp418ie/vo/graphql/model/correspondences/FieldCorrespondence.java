package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

import de.fhdw.hfp418ie.vo.graphql.model.fields.BaseField;

import java.util.Collection;

public interface FieldCorrespondence extends Correspondence {

    Collection<BaseField> getRelatedFields();

}
