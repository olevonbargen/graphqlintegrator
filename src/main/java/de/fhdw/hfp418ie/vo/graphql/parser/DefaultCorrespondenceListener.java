package de.fhdw.hfp418ie.vo.graphql.parser;

import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCallerFactory;
import de.fhdw.hfp418ie.vo.graphql.converter.GraphQLConverter;
import de.fhdw.hfp418ie.vo.graphql.expression.*;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondenceImpl;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondenceImpl;
import de.fhdw.hfp418ie.vo.graphql.model.fields.BaseField;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import graphql.schema.GraphQLSchema;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class DefaultCorrespondenceListener extends CorrespondencesBaseListener implements CorrespondenceFileParser {

    private final String path;
    private final Map<String, Model> models;
    private final Collection<ModelCorrespondence> correspondences;
    private String lastUrl;
    private String lastModelAlias;
    private Collection<Type> relatedTypes;
    private Type lastType;
    private Field lastField;
    private String lastFieldCorrespondenceAlias;
    private String lastModelCorrespondenceAlias;
    private Collection<FieldCorrespondence> fieldCorrespondences;
    private Collection<BaseField> relatedFields;
    private EqualityExpression equalityExpression;
    private Stack<CompositeExpression> expressionStack;
    private boolean pathIsRelative;

    public DefaultCorrespondenceListener(final String pathToCorrespondenceFile, boolean pathIsRelative) {
        this.path = pathToCorrespondenceFile;
        this.models = new HashMap<>();
        this.correspondences = new ArrayList<>();
        this.pathIsRelative = pathIsRelative;
    }

    @Override
    public void enterUrl(CorrespondencesParser.UrlContext ctx) {
        this.setLastUrl(ctx.getText());
    }

    @Override
    public void enterModelAlias(CorrespondencesParser.ModelAliasContext ctx) {
        this.setLastModelAlias(ctx.getText());
    }

    @Override
    public void exitEndpoint(CorrespondencesParser.EndpointContext ctx) {
        GraphQLSchema schema;
        GraphQLCaller caller = GraphQLCallerFactory.getInstance().create();
        try {
            schema = caller.getGraphQLSchema(this.getLastUrl());
        } catch (IOException e) {
            throw new RuntimeException("Could not fetch schema from defined endpoint " + this.getLastUrl(), e);
        }
        Model model = GraphQLConverter.INSTANCE.convert(this.getLastModelAlias(), this.getLastUrl(), schema).getFirst();
        this.getModelMap().put(model.getName(), model);
    }

    @Override
    public void enterModelCorrespondence(CorrespondencesParser.ModelCorrespondenceContext ctx) {
        this.setRelatedTypes(new ArrayList<>());
        this.setFieldCorrespondences(new ArrayList<>());
        this.setLastModelCorrespondenceAlias(null);
        this.expressionStack = new Stack<>();
        this.equalityExpression = null;
    }

    @Override
    public void enterTypeName(CorrespondencesParser.TypeNameContext ctx) {
        Optional<Type> typeSearch = this.getModelMap().get(this.getLastModelAlias()).getTypes().stream()
                .filter(type -> type.getName().equals(ctx.getText())).findAny();
        if (!typeSearch.isPresent()) {
            throw new RuntimeException("Could not find related type " + ctx.getText() + " within the model "
                    + this.getLastModelAlias());
        }
        if (ctx.getParent().getParent().getRuleIndex() == CorrespondencesParser.RULE_modelCorrespondence) {
            this.getRelatedTypes().add(typeSearch.get());
        } else if (ctx.getParent().getParent().getRuleIndex() == CorrespondencesParser.RULE_fieldIdentifier) {
            this.setLastType(typeSearch.get());
        }
    }

    @Override
    public void enterCorrespondenceAlias(CorrespondencesParser.CorrespondenceAliasContext ctx) {
        if (ctx.getParent().getRuleIndex() == CorrespondencesParser.RULE_modelCorrespondence) {
            this.setLastModelCorrespondenceAlias(ctx.getText());
        } else if (ctx.getParent().getRuleIndex() == CorrespondencesParser.RULE_fieldCorrespondence) {
            this.setLastFieldCorrespondenceAlias(ctx.getText());
        }
    }

    @Override
    public void enterFieldCorrespondence(CorrespondencesParser.FieldCorrespondenceContext ctx) {
        this.setRelatedFields(new ArrayList<>());
        this.setLastFieldCorrespondenceAlias(null);
        this.equalityExpression = null;
    }

    @Override
    public void enterFieldName(CorrespondencesParser.FieldNameContext ctx) {
        Optional<Field> fieldSearch = this.getLastType().getFields().stream()
                .filter(field -> field.getName().equals(ctx.getText())).findAny();
        if (!fieldSearch.isPresent()) {
            throw new RuntimeException("Could not find related field " + ctx.getText() + " within the type "
                    + this.getLastType().toString());
        }
        if (!(fieldSearch.get() instanceof BaseField)) {
            throw new RuntimeException("Found merged field in original configuration of field " + ctx.getText() +
                    " within the type " + this.getLastType().toString());
        }
        if (ctx.getParent().getParent().getRuleIndex() == CorrespondencesParser.RULE_fieldCorrespondence) {
            this.getRelatedFields().add((BaseField) fieldSearch.get());
        } else if (ctx.getParent().getParent().getRuleIndex() == CorrespondencesParser.RULE_variable) {
            this.lastField = fieldSearch.get();
        }
    }

    @Override
    public void exitEqualityDescription(CorrespondencesParser.EqualityDescriptionContext ctx) {
        if (!(this.expressionStack.size() == 1 &&
                this.expressionStack.peek().getClass() == EqualityExpression.class)) {
            throw new Error("Invalid equality description found! Require exactly one expression - found " +
                    this.expressionStack.size());
        }
        this.equalityExpression = (EqualityExpression) this.expressionStack.pop();
    }

    @Override
    public void enterEqualityExpression(CorrespondencesParser.EqualityExpressionContext ctx) {
        final EqualityExpression current = new EqualityExpression();
        if (!this.expressionStack.isEmpty()) {
            this.expressionStack.peek().addExpression(current);
        }
        this.expressionStack.push(current);
    }

    @Override
    public void exitEqualityExpression(CorrespondencesParser.EqualityExpressionContext ctx) {
        if (this.expressionStack.size() > 1) {
            this.equalityExpression = (EqualityExpression) this.expressionStack.pop();
        }
    }

    @Override
    public void enterConcatenationExpression(CorrespondencesParser.ConcatenationExpressionContext ctx) {
        ConcatenationExpression current = new ConcatenationExpression();
        this.expressionStack.peek().addExpression(current);
        this.expressionStack.push(current);
    }

    @Override
    public void exitConcatenationExpression(CorrespondencesParser.ConcatenationExpressionContext ctx) {
        this.expressionStack.pop();
    }

    @Override
    public void enterConjunctionExpression(CorrespondencesParser.ConjunctionExpressionContext ctx) {
        final ConjunctionExpression current = new ConjunctionExpression();
        this.expressionStack.peek().addExpression(current);
        this.expressionStack.push(current);
    }

    @Override
    public void exitConjunctionExpression(CorrespondencesParser.ConjunctionExpressionContext ctx) {
        this.expressionStack.pop();
    }

    @Override
    public void enterDisjunctionExpression(CorrespondencesParser.DisjunctionExpressionContext ctx) {
        final DisjunctionExpression current = new DisjunctionExpression();
        this.expressionStack.peek().addExpression(current);
        this.expressionStack.push(current);
    }

    @Override
    public void exitDisjunctionExpression(CorrespondencesParser.DisjunctionExpressionContext ctx) {
        this.expressionStack.pop();
    }

    @Override
    public void enterBoolConstant(CorrespondencesParser.BoolConstantContext ctx) {
        if (ctx.getText().equals("true")) {
            this.expressionStack.peek().addExpression(new BooleanConstant(true));
        } else if (ctx.getText().equals("false")) {
            this.expressionStack.peek().addExpression(new BooleanConstant(false));
        } else {
            throw new Error("Found Boolean constant with text " + ctx.getText());
        }
    }

    @Override
    public void enterIntegerConstant(CorrespondencesParser.IntegerConstantContext ctx) {
        Integer value = new Integer(ctx.getText());
        this.expressionStack.peek().addExpression(new IntegerConstant(value));
    }

    @Override
    public void enterFloatConstant(CorrespondencesParser.FloatConstantContext ctx) {
        Float value = new Float(ctx.getText());
        this.expressionStack.peek().addExpression(new FloatConstant(value));
    }

    @Override
    public void enterStringConstant(CorrespondencesParser.StringConstantContext ctx) {
        this.expressionStack.peek().addExpression(
                new StringConstant(ctx.getText().substring(1, ctx.getText().length() - 1)));
    }

    @Override
    public void exitVariable(CorrespondencesParser.VariableContext ctx) {
        if (this.lastField.getResultType().getName().equals("String")) {
            this.expressionStack.peek().addExpression(new StringVariable(this.lastModelAlias, this.lastType.getName(),
                    this.lastField.getName()));
        } else {
            this.expressionStack.peek().addExpression(new VariableExpression(this.lastModelAlias,
                    this.lastType.getName(), this.lastField.getName()));
        }
    }

    @Override
    public void exitFieldCorrespondence(CorrespondencesParser.FieldCorrespondenceContext ctx) {
        FieldCorrespondence result = new FieldCorrespondenceImpl(this.getLastFieldCorrespondenceAlias());
        result.getRelatedFields().addAll(this.getRelatedFields());
        this.getFieldCorrespondences().add(result);
    }

    @Override
    public void exitModelCorrespondence(CorrespondencesParser.ModelCorrespondenceContext ctx) {
        this.getCorrespondences().add(new ModelCorrespondenceImpl(this.getLastModelCorrespondenceAlias(),
                this.getRelatedTypes(), this.getFieldCorrespondences(), this.equalityExpression));
    }

    @Override
    public void exitModelCorrespondences(CorrespondencesParser.ModelCorrespondencesContext ctx) {
        Collection<String> defaultTypes = new ArrayList<>(Arrays.asList(GraphQLConverter.DEFAULT_GRAPHQL_TYPES));
        defaultTypes.addAll(Arrays.asList(GraphQLConverter.DEFAULT_SCALAR_TYPES));
        defaultTypes.stream()
                .map(defaultType -> this.getModels().stream().flatMap(model ->
                        model.getTypes().stream()).filter(type -> type.getName().equals(defaultType))
                        .collect(Collectors.toList()))
                .forEach(list -> {
                    if (list.size() > 1)
                        this.getCorrespondences().add(new ModelCorrespondenceImpl(list.get(0).getName(), list));
                });
    }

    @Override
    public void parse() throws IOException {
        CharStream stream;
        if (pathIsRelative) {
            final URL filePath = this.getClass().getClassLoader().getResource(this.getPath());
            if (filePath == null) {
                throw new FileNotFoundException("File for relative path " + this.getPath() + " not found!");
            }
            stream = CharStreams.fromFileName(filePath.getFile());
        } else {
            stream = CharStreams.fromFileName(getPath());
        }
        final CorrespondencesLexer lexer = new CorrespondencesLexer(stream);
        final CorrespondencesParser parser = new CorrespondencesParser(new CommonTokenStream(lexer));
        new ParseTreeWalker().walk(this, parser.correspondences());
    }

    @Override
    public Collection<Model> getModels() {
        return this.models.values();
    }

    @Override
    public Collection<ModelCorrespondence> getCorrespondences() {
        return this.correspondences;
    }

    private Map<String, Model> getModelMap() {
        return this.models;
    }

    private String getPath() {
        return this.path;
    }

    private String getLastUrl() {
        return lastUrl;
    }

    private void setLastUrl(String lastUrl) {
        this.lastUrl = lastUrl;
    }

    private String getLastModelAlias() {
        return lastModelAlias;
    }

    private void setLastModelAlias(String lastModelAlias) {
        this.lastModelAlias = lastModelAlias;
    }

    private Collection<Type> getRelatedTypes() {
        return relatedTypes;
    }

    private void setRelatedTypes(Collection<Type> relatedTypes) {
        this.relatedTypes = relatedTypes;
    }

    private Type getLastType() {
        return lastType;
    }

    private void setLastType(Type lastType) {
        this.lastType = lastType;
    }

    private String getLastFieldCorrespondenceAlias() {
        return lastFieldCorrespondenceAlias;
    }

    private void setLastFieldCorrespondenceAlias(String lastFieldCorrespondenceAlias) {
        this.lastFieldCorrespondenceAlias = lastFieldCorrespondenceAlias;
    }

    private String getLastModelCorrespondenceAlias() {
        return lastModelCorrespondenceAlias;
    }

    private void setLastModelCorrespondenceAlias(String lastModelCorrespondenceAlias) {
        this.lastModelCorrespondenceAlias = lastModelCorrespondenceAlias;
    }

    private Collection<FieldCorrespondence> getFieldCorrespondences() {
        return fieldCorrespondences;
    }

    private void setFieldCorrespondences(Collection<FieldCorrespondence> fieldCorrespondences) {
        this.fieldCorrespondences = fieldCorrespondences;
    }

    private Collection<BaseField> getRelatedFields() {
        return relatedFields;
    }

    private void setRelatedFields(Collection<BaseField> relatedFields) {
        this.relatedFields = relatedFields;
    }
}
