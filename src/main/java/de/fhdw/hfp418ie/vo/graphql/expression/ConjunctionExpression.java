package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.Map;

public class ConjunctionExpression extends BooleanCompositeExpression {

    public ConjunctionExpression() {
        super();
    }

    @Override
    public Boolean evaluate(Map<VariableExpression, Object> variables) {
        boolean result = true;
        boolean skipped = false;
        outer:
        for (BooleanExpression expression : this.getExpressions()) {
            for (VariableExpression variable : expression.involvedVariables()) {
                if (!variables.containsKey(variable)) {
                    skipped = true;
                    continue outer;
                }
            }
            result = expression.evaluate(variables);
            if (!result) {
                break;
            }
        }
        if (skipped && result) {
            throw new Error("Could not evaluate the expression " + this + " completely!");
        }
        return result;
    }

    @Override
    public boolean accept(ExpressionVisitor visitor) {
        return visitor.handleConjunction(this);
    }

    @Override
    public String toString() {
        return "ConjunctionExpression{" +
                "expressions=" + this.getExpressions() +
                '}';
    }
}
