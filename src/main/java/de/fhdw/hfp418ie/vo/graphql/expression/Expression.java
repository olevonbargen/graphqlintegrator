package de.fhdw.hfp418ie.vo.graphql.expression;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Collection;
import java.util.Map;

/**
 * Abstract expression for formalizing a way to express corresponding instances.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
public interface Expression {

    /**
     * Evaluates the expression and returns the result of the evaluation.
     *
     * @param variables Given variable assignments.
     * @return Result of the evaluation.
     */
    Object evaluate(Map<VariableExpression, Object> variables);

    /**
     * Collects all involved variables of the expression including its subexpressions.
     *
     * @return Collection of all involved variables expressions.
     */
    Collection<VariableExpression> involvedVariables();

    /**
     * Accepts a given visitor according to the visitor development pattern.
     *
     * @param visitor Given visitor.
     * @return The result of the visitors evaluation.
     */
    boolean accept(final ExpressionVisitor visitor);

}
