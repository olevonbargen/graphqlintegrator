package de.fhdw.hfp418ie.vo.graphql.model.output;

public class OutputArgument extends OutputEntity {

    private String originalName;
    private boolean string;

    public OutputArgument() {
        super();
    }

    public OutputArgument(final String name, final String originalName, final boolean string) {
        super(name);
        this.setOriginalName(originalName);
        this.setString(string);
    }

    public void setString(final boolean string) {
        this.string = string;
    }

    public boolean isString() {
        return this.string;
    }

    public void setOriginalName(final String originalName) {
        this.originalName = originalName;
    }

    public String getOriginalName() {
        return this.originalName;
    }
}
