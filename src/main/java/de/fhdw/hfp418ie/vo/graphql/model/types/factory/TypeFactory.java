package de.fhdw.hfp418ie.vo.graphql.model.types.factory;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;

import java.util.Collection;

public interface TypeFactory {

    TypeFactory FACTORY = TypeFactoryImpl.getInstance();

    BaseType createBaseType(String name);

    MergedType createMergedType(Collection<FieldCorrespondence> correspondences, EqualityExpression equalityExpression);

    MergedType createMergedType(String name, Collection<FieldCorrespondence> correspondences,
                                EqualityExpression equalityExpression);

}
