package de.fhdw.hfp418ie.vo.graphql.model.builder;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.Collection;

public interface ModelBuilder {
    BaseType newType(String name);

    ModelBuilder addType(Type type);

    ModelBuilder addAllTypes(Collection<? extends Type> types);

    Model buildInitial();

    Model buildMerged();

    String getName();

    void setName(String name);

    String getEndpoint();

    void setEndpoint(String endpoint);
}
