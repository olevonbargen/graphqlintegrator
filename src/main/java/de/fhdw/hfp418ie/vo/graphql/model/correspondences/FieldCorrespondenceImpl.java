package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.model.fields.BaseField;

import java.util.ArrayList;
import java.util.Collection;

public class FieldCorrespondenceImpl extends CorrespondenceImpl implements FieldCorrespondence {

    private final Collection<BaseField> relatedFields;

    public FieldCorrespondenceImpl() {
        this(null);
    }

    public FieldCorrespondenceImpl(final String name) {
        super(name);
        this.relatedFields = new ArrayList<>();
    }

    @Override
    public Collection<BaseField> getRelatedFields() {
        return this.relatedFields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldCorrespondenceImpl that = (FieldCorrespondenceImpl) o;
        return Objects.equal(getRelatedFields(), that.getRelatedFields()) &&
                Objects.equal(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRelatedFields(), getName());
    }
}
