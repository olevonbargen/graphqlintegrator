package de.fhdw.hfp418ie.vo.graphql.model.arguments;

import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

public class ArgumentImpl implements Argument {

    private String name;
    private String originalName;
    private Type type;
    private boolean nullable;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (this.name == null) {
            this.originalName = name;
        }
        this.name = name;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean isNullable() {
        return nullable;
    }

    @Override
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    @Override
    public String getOriginalName() {
        return this.originalName;
    }
}
