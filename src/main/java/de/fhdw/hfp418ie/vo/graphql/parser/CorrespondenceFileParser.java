package de.fhdw.hfp418ie.vo.graphql.parser;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondence;

import java.io.IOException;
import java.util.Collection;

public interface CorrespondenceFileParser {

    void parse() throws IOException;

    Collection<Model> getModels();

    Collection<ModelCorrespondence> getCorrespondences();

}
