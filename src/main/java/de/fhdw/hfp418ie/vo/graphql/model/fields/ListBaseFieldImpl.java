package de.fhdw.hfp418ie.vo.graphql.model.fields;

import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;

public class ListBaseFieldImpl extends BaseFieldImpl implements ListField {

    static final String ListTypeDescriptionStart = "[";
    static final String ListTypeDescriptionEnd = "]";

    private boolean elementNullable;

    public ListBaseFieldImpl(BaseType baseType) {
        super(baseType);
        this.elementNullable = true;
    }

    @Override
    public String generateFieldType() {
        StringBuilder result = new StringBuilder();
        result.append(ListTypeDescriptionStart);
        result.append(this.getResultType().getName());
        if (this.isElementNullable()) {
            result.append(notNullableSign);
        }
        result.append(ListTypeDescriptionEnd);
        return result.toString();
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public boolean isElementNullable() {
        return elementNullable;
    }

    @Override
    public void setElementNullable(final boolean elementNullable) {
        this.elementNullable = elementNullable;
    }
}
