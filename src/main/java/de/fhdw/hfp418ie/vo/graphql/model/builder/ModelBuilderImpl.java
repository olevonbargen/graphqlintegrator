package de.fhdw.hfp418ie.vo.graphql.model.builder;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.ModelImpl;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.ModelBuilderMergedTypeVisitor;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.TypeVisitor;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class ModelBuilderImpl implements ModelBuilder {

    private String name;
    private String endpoint;
    private final Collection<Type> types;

    private ModelBuilderImpl() {
        this.types = new HashSet<>();
    }

    public ModelBuilderImpl(final String name, final String endpoint) {
        this();
        this.setName(name);
        this.setEndpoint(endpoint);
    }

    @Override
    public BaseType newType(final String name) {
        BaseType result = TypeFactory.FACTORY.createBaseType(name);
        this.getTypes().add(result);
        return result;
    }

    @Override
    public ModelBuilder addType(final Type type) {
        this.getTypes().add(type);
        return this;
    }

    @Override
    public ModelBuilder addAllTypes(Collection<? extends Type> types) {
        types.forEach(this::addType);
        return this;
    }

    @Override
    public Model buildMerged() {
        final Model result = new ModelImpl(this.getName(), this.getEndpoint(), this.getTypes());
        final TypeVisitor mergedTypeVisitor = new ModelBuilderMergedTypeVisitor(result);
        this.getTypes().forEach(type -> type.accept(mergedTypeVisitor));
        this.updateFieldResultTypes(result);
        return result;
    }

    private void updateFieldResultTypes(final Model model) {
        for (Type currentType : model.getTypes()) {
            for (Field currentField : currentType.getFields()) {
                Type searched = currentField.getResultType();
                if (model.getTypes().contains(searched))
                    continue;
                Optional<Type> found = model.getTypes().stream()
                        .filter(type -> type.getBaseTypes().contains(searched)).findAny();
                if (!found.isPresent()) {
                    throw new Error("Found no type in current model for " + searched
                            + " as result type of " + currentField);
                }
                currentField.setResultType(found.get());
            }
        }
    }

    @Override
    public Model buildInitial() {
        final Model result = new ModelImpl(this.getName(), this.getEndpoint(), this.getTypes());
        this.getTypes().forEach(type -> type.setModel(result));
        return result;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    private Collection<Type> getTypes() {
        return types;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelBuilderImpl that = (ModelBuilderImpl) o;
        return Objects.equal(getName(), that.getName()) &&
                Objects.equal(getEndpoint(), that.getEndpoint()) &&
                Objects.equal(getTypes(), that.getTypes());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getEndpoint(), getTypes());
    }

    @Override
    public String toString() {
        return "ModelBuilder{" +
                "name='" + name + '\'' +
                ", endpoint='" + endpoint + '\'' +
                ", types=" + types +
                '}';
    }
}
