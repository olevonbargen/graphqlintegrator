package de.fhdw.hfp418ie.vo.graphql.model.types.factory;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseTypeImpl;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedTypeImpl;

import java.util.Collection;

public class TypeFactoryImpl implements TypeFactory {

    public static TypeFactory getInstance() {
        if (TypeFactoryImpl.instance == null) {
            TypeFactoryImpl.instance = new TypeFactoryImpl();
        }
        return TypeFactoryImpl.instance;
    }

    private static TypeFactory instance = null;

    private TypeFactoryImpl() {
    }

    @Override
    public BaseType createBaseType(final String name) {
        return new BaseTypeImpl(name);
    }

    @Override
    public MergedType createMergedType(final Collection<FieldCorrespondence> correspondences,
                                       EqualityExpression equalityExpression) {
        return new MergedTypeImpl(correspondences, equalityExpression);
    }

    @Override
    public MergedType createMergedType(final String name, final Collection<FieldCorrespondence> correspondences,
                                       EqualityExpression equalityExpression) {
        return new MergedTypeImpl(name, correspondences, equalityExpression);
    }
}
