package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.Map;

public interface StringExpression extends Expression {

    @Override
    String evaluate(Map<VariableExpression, Object> variables);

}
