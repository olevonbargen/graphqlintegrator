package de.fhdw.hfp418ie.vo.graphql.model.fields;

import java.util.Collection;

public interface MergedField extends Field {

    boolean isElementNullable();

    void setElementNullable(boolean nullable);

    Collection<BaseField> getBaseFields();

}
