package de.fhdw.hfp418ie.vo.graphql.expression;

import java.util.Map;

public class StringVariable extends VariableExpression implements StringExpression {

    public StringVariable() {
        super();
    }

    public StringVariable(String modelName, String type, String field) {
        super(modelName, type, field);
    }

    @Override
    public String evaluate(Map<VariableExpression, Object> variables) {
        Object result = variables.get(this);
        if (result.getClass() != String.class) {
            throw new Error("Resolving variable " + this.toString() + " results in wrong typed result "
                    + result.toString());
        }
        return (String) result;
    }
}
