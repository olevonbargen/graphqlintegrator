package de.fhdw.hfp418ie.vo.graphql.converter;

import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.arguments.Argument;
import de.fhdw.hfp418ie.vo.graphql.model.arguments.ArgumentImpl;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilder;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilderImpl;
import de.fhdw.hfp418ie.vo.graphql.model.fields.*;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.util.Pair;
import graphql.schema.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class GraphQLConverterImpl implements GraphQLConverter {

    public static GraphQLConverter getINSTANCE() {
        if (GraphQLConverterImpl.INSTANCE == null) {
            GraphQLConverterImpl.INSTANCE = new GraphQLConverterImpl();
        }
        return GraphQLConverterImpl.INSTANCE;
    }

    private static GraphQLConverter INSTANCE = null;

    private GraphQLConverterImpl() {
    }

    @Override
    public Pair<Model, Map<GraphQLType, BaseType>> convert(final String name, final String endpoint, final GraphQLSchema schema) {
        Collection<GraphQLType> types = schema.getAllTypesAsList().stream()
                .filter(graphQLType -> !graphQLType.getName().startsWith(GraphQLCaller.GRAPHQL_META_INFO_PREFIX))
                .collect(Collectors.toList());
        final Map<GraphQLType, BaseType> mapping = new HashMap<>();
        final ModelBuilder builder = new ModelBuilderImpl(name, endpoint);
        for (GraphQLType type : types) {
            mapping.put(type, builder.newType(type.getName()));
        }
        for (GraphQLType type : mapping.keySet()) {
            if (type instanceof GraphQLObjectType) {
                for (GraphQLFieldDefinition field : schema.getObjectType(type.getName()).getFieldDefinitions()) {
                    mapping.get(type).addField(this.convertField(field, type, mapping));
                }
            }
        }
        return new Pair<>(builder.buildInitial(), mapping);
    }

    private Field convertField(final GraphQLFieldDefinition fieldDefinition, final GraphQLType owner,
                               final Map<GraphQLType, BaseType> mapping) {
        BaseField result = this.convertType(fieldDefinition.getType(), owner, mapping);
        result.setName(fieldDefinition.getName());
        result.setType(result.getBaseType());
        Collection<GraphQLArgument> arguments = fieldDefinition.getArguments();
        return this.addArguments(result, arguments, mapping);
    }

    private BaseField convertType(final GraphQLType type, final GraphQLType owner,
                                  final Map<GraphQLType, BaseType> mapping) {
        BaseField result;
        if (type.getClass() == GraphQLNonNull.class) {
            result = this.convertType(type.getChildren().get(0), owner, mapping);
            result.setNullable(false);
            return result;
        } else if (type.getClass() == GraphQLList.class) {
            boolean nullable = true;
            GraphQLType current = type;
            if (current.getChildren().get(0).getClass() == GraphQLNonNull.class) {
                current = current.getChildren().get(0);
                nullable = false;
            }
            ListField list = new ListBaseFieldImpl(mapping.get(owner));
            list.setElementNullable(nullable);
            list.setResultType(mapping.get(current.getChildren().get(0)));
            result = list;
        } else {
            result = new ElementBaseFieldImpl(mapping.get(owner));
            result.setResultType(mapping.get(type));
            result.setNullable(true);
        }
        return result;
    }

    private Field addArguments(final Field result, final Collection<GraphQLArgument> arguments,
                               final Map<GraphQLType, BaseType> mapping) {
        for (GraphQLArgument argument : arguments) {
            Argument convertedArgument = this.convertArgument(argument, mapping);
            result.addArgument(convertedArgument);
        }
        return result;
    }

    private Argument convertArgument(final GraphQLArgument argument, Map<GraphQLType, BaseType> mapping) {
        Argument result = new ArgumentImpl();
        result.setName(argument.getName());
        if (argument.getType().getClass() == GraphQLNonNull.class) {
            result.setNullable(false);
            result.setType(mapping.get(argument.getType().getChildren().get(0)));
        } else {
            result.setNullable(true);
            result.setType(mapping.get(argument.getType()));
        }
        return result;
    }
}
