package de.fhdw.hfp418ie.vo.graphql.model.fields;

import de.fhdw.hfp418ie.vo.graphql.model.arguments.Argument;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.ArrayList;
import java.util.Collection;

public abstract class FieldImpl implements Field {

    public static final String typeFieldNameSeparator = "_";
    static final String fieldNameSeparator = ":";
    static final String argumentListStartSign = "(";
    static final String argumentListSeparatorSign = ",";
    static final String argumentListEndSign = ")";
    static final String notNullableSign = "!";

    private String name;
    private Type type;
    private Type resultType;
    private boolean nullable;
    private Collection<Argument> arguments;

    FieldImpl() {
        this.arguments = new ArrayList<>();
    }

    @Override
    public String generateDescription(boolean duplicateName) {
        StringBuilder result = new StringBuilder();
        if (duplicateName) {
            this.setName(this.handleDuplicateName());
        }
        result.append(this.getName());
        if (this.getArguments().size() > 0) {
            result.append(argumentListStartSign);
            boolean first = true;
            for (Argument argument : this.getArguments()) {
                if (!first) {
                    result.append(argumentListSeparatorSign);
                }
                first = false;
                result.append(argument.getName());
                result.append(fieldNameSeparator);
                result.append(argument.getType().getName());
                if (!argument.isNullable()) {
                    result.append(notNullableSign);
                }
            }
            result.append(argumentListEndSign);
        }
        result.append(fieldNameSeparator);
        result.append(this.generateFieldType());
        if (!this.isNullable()) {
            result.append(notNullableSign);
        }
        return result.toString();
    }

    protected abstract String handleDuplicateName();

    protected abstract String generateFieldType();

    protected abstract void handleNameChange(final String old);

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        String old = this.getName();
        this.name = name;
        this.handleNameChange(old);
    }

    @Override
    public Type getType() {
        return this.type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public Type getResultType() {
        return resultType;
    }

    @Override
    public void setResultType(Type resultType) {
        this.resultType = resultType;
    }

    @Override
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    @Override
    public boolean isNullable() {
        return nullable;
    }

    @Override
    public Collection<Argument> getArguments() {
        return arguments;
    }

    @Override
    public void addArgument(Argument argument) {
        this.getArguments().add(argument);
    }

    @Override
    public String toString() {
        return "FieldImpl{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", resultType=" + resultType +
                ", nullable=" + nullable +
                ", arguments=" + arguments +
                '}';
    }
}
