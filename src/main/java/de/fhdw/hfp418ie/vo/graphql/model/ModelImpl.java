package de.fhdw.hfp418ie.vo.graphql.model;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.arguments.Argument;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputField;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputModel;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputType;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.TypeVisitor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ModelImpl implements Model {

    private final String name;
    private final String endpoint;
    private final Collection<Type> types;

    public ModelImpl(final String name, final String endpoint, final Collection<Type> types) {
        this.name = name;
        this.endpoint = endpoint;
        this.types = types;
    }

    @Override
    public Collection<OutputModel> generateOutput() {
        final Map<String, OutputModel> result = this.calculateInvolvedModels();
        for (Type type : this.getTypes()) {
            for (Type baseType : type.getBaseTypes()) {
                final String endpoint = baseType.getModel().getEndpoint();
                final EqualityExpression[] expression = new EqualityExpression[1];
                type.accept(new TypeVisitor() {
                    @Override
                    public void handleMergedType(MergedType type) {
                        expression[0] = type.getEqualityExpression();
                    }

                    @Override
                    public void handleBaseType(BaseType type) {
                        expression[0] = null;
                    }
                });
                final OutputType currentType = result.get(endpoint).addType(type.getName(), expression[0]);
                for (Field field : baseType.getFields()) {
                    OutputField fieldResult = currentType.addField(field.getName(), field.getOriginalName(),
                            field.getResultType().getName());
                    for (Argument argument : field.getArguments()) {
                        final boolean isArgumentString = argument.getType().getName().contains("String");
                        fieldResult.addArgument(argument.getName(), argument.getOriginalName(), isArgumentString);
                    }
                }
            }
        }
        return result.values();
    }

    private Map<String, OutputModel> calculateInvolvedModels() {
        Map<String, OutputModel> result = new HashMap<>();
        Collection<Type> types = this.getTypes().stream().flatMap(type -> type.getBaseTypes().stream()).collect(Collectors.toList());
        for (Type type : types) {
            Model model = type.getModel();
            if (!result.containsKey(model.getEndpoint())) {
                result.put(model.getEndpoint(), new OutputModel(model.getName(), model.getEndpoint()));
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public Collection<Type> getTypes() {
        return types;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelImpl model = (ModelImpl) o;
        return Objects.equal(getName(), model.getName()) &&
                Objects.equal(getEndpoint(), model.getEndpoint());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getEndpoint());
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
