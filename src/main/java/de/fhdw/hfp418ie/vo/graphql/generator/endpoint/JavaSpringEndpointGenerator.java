package de.fhdw.hfp418ie.vo.graphql.generator.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhdw.hfp418ie.vo.graphql.converter.GraphQLConverter;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.arguments.Argument;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.fields.MergedField;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputModel;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.io.*;
import java.net.URL;
import java.util.*;

public class JavaSpringEndpointGenerator implements EndpointGenerator {

    private static final String expressionsDefaultPackage = "de.fhdw.hfp418ie.vo.graphql.expression";
    private static final String[] expressionFiles = {"BooleanCompositeExpression.java", "BooleanConstant.java",
            "BooleanExpression.java", "CompositeExpression.java", "ConcatenationExpression.java",
            "ConjunctionExpression.java", "DisjunctionExpression.java", "EqualityExpression.java", "Expression.java",
            "FloatConstant.java", "FloatExpression.java", "IntegerConstant.java", "IntegerExpression.java",
            "LongExpression.java", "StringConstant.java", "StringExpression.java", "StringVariable.java",
            "VariableExpression.java", "Constant.java", "ExpressionVisitor.java"};
    private static final String mergerInterfaceFileName = "Merger.java";
    private static final String mergerImplementationFileName = "MergerImpl.java";
    private static final String gradleBuildFileName = "build.gradle";
    private static final String gradleSettingsFileName = "settings.gradle";
    private static final String applicationStarterFileName = "Application.java";
    private static final String defaultEntityFileName = "Entity.java";
    private static final String defaultEntityMergerFileName = "EntityMerger.java";
    private static final String defaultFieldFileName = "Field.java";
    private static final String defaultGraphQlCallerInterfaceFileName = "GraphQLCaller.java";
    private static final String defaultGraphQlCallerImplementationFileName = "GraphQLCallerImpl.java";
    private static final String defaultResolverFileName = "Resolver.java";
    private static final String applicationPropertiesFileName = "application.properties";
    private static final String defaultGetterAndSetterFileName = "GetterAndSetter.java";
    private static final String defaultElementFieldMergerFileName = "ElementFieldMerger.java";
    private static final String defaultScalarListMergerFileName = "ScalarListFieldMerger.java";
    private static final String defaultListMergerFileName = "ListFieldMerger.java";
    private static final String defaultFieldResolverFileName = "FieldResolver.java";
    private static final String defaultMergedFieldResolverFileName = "MergedFieldResolver.java";
    private static final String defaultResolverFunctionElementFileName = "ResolverFunctionElement.java";
    private static final String defaultResolverFunctionListFileName = "ResolverFunctionList.java";
    private static final String defaultResolverStandardAggregationFileName = "ResolverStandardAggregation.java";
    private static final String defaultResolverScalarAggregationFileName = "ResolverScalarAggregation.java";
    private static final String resolverAggregationPlaceholder = "$$AGGREGATION$$";
    private static final String templateRootPath = "templates/javaSpring/";
    private static final String templateSuffix = ".template";
    private static final String projectNameVariableKey = "projectName";
    private static final String rootPackageVariableKey = "rootPackageName";
    private static final String entityNameKey = "entityName";
    private static final String fieldNameKey = "fieldName";
    private static final String fieldTypeKey = "fieldType";
    private static final String endpointFieldName = "endpoint";
    private static final String fieldNameUpperKey = "fieldNameUpper";
    private static final String classNameKey = "className";
    private static final String rootTypeKey = "rootType";
    private static final String interfaceName = "interfaceName";
    private static final String fieldsPlaceholder = "$$FIELDS$$";
    private static final String importsPlaceholder = "$$IMPORTS$$";
    private static final String functionsPlaceholder = "$$FUNCTIONS$$";
    private static final String argumentsPlaceholder = "$$ARGUMENTS$$";
    private static final String constructorPlaceholder = "$$CONSTRUCTOR$$";
    private static final String elementConstructionPlaceholder = "$$CONSTRUCTION$$";
    private static final String mergeFieldsPlaceholder = "$$MERGEFIELDS$$";
    private static final String resolveFieldsPlaceholder = "$$RESOLVEFIELD$$";
    private static final String listPrefix = "Collection<";
    private static final String listSuffix = ">";
    private static final String queryResolverClassName = "Query";
    private static final String queryResolverInterfaceName = "GraphQLQueryResolver";
    private static final String mutationResolverClassName = "Mutation";
    private static final String mutationResolverInterfaceName = "GraphQLMutationResolver";
    private static final String collectionImport = "import java.util.Collection;";
    private static final String arrayListImport = "import java.util.ArrayList;";
    private static final String mapImport = "import java.util.Map;";
    private static final String hashMapImport = "import java.util.HashMap;";
    private static final String constructorStartPrefix = "public ";
    private static final String constructorStartSuffix = "(final JsonNode input, final OutputType outputType, final String endpoint) {\n";
    private static final String constructorEnd = "globaleType.getTypes().put(endpoint, outputType);\n" +
            "       if (outputType.getEqualityExpression() != null) {\n" +
            "            globaleType.setExpression(outputType.getEqualityExpression());\n" +
            "            merger = new entityNameMerger(outputType.getEqualityExpression());\n" +
            "        }\n" +
            "        this.setEndpoint(endpoint);\n}\n";
    private static final String iterationStart = "final Iterator<String> fieldNames = input.fieldNames();\n" +
            "while (fieldNames.hasNext()) {\nfinal String currentFieldName = fieldNames.next();\n" +
            "switch (currentFieldName) {\n";
    private static final String iterationEndPrefix = "default: \nthrow new Error(\"Cannot convert field \" +" +
            " currentFieldName + \" within the type ";
    private static final String iterationEndSuffix = "\");\n}\n}\n";

    private final Map<String, String> mappingOfScalars;

    public JavaSpringEndpointGenerator() {
        this.mappingOfScalars = new HashMap<>();
        this.mappingOfScalars.put("Boolean", "Boolean");
        this.mappingOfScalars.put("Int", "Integer");
        this.mappingOfScalars.put("Float", "Float");
        this.mappingOfScalars.put("ID", "Long");
        this.mappingOfScalars.put("String", "String");
    }

    @Override
    public void generateEndpoint(String rootPath, String graphQlSchema, Model model, String rootPackageName, String name)
            throws IOException {
        Map<String, String> variables = new HashMap<>();
        variables.put(projectNameVariableKey, name);
        variables.put(rootPackageVariableKey, rootPackageName);
        File rootPathFile = new File(rootPath);
        if (!rootPathFile.exists()) {
            rootPathFile.mkdirs();
        }
        String path = this.generateDirectoryStructure(rootPath, name, variables);
        this.generateJavaClasses(path, model, variables);
        this.generateResourceDirectory(rootPath + "/" + name, variables, graphQlSchema, model);
    }

    private void generateResourceDirectory(String path, Map<String, String> variables, String graphQlSchema,
                                           Model model) throws IOException {
        File file = new File(path + "/src/main/resources/");
        file.mkdir();
        File config = new File(file.getPath() + "/config/");
        config.mkdir();
        ObjectMapper modelWriter = new ObjectMapper();
        Collection<OutputModel> models = model.generateOutput();
        modelWriter.writeValue(new File(config.getPath() + "/models.json"), models);
        this.writeFromTemplateFile(file.getPath(), applicationPropertiesFileName, variables);
        File graphql = new File(file.getPath() + "/graphql/");
        graphql.mkdir();
        graphql = new File(graphql.getPath() + "/schema.graphqls");
        BufferedWriter writer = new BufferedWriter(new FileWriter(graphql));
        writer.write(graphQlSchema);
        writer.flush();
        writer.close();
    }

    private void generateJavaClasses(String rootPath, Model model, Map<String, String> variables) throws IOException {
        this.writeFromTemplateFile(rootPath, applicationStarterFileName, variables);
        this.generateModelPackage(rootPath, model, variables);
        this.generateResolverPackage(rootPath, model, variables);
        this.generateUtilPackage(rootPath, variables);
    }

    private void generateUtilPackage(String path, Map<String, String> variables) throws IOException {
        File directory = new File(path + "/util/");
        directory.mkdir();
        this.writeFromTemplateFile(directory.getPath(), defaultGraphQlCallerInterfaceFileName, variables);
        this.writeFromTemplateFile(directory.getPath(), defaultGraphQlCallerImplementationFileName, variables);
    }

    private void generateResolverPackage(String path, Model model, Map<String, String> variables) throws IOException {
        File directory = new File(path + "/resolvers/");
        directory.mkdir();
        variables.put(classNameKey, queryResolverClassName);
        variables.put(interfaceName, queryResolverInterfaceName);
        this.generateResolver(directory.getPath(), model, variables);
        variables.put(classNameKey, mutationResolverClassName);
        variables.put(interfaceName, mutationResolverInterfaceName);
        this.generateResolver(directory.getPath(), model, variables);
    }

    private void generateResolver(String path, Model model, Map<String, String> variables) throws IOException {
        String resolver = this.readTemplateFile(defaultResolverFileName, variables);
        Optional<Type> search = model.getTypes().stream().filter(
                type -> type.getName().equals(variables.get(classNameKey))).findAny();
        if (!search.isPresent()) {
            throw new Error(variables.get(classNameKey) + " type not found in generated model!");
        }
        StringBuilder resolveFunctions = new StringBuilder();
        Type searched = search.get();
        boolean collectionUsed = false;
        for (Field field : searched.getFields()) {
            if (field.isList()) {
                collectionUsed = true;
            }
            this.generateResolveFunctionForField(resolveFunctions, field);
        }
        String imports = mapImport + System.lineSeparator() + hashMapImport + System.lineSeparator();
        if (collectionUsed) {
            imports += collectionImport + System.lineSeparator() + arrayListImport;
        }
        resolver = resolver.replace(functionsPlaceholder, resolveFunctions.toString());
        resolver = resolver.replace(importsPlaceholder, imports);
        File result = new File(path + "/" + variables.get(classNameKey) + ".java");
        result.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(result));
        writer.write(resolver);
        writer.flush();
        writer.close();
    }

    private void generateResolveFunctionForField(StringBuilder result, Field field) throws IOException {
        Map<String, String> variables = new HashMap<>();
        variables.put(rootTypeKey, field.getType().getName());
        variables.put(fieldNameUpperKey, field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
        variables.put(fieldTypeKey, this.convertTypeName(field.getResultType().getName()));
        variables.put(fieldNameKey, field.getName());
        String function = field.isList() ? this.readTemplateFile(defaultResolverFunctionListFileName, variables) :
                this.readTemplateFile(defaultResolverFunctionElementFileName, variables);
        if (Arrays.asList(GraphQLConverter.DEFAULT_SCALAR_TYPES).contains(field.getResultType().getName())) {
            switch (field.getResultType().getName()) {
                case "String":
                    function = function.replace(elementConstructionPlaceholder, "response.asText()");
                    break;
                case "ID":
                    function = function.replace(elementConstructionPlaceholder, "response.asLong()");
                    break;
                case "Boolean":
                    function = function.replace(elementConstructionPlaceholder, "response.asBoolean()");
                    break;
                case "Int":
                    function = function.replace(elementConstructionPlaceholder, "response.asInt()");
                    break;
                case "Float":
                    function = function.replace(elementConstructionPlaceholder, "(float)response.asDouble()");
                    break;
                default:
                    throw new Error("Found unknown scalar type " + field.getResultType().getName());
            }
            function = function.replace(resolverAggregationPlaceholder,
                    this.readTemplateFile(defaultResolverScalarAggregationFileName, variables));
        } else {
            function = function.replace(elementConstructionPlaceholder,
                    "new " + variables.get(fieldTypeKey) + "(response, GraphQLCaller.INSTANCE.getResultType(endpoint, \"" + variables.get(rootTypeKey) + "\", \"" + field.getName() + "\"), endpoint)");
            function = function.replace(resolverAggregationPlaceholder,
                    this.readTemplateFile(defaultResolverStandardAggregationFileName, variables));
        }
        StringBuilder arguments = new StringBuilder();
        for (Argument argument : field.getArguments()) {
            arguments.append(this.convertTypeName(argument.getType().getName()));
            arguments.append(" ");
            arguments.append(argument.getName());
            arguments.append(", ");
        }
        result.append(function.replace(argumentsPlaceholder, arguments.toString()));
    }

    private String generateDirectoryStructure(String rootPath, String name, Map<String, String> variables)
            throws IOException {
        File root = new File(rootPath + "/" + name);
        root.mkdir();
        this.writeFromTemplateFile(root.getPath(), gradleBuildFileName, variables);
        this.writeFromTemplateFile(root.getPath(), gradleSettingsFileName, variables);
        return this.generateSourceCodeDirectory(root.getPath(), variables);
    }

    private void generateModelPackage(String sourceCodeDirectoryPath, Model model, Map<String, String> variables)
            throws IOException {
        File directory = new File(sourceCodeDirectoryPath + "/model/");
        directory.mkdir();
        File outputModelPackage = new File(directory.getPath() + "/output/");
        outputModelPackage.mkdir();
        for (Type type : model.getTypes()) {
            if (Arrays.asList(GraphQLConverter.DEFAULT_SCALAR_TYPES).contains(type.getName()) ||
                    Arrays.asList(GraphQLConverter.DEFAULT_GRAPHQL_TYPES).contains(type.getName()))
                continue;
            this.generateJavaClass(directory.getPath(), type, variables);
        }
        this.writeFromTemplateFile(directory.getPath(), mergerInterfaceFileName, variables);
        this.writeFromTemplateFile(directory.getPath(), mergerImplementationFileName, variables);
        this.writeFromTemplateFile(outputModelPackage.getPath(), "OutputEntity.java", variables);
        this.writeFromTemplateFile(outputModelPackage.getPath(), "OutputModel.java", variables);
        this.writeFromTemplateFile(outputModelPackage.getPath(), "OutputType.java", variables);
        this.writeFromTemplateFile(outputModelPackage.getPath(), "OutputField.java", variables);
        this.writeFromTemplateFile(outputModelPackage.getPath(), "OutputArgument.java", variables);
        this.writeFromTemplateFile(outputModelPackage.getPath(), "OutputGlobalType.java", variables);
    }

    private String convertTypeName(String typeName) {
        if (Arrays.asList(GraphQLConverter.DEFAULT_SCALAR_TYPES).contains(typeName)) {
            return this.mappingOfScalars.get(typeName);
        } else {
            return typeName;
        }
    }

    private void generateJavaClass(String path, Type type, Map<String, String> variables) throws IOException {
        variables.put(entityNameKey, type.getName());
        final File classFile = new File(path + "/" + type.getName() + ".java");
        String result = this.readTemplateFile(defaultEntityFileName, variables);
        final StringBuilder fields = new StringBuilder();
        final StringBuilder getterAndSetter = new StringBuilder();
        boolean foundMerged = false;
        boolean foundList = false;
        for (Field field : type.getFields()) {
            variables.put(fieldNameKey, field.getName());
            variables.put(fieldNameUpperKey, field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
            variables.put(fieldTypeKey, this.convertTypeName(field.getResultType().getName()));
            if (field.isList()) {
                variables.put(fieldTypeKey, listPrefix + variables.get(fieldTypeKey) + listSuffix);
                foundList = true;
            }
            if (field.isMerged()) {
                foundMerged = true;
            }
            fields.append(this.readTemplateFile(defaultFieldFileName, variables));
            fields.append(System.lineSeparator());
            getterAndSetter.append(this.readTemplateFile(defaultGetterAndSetterFileName, variables));
            getterAndSetter.append(System.lineSeparator());
        }
        variables.put(fieldNameKey, endpointFieldName);
        variables.put(fieldNameUpperKey, endpointFieldName.substring(0, 1).toUpperCase() + endpointFieldName.substring(1));
        variables.put(fieldTypeKey, "String");
        fields.append(this.readTemplateFile(defaultFieldFileName, variables));
        getterAndSetter.append(this.readTemplateFile(defaultGetterAndSetterFileName, variables));
        fields.append(getterAndSetter.toString());
        final String imports = foundList ? collectionImport + System.lineSeparator() + arrayListImport : "";
        result = result.replace(fieldsPlaceholder, fields.toString());
        result = result.replace(importsPlaceholder, imports);
        result = result.replace(constructorPlaceholder, this.generateConstructor(type, variables));
        BufferedWriter writer = new BufferedWriter(new FileWriter(classFile));
        writer.write(result);
        writer.flush();
        writer.close();
        this.generateEntityMergerFile(path, type, variables);
    }

    private void generateEntityMergerFile(String path, Type type, Map<String, String> variables) throws IOException {
        final File classFile = new File(path + "/" + type.getName() + "Merger.java");
        String result = this.readTemplateFile(defaultEntityMergerFileName, variables);
        final StringBuilder mergeFields = new StringBuilder();
        final StringBuilder resolveFields = new StringBuilder();
        for (Field field : type.getFields()) {
            variables.put(fieldNameKey, field.getName());
            variables.put(fieldNameUpperKey, field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
            variables.put(fieldTypeKey, this.convertTypeName(field.getResultType().getName()));
            if (field.isList()) {
                if (Arrays.asList(GraphQLConverter.DEFAULT_SCALAR_TYPES).contains(field.getResultType().getName())) {
                    mergeFields.append(this.readTemplateFile(defaultScalarListMergerFileName, variables));
                } else {
                    mergeFields.append(this.readTemplateFile(defaultListMergerFileName, variables));
                }
            } else {
                mergeFields.append(this.readTemplateFile(defaultElementFieldMergerFileName, variables));
            }
            boolean needResolver = true;
            if (field.isMerged()) {
                MergedField mergedField = (MergedField) field;
                if (!mergedField.getBaseFields().iterator().next().isList()) {
                    needResolver = false;
                    resolveFields.append(this.readTemplateFile(defaultMergedFieldResolverFileName, variables));
                }
            }
            if (needResolver) {
                resolveFields.append(this.readTemplateFile(defaultFieldResolverFileName, variables));
            }
        }
        result = result.replace(mergeFieldsPlaceholder, mergeFields.toString());
        result = result.replace(resolveFieldsPlaceholder, resolveFields.toString());
        BufferedWriter writer = new BufferedWriter(new FileWriter(classFile));
        writer.write(result);
        writer.flush();
        writer.close();
    }

    private String generateConstructor(Type type, Map<String, String> variables) {
        StringBuilder result = new StringBuilder();
        result.append(constructorStartPrefix);
        result.append(type.getName());
        result.append(constructorStartSuffix);
        result.append(iterationStart);
        for (Field field : type.getFields()) {
            result.append("case \"");
            result.append(field.getName());
            result.append("\":\n");
            String operation = "set";
            if (field.isList()) {
                operation = "get";
                result.append("this.set");
                result.append(field.getName().substring(0, 1).toUpperCase());
                result.append(field.getName().substring(1));
                result.append("(");
                result.append("new ArrayList<>());\n");
                result.append("if (input.get(currentFieldName).isArray()) {\n");
                result.append("input.get(currentFieldName).forEach(element -> "
                        + this.generateValueFetcher(operation, field, type, true));
                result.append("} else {\n");
            }
            result.append(this.generateValueFetcher(operation, field, type, false));
            if (field.isList()) {
                result.append("}");
            }
            result.append("break;\n");
        }
        result.append(iterationEndPrefix);
        result.append(type.getName());
        result.append(iterationEndSuffix);
        result.append(constructorEnd.replace(entityNameKey, variables.get(entityNameKey)));
        return result.toString();
    }

    private String generateValueFetcher(String operation, Field field, Type type, boolean inLoop) {
        String end = inLoop ? ");" : ";";
        String input = inLoop ? "element" : "input.get(currentFieldName)";
        StringBuilder result = new StringBuilder();
        result.append("this.");
        result.append(operation);
        result.append(field.getName().substring(0, 1).toUpperCase());
        result.append(field.getName().substring(1));
        result.append("(");
        if (operation.equals("get")) {
            result.append(").add(");
        }
        switch (field.getResultType().getName()) {
            case "String":
                result.append(input + ".asText())" + end + "\n");
                break;
            case "ID":
                result.append(input + ".asLong())" + end + "\n");
                break;
            case "Boolean":
                result.append(input + ".asBoolean())+ end +\n");
                break;
            case "Int":
                result.append(input + ".asInt())" + end + "\n");
                break;
            case "Float":
                result.append("(float)" + input + ".asDouble())" + end + "\n");
                break;
            default:
                result.append("new ");
                result.append(field.getResultType().getName());
                result.append("(" + input + ", GraphQLCaller.INSTANCE.getResultType(endpoint, \""
                        + type.getName() + "\", \"" + field.getName() + "\"), endpoint))" + end + "\n");
        }
        return result.toString();
    }

    private String generateSourceCodeDirectory(String rootPath, Map<String, String> variables) throws IOException {
        File directory = new File(rootPath + "/src/");
        directory.mkdir();
        directory = new File(directory.getPath() + "/main/");
        directory.mkdir();
        directory = new File(directory.getPath() + "/java/");
        directory.mkdir();
        String rootPackageName = variables.get(rootPackageVariableKey);
        final String result = this.createPackageDirectory(directory.getPath(), rootPackageName).getPath();
        String exprPackage = this.createPackageDirectory(directory.getPath(), expressionsDefaultPackage).getPath();
        for (String fileName : expressionFiles) {
            this.writeFromTemplateFile(exprPackage, fileName, variables);
        }
        return result;
    }

    private File createPackageDirectory(String rootPath, String packageName) {
        File directory = new File(rootPath);
        String current = packageName;
        while (!current.isEmpty()) {
            if (current.contains(".")) {
                directory = new File(directory.getPath() + "/" +
                        current.substring(0, current.indexOf(".")) + "/");
                directory.mkdir();
                current = current.substring(current.indexOf(".") + 1);
            } else {
                directory = new File(directory.getPath() + "/" + current + "/");
                directory.mkdir();
                current = "";
            }
        }
        return directory;
    }

    private void writeFromTemplateFile(String path, String fileName, Map<String, String> variables)
            throws IOException {
        File file = new File(path + "/" + fileName);
        file.createNewFile();
        String result = this.readTemplateFile(fileName, variables);
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(result);
        writer.flush();
        writer.close();
    }

    private String readTemplateFile(String fileName, Map<String, String> variables) throws IOException {
        URL url = this.getClass().getClassLoader().getResource(
                this.getTemplateFilePath(fileName));
        if (url == null) {
            throw new FileNotFoundException("Template file with the name " + this.getTemplateFilePath(fileName)
                    + " not found!");
        }
        StringBuilder result = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(new File(url.getFile())));
        String line = reader.readLine();
        while (line != null) {
            if (line.startsWith("${")) {
                line = line.substring(2);
                String variableName = line.substring(0, line.indexOf("}"));
                if (!variables.containsKey(variableName)) {
                    throw new Error("Cannot resolve variable " + variableName);
                }
                result.append(variables.get(variableName));
                line = line.substring(line.indexOf("}") + 1);
            } else if (line.contains("${")) {
                result.append(line, 0, line.indexOf("${"));
                line = line.substring(line.indexOf("${"));
            } else {
                result.append(line);
                result.append(System.lineSeparator());
                line = reader.readLine();
            }
        }
        reader.close();
        return result.toString();
    }

    private String getTemplateFilePath(String fileName) {
        return templateRootPath + fileName + templateSuffix;
    }

}
