package de.fhdw.hfp418ie.vo.graphql.model.output;

import java.util.ArrayList;
import java.util.Collection;

public class OutputField extends OutputEntity {

    private String originalName;
    private String resultTypeName;
    private final Collection<OutputArgument> arguments;

    public OutputField() {
        this.arguments = new ArrayList<>();
    }

    OutputField(String name, String originalName, String resultTypeName) {
        super(name);
        this.arguments = new ArrayList<>();
        this.setOriginalName(originalName);
        this.setResultTypeName(resultTypeName);
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public void setResultTypeName(String resultTypeName) {
        this.resultTypeName = resultTypeName;
    }

    public String getResultTypeName() {
        return resultTypeName;
    }

    public String getOriginalName() {
        return this.originalName;
    }

    public void addArgument(final String name, final String originalName, final boolean string) {
        this.getArguments().add(new OutputArgument(name, originalName, string));
    }

    public Collection<OutputArgument> getArguments() {
        return arguments;
    }
}
