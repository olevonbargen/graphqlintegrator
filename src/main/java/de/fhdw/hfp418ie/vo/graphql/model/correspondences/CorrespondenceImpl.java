package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

public abstract class CorrespondenceImpl implements Correspondence {

    private final String name;

    CorrespondenceImpl(final String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
