package de.fhdw.hfp418ie.vo.graphql.integration;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondence;

import java.util.Collection;

/**
 * Integrator for the calculation of the integrated internal model.
 */
public interface Integrator {

    /**
     * Calculates the integrated internal model from a given amount of models and the correspondences.
     *
     * @param name            Given name for the resulting integrated model.
     * @param endpoint        Given endpoint for the resulting internal model.
     * @param models          Given collection of internal original models.
     * @param correspondences Given correspondences.
     * @return The integrated internal model.
     */
    Model calculateModelIntegration(String name, String endpoint, Collection<Model> models,
                                    Collection<ModelCorrespondence> correspondences);

}
