package de.fhdw.hfp418ie.vo.graphql.model.fields;

import java.util.Collection;

import static de.fhdw.hfp418ie.vo.graphql.model.fields.ListBaseFieldImpl.ListTypeDescriptionEnd;
import static de.fhdw.hfp418ie.vo.graphql.model.fields.ListBaseFieldImpl.ListTypeDescriptionStart;

public class MergedFieldImpl extends FieldImpl implements MergedField {

    private boolean elementNullable;
    private final Collection<BaseField> baseFields;

    public MergedFieldImpl(Collection<BaseField> baseFields) {
        this.baseFields = baseFields;
    }

    @Override
    public Collection<BaseField> getBaseFields() {
        return baseFields;
    }

    @Override
    public boolean isElementNullable() {
        return this.elementNullable;
    }

    @Override
    public void setElementNullable(boolean nullable) {
        this.elementNullable = nullable;
    }

    @Override
    protected String handleDuplicateName() {
        throw new Error("The merged field " + this.getName() + "results in duplicate names!");
    }

    @Override
    protected String generateFieldType() {
        String result = ListTypeDescriptionStart;
        result = result.concat(this.getResultType().getName());
        if (this.isElementNullable()) {
            result = result.concat(notNullableSign);
        }
        return result.concat(ListTypeDescriptionEnd);
    }

    @Override
    protected void handleNameChange(String old) {
        // nothing to do here
    }

    @Override
    public String getOriginalName() {
        return this.getName();
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public boolean isMerged() {
        return true;
    }
}
