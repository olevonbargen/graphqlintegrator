package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

interface Correspondence {

    String getName();

}
