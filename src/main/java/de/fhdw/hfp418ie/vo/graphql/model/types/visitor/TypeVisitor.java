package de.fhdw.hfp418ie.vo.graphql.model.types.visitor;

import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;

public interface TypeVisitor {

    void handleMergedType(final MergedType type);

    void handleBaseType(final BaseType type);

}
