package de.fhdw.hfp418ie.vo.graphql.model.types;

import com.google.common.base.Objects;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.TypeVisitor;

import java.util.Collection;
import java.util.HashSet;

public class BaseTypeImpl extends TypeImpl implements BaseType {

    private final String name;

    public BaseTypeImpl(String name) {
        this.name = name;
    }

    @Override
    public void accept(final TypeVisitor visitor) {
        visitor.handleBaseType(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Collection<Type> getBaseTypes() {
        Collection<Type> result = new HashSet<>();
        result.add(this);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseTypeImpl type = (BaseTypeImpl) o;
        return Objects.equal(getModel(), type.getModel()) &&
                Objects.equal(getName(), type.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName());
    }

    @Override
    public String toString() {
        return this.getModel() + Type.MODEL_NAME_SEPARATOR + this.getName();
    }
}
