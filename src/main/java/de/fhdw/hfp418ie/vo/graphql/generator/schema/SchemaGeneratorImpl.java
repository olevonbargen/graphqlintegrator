package de.fhdw.hfp418ie.vo.graphql.generator.schema;

import de.fhdw.hfp418ie.vo.graphql.converter.GraphQLConverter;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class SchemaGeneratorImpl implements SchemaGenerator {

    private static final String typePrefix = "type ";
    private static final String blockStartSymbol = "{";
    private static final String blockEndSymbol = "}";

    @Override
    public String generateSchemaDefinition(Model model) {
        StringBuilder result = new StringBuilder();
        model.getTypes().forEach(type -> this.generateType(result, type));
        return result.toString();
    }

    private void generateType(final StringBuilder builder, final Type type) {
        if (Arrays.asList(GraphQLConverter.DEFAULT_SCALAR_TYPES).contains(type.getName()))
            return;
        builder.append(typePrefix);
        builder.append(type.getName());
        builder.append(blockStartSymbol);
        builder.append(System.lineSeparator());
        final Collection<Field> duplicates = this.calculateDuplicateFields(type.getFields());
        type.getFields().forEach(field -> this.generateField(builder, field, duplicates));
        builder.append(blockEndSymbol);
        builder.append(System.lineSeparator());
    }

    private Collection<Field> calculateDuplicateFields(final Collection<Field> fields) {
        Collection<Field> result = new HashSet<>();
        for (Field field : fields) {
            Collection<Field> duplicates = fields.stream().filter(current -> current.getName().equals(field.getName()))
                    .collect(Collectors.toList());
            if (duplicates.size() > 1) {
                result.addAll(duplicates);
            }
        }
        return result;
    }

    private void generateField(final StringBuilder builder, final Field field, final Collection<Field> duplicateFields) {
        builder.append(field.generateDescription(duplicateFields.contains(field)));
        builder.append(System.lineSeparator());
    }
}
