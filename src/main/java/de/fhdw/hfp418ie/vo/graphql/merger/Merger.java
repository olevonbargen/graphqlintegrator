package de.fhdw.hfp418ie.vo.graphql.merger;

import java.util.Collection;
import java.util.Map;

public interface Merger<T> {

    Collection<T> merge(Map<String, Collection<T>> elements);

}
