package de.fhdw.hfp418ie.vo.graphql.demo;

import com.google.common.base.Objects;

public class Demo {

    private String info;

    public Demo() {
    }

    private String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Demo demo = (Demo) o;
        return Objects.equal(getInfo(), demo.getInfo());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getInfo());
    }
}
