package de.fhdw.hfp418ie.vo.graphql.model.types;

import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;

import java.util.Collection;

public interface MergedType extends Type {

    String SUBTYPE_NAME_SEPARATOR = "_";

    void setName(final String name);

    void addTypes(Collection<Type> types);

    Collection<Type> getTypes();

    Collection<FieldCorrespondence> getCorrespondences();

    EqualityExpression getEqualityExpression();

    void setEqualityExpression(EqualityExpression equalityExpression);

}
