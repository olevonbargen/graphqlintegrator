package de.fhdw.hfp418ie.vo.graphql.caller;

import com.fasterxml.jackson.databind.JsonNode;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputType;
import graphql.schema.GraphQLSchema;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * This interface defines the methods for a utility class which coordinates the querying of GraphQL endpoints.
 */
public interface GraphQLCaller {


    String GRAPHQL_META_INFO_PREFIX = "__";

    /**
     * This method performs an introspection query against the defined endpoint.
     * The result will be converted into a GraphQL schema.
     *
     * @param endpoint Given url of an endpoint.
     * @return Converted response of the introspection query as GraphQL schema.
     * @throws IOException If the connection can not be established or the query can not be executed.
     */
    GraphQLSchema getGraphQLSchema(String endpoint) throws IOException;

    /**
     * This method executes a given entry against a given endpoint.
     *
     * @param endpoint Given url of an endpoint.
     * @param query    Given query.
     * @return Json response from the endpoint.
     * @throws IOException If the connection can not be established or the query can not be executed.
     */
    JsonNode executeQuery(String endpoint, String query) throws IOException;

    /**
     * This method translates a query within a given root type of a given field with given arguments
     * and a given selection of subfields into an amount of queries
     * against all original endpoints with the defined field.
     *
     * @param rootType              Given root type.
     * @param fieldName             Given root field.
     * @param arguments             Given arguments of the query.
     * @param selectionSetArguments Given selection of subfields.
     * @return Map of the endpoint url and the generated query for this endpoint.
     */
    Map<String, String> generateQueries(String rootType, String fieldName, Map<String, Object> arguments, Set<String> selectionSetArguments);

    /**
     * This method translates the response of an original endpoint into the schema of the integrated endpoint.
     *
     * @param endpoint              Given original endpoint.
     * @param type                  Given type within the original endpoint.
     * @param response              Given response to be translated.
     * @param selectionSetArguments Given selection of subfields initially queried by the client.
     * @return Translated json object with the fieldnames of the integrated endpoint
     * including only fields selected by the client.
     */
    JsonNode normalizeResults(final String endpoint, final String type, JsonNode response, Set<String> selectionSetArguments);

    /**
     * Fetches the result type for a given field defined by its qualified name including the models endpoints,
     * the types name and the fields name.
     *
     * @param endpoint  Given endpoint of the model of the searched type.
     * @param typeName  Given name of the owner type from the field.
     * @param fieldName Given name of the field.
     * @return The result type of the described field.
     */
    OutputType getResultType(final String endpoint, final String typeName, final String fieldName);

    /**
     * Fetches the name of the given model, which is identified by its endpoint.
     *
     * @param endpoint Given endpoint of the model.
     * @return The defined name of the given model.
     */
    String getModelName(final String endpoint);

}
