package de.fhdw.hfp418ie.vo.graphql.converter;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.util.Pair;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;

import java.util.Map;

/**
 * A GraphQLConverter converts a given GraphQL schema into the internal model of the integration tool.
 */
public interface GraphQLConverter {

    String[] DEFAULT_SCALAR_TYPES = {"Boolean", "Int", "Float", "String", "ID"};
    String[] DEFAULT_GRAPHQL_TYPES = {"Query", "Mutation"};
    GraphQLConverter INSTANCE = GraphQLConverterImpl.getINSTANCE();

    /**
     * Converts a given GraphQL schema into the internal model of the integration tool.
     *
     * @param name     Name of the resulting internal model.
     * @param endpoint Endpoint of the resulting internal model.
     * @param schema   Given GraphQL schema which will be converted.
     * @return The resulting internal model and the mapping between the GraphQL types and the internal types.
     */
    Pair<Model, Map<GraphQLType, BaseType>> convert(String name, String endpoint, GraphQLSchema schema);
}
