package de.fhdw.hfp418ie.vo.graphql.integration;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilder;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilderImpl;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.FieldCorrespondenceImpl;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondence;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondenceImpl;
import de.fhdw.hfp418ie.vo.graphql.model.fields.BaseField;
import de.fhdw.hfp418ie.vo.graphql.model.fields.ElementBaseFieldImpl;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.MergedType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import de.fhdw.hfp418ie.vo.graphql.model.types.visitor.ModelBuilderMergedTypeVisitor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class IntegratorTests {

    private Integrator integrator;
    private Collection<Model> models;
    private Collection<ModelCorrespondence> correspondences;
    private ModelCorrespondence corr;
    private Type one;
    private Type two;
    private Type three;
    private Type four;
    private Type five;
    private Type six;
    private Type seven;
    private Type eight;
    private Type nine;

    @BeforeEach
    void setup() {
        this.integrator = new IntegratorImpl();
        ModelBuilder aBuilder = new ModelBuilderImpl("a", "demo");
        this.one = aBuilder.newType("1");
        this.two = aBuilder.newType("2");
        this.three = aBuilder.newType("3");
        Model a = aBuilder.buildInitial();

        ModelBuilder bBuilder = new ModelBuilderImpl("b", "demo");
        this.four = bBuilder.newType("4");
        this.five = bBuilder.newType("5");
        this.six = bBuilder.newType("6");
        Model b = bBuilder.buildInitial();

        ModelBuilder cBuilder = new ModelBuilderImpl("c", "demo");
        this.seven = cBuilder.newType("7");
        this.eight = cBuilder.newType("8");
        this.nine = cBuilder.newType("9");
        Model c = cBuilder.buildInitial();

        this.models = new ArrayList<>();
        this.models.add(a);
        this.models.add(b);
        this.models.add(c);

        this.correspondences = new ArrayList<>();
        Collection<Type> relatedTypes = new ArrayList<>();
        relatedTypes.add(this.one);
        relatedTypes.add(this.four);
        relatedTypes.add(this.seven);
        this.corr = new ModelCorrespondenceImpl(relatedTypes);
        this.correspondences.add(this.corr);
        relatedTypes = new ArrayList<>();
        relatedTypes.add(this.one);
        relatedTypes.add(this.five);
        relatedTypes.add(this.eight);
        this.correspondences.add(new ModelCorrespondenceImpl(relatedTypes));
        relatedTypes = new ArrayList<>();
        relatedTypes.add(this.three);
        relatedTypes.add(this.six);
        relatedTypes.add(this.nine);
        this.correspondences.add(new ModelCorrespondenceImpl(relatedTypes));
    }

    @Test
    @DisplayName("Test with three Models and overlapping correspondences")
    void testCorrespondences() {
        Model result = this.integrator.calculateModelIntegration("Integrated", "doesnt matter in this test",
                models, correspondences);

        MergedType type14758 = TypeFactory.FACTORY.createMergedType(new ArrayList<>(), null);
        Collection<Type> subtypes = new ArrayList<>();
        subtypes.add(this.one);
        subtypes.add(this.four);
        subtypes.add(this.seven);
        subtypes.add(this.five);
        subtypes.add(this.eight);
        type14758.addTypes(subtypes);
        type14758.accept(new ModelBuilderMergedTypeVisitor(result));
        subtypes.clear();

        MergedType type369 = TypeFactory.FACTORY.createMergedType(new ArrayList<>(), null);
        subtypes.add(this.three);
        subtypes.add(this.six);
        subtypes.add(this.nine);
        type369.addTypes(subtypes);
        type369.accept(new ModelBuilderMergedTypeVisitor(result));

        Collection<Type> expected = new HashSet<>();
        expected.add(this.two);
        expected.add(type14758);
        expected.add(type369);
        assertEquals(expected, result.getTypes());
    }

    @Test
    @DisplayName("Test of merging with correspondences referencing incorrect types")
    void testNonCorrectMerge() {
        Type notContained = TypeFactory.FACTORY.createBaseType("notContained");
        Collection<Type> relatedTypes = new ArrayList<>();
        relatedTypes.add(this.three);
        relatedTypes.add(this.six);
        relatedTypes.add(this.nine);
        relatedTypes.add(notContained);
        ModelCorrespondence modelCorrespondence = new ModelCorrespondenceImpl(relatedTypes);
        this.correspondences.add(modelCorrespondence);
        assertThrows(Error.class, () -> this.integrator.calculateModelIntegration(
                "name", "endpoint", this.models, this.correspondences));
    }

    @Test
    @DisplayName("Test of merging fields with correspondences")
    void testFieldMerge() {
        BaseField field1 = new ElementBaseFieldImpl((BaseType) this.one);
        field1.setName("field 1");
        field1.setResultType(this.three);
        this.one.getFields().add(field1);

        BaseField unmergedField = new ElementBaseFieldImpl((BaseType) this.one);
        unmergedField.setName("unmerged");
        unmergedField.setResultType(this.three);
        this.one.getFields().add(unmergedField);

        BaseField field2 = new ElementBaseFieldImpl((BaseType) this.four);
        field2.setName("field 2");
        field2.setResultType(this.six);
        this.four.getFields().add(field2);

        FieldCorrespondence fieldCorrespondence = new FieldCorrespondenceImpl("mergedField");
        fieldCorrespondence.getRelatedFields().add(field1);
        fieldCorrespondence.getRelatedFields().add(field2);
        this.corr.getFieldCorrespondences().add(fieldCorrespondence);

        Model result = this.integrator.calculateModelIntegration("Integrated", "doesnt matter in this test",
                models, correspondences);
        Optional<Type> newOne = result.getTypes().stream().filter(type -> type.getBaseTypes().contains(this.one)).findAny();
        if (!newOne.isPresent()) {
            fail("Type one was not found in merged model!");
        }
        assertTrue(newOne.get().getFields().stream().anyMatch(field -> field.getName().equals("mergedField")));
        assertFalse(newOne.get().getFields().stream().anyMatch(field -> field.getName().equals("field 1")));
        assertFalse(newOne.get().getFields().stream().anyMatch(field -> field.getName().equals("field 2")));
        assertTrue(newOne.get().getFields().stream().anyMatch(field -> field.getName().equals("unmerged")));
    }

}
