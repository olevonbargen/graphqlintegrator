package de.fhdw.hfp418ie.vo.graphql.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairTests {

    private Pair<String, Integer> pair1;
    private Pair<Integer, String> pair2;
    private Pair<String, Integer> pair3;
    private Pair<String, Integer> pair4;

    @BeforeEach
    void setup() {
        this.pair1 = new Pair<>("Hello", 42);
        this.pair2 = new Pair<>(42, "Hello");
        this.pair3 = new Pair<>("Hello", 42);
        this.pair4 = new Pair<>("Hello", 43);
    }

    @Test
    @DisplayName("Test of getting first element from pairs")
    void getFirstTest() {
        assertEquals("Hello", this.pair1.getFirst());
        assertEquals(42, this.pair2.getFirst());
        assertEquals("Hello", this.pair3.getFirst());
    }

    @Test
    @DisplayName("Test of getting second element from pairs")
    void getSecondTest() {
        assertEquals(42, this.pair1.getSecond());
        assertEquals("Hello", this.pair2.getSecond());
        assertEquals(42, this.pair3.getSecond());
    }

    @Test
    @DisplayName("Test of equals from pairs")
    void equalsTest() {
        assertEquals(this.pair1, this.pair1);
        assertFalse(this.pair2.equals(null));
        assertFalse(this.pair3.equals("no pair"));
        assertNotEquals(this.pair1, this.pair2);
        assertEquals(this.pair1, this.pair3);
        assertNotEquals(this.pair1, this.pair4);
    }

    @Test
    @DisplayName("Test of hash codes from pairs")
    void hashCodeTest() {
        assertEquals(this.pair1.hashCode(), this.pair1.hashCode());
        assertEquals(this.pair1.hashCode(), this.pair3.hashCode());
    }

    @Test
    @DisplayName("Test of toString from pairs")
    void toStringTest() {
        assertEquals(this.pair1.toString(), this.pair1.toString());
        assertEquals(this.pair1.toString(), this.pair3.toString());
    }

}
