package de.fhdw.hfp418ie.vo.graphql.model.builder;

import de.fhdw.hfp418ie.vo.graphql.model.fields.ElementBaseFieldImpl;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModelBuilderTests {

    private ModelBuilder builder1;
    private ModelBuilder builder2;
    private ModelBuilder builder3;
    private ModelBuilder builder4;

    @BeforeEach
    void setup() {
        this.builder1 = new ModelBuilderImpl("name", "endpoint");
        this.builder2 = new ModelBuilderImpl("name", "another endpoint");
        this.builder3 = new ModelBuilderImpl("name", "endpoint");
        this.builder4 = new ModelBuilderImpl("another name", "endpoint");
    }

    @Test
    @DisplayName("Test invalid field build")
    void testMissingTypeForField() {
        BaseType baseType = TypeFactory.FACTORY.createBaseType("baseType");
        Type missingType = TypeFactory.FACTORY.createBaseType("missing");
        Field field = new ElementBaseFieldImpl(baseType);
        field.setResultType(missingType);
        baseType.addField(field);
        this.builder1.addType(baseType);
        assertThrows(Error.class, this.builder1::buildMerged);
    }

    @Test
    @DisplayName("Test equals of model builders")
    void testEquals() {
        assertEquals(this.builder1, this.builder1);
        assertFalse(this.builder1.equals(null));
        assertFalse(this.builder1.equals("no model builder"));
        assertNotEquals(this.builder1, this.builder2);
        assertEquals(this.builder1, this.builder3);
        assertNotEquals(this.builder1, this.builder4);
        this.builder3.addType(TypeFactory.FACTORY.createBaseType("type"));
        assertNotEquals(this.builder1, this.builder3);
    }

    @Test
    @DisplayName("Test hash codes of model builders")
    void testHashCode() {
        assertEquals(this.builder1.hashCode(), this.builder1.hashCode());
        assertEquals(this.builder1.hashCode(), this.builder3.hashCode());
    }

    @Test
    @DisplayName("Test toString of model builders")
    void testToString() {
        assertEquals(this.builder1.toString(), this.builder1.toString());
        assertEquals(this.builder1.toString(), this.builder3.toString());
        assertNotEquals(this.builder1.toString(), this.builder2.toString());
    }
}
