package de.fhdw.hfp418ie.vo.graphql.generator.endpoint;

import de.fhdw.hfp418ie.vo.graphql.GraphQLMergeFacade;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.util.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class EndpointGeneratorTests {

    GraphQLMergeFacade facade;
    EndpointGenerator generator;
    File directory;

    @BeforeEach
    void setup() {
        this.facade = new GraphQLMergeFacade();
        this.generator = new JavaSpringEndpointGenerator();
        this.directory = new File("generatorTests/");
        this.directory.mkdir();
    }

    @AfterEach
    void cleanup() {
        this.delete(this.directory.getPath());
    }

    @Test
    void generateSimple() {
        try {
            Pair<Model, String> inputs = this.facade.calculateGraphQLSchemaAfterMerge(
                    "examples/correspondence/PersonEmployee.corr", true);
            this.generator.generateEndpoint(this.directory.getAbsolutePath(), inputs.getSecond(), inputs.getFirst(),
                    "com.demo.graphql", "Integration");
        } catch (IOException e) {
            fail("Could not generate the endpoint during the test", e);
        }
        File currentRoot = new File(this.directory.getPath() + "/Integration");
        assertEquals(3, currentRoot.list().length);
        currentRoot = new File(currentRoot.getPath() + "/src");
        assertTrue(currentRoot.isDirectory());
        currentRoot = new File(currentRoot.getPath() + "/main/java/com/demo/graphql");
        assertTrue(currentRoot.isDirectory());
        assertEquals(4, currentRoot.list().length);
    }

    private void delete(final String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            for (String child : file.list()) {
                this.delete(new File(file.getPath() + "/" + child).getPath());
            }
        }
        file.delete();
    }

}
