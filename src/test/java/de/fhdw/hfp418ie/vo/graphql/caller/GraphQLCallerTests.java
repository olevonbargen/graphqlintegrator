package de.fhdw.hfp418ie.vo.graphql.caller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhdw.hfp418ie.vo.graphql.demo.Demo;
import de.fhdw.hfp418ie.vo.graphql.expression.ConcatenationExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.Expression;
import de.fhdw.hfp418ie.vo.graphql.expression.StringVariable;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputModel;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static graphql.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class GraphQLCallerTests {

    private GraphQLCaller caller;
    public GraphQLCallerTests() {
        GraphQLCallerFactory.initializeForTest();
        caller = GraphQLCallerFactory.getInstance().create();
    }

    private static final String url = "http://localhost:4015/graphql/";
    private static final String url2 = "http://localhost:4010/";
    private static final String query = "query Demo { info }";

    @Test
    @DisplayName("Test graphql introspection call")
    void testGraphQLIntrospection() {
        try {
            GraphQLSchema schema = caller.getGraphQLSchema(url);
            Collection<String> types = schema.getAllTypesAsList().stream()
                    .map(GraphQLType::getName)
                    .filter(name -> !name.startsWith(GraphQLCaller.GRAPHQL_META_INFO_PREFIX))
                    .collect(Collectors.toSet());

            Collection<String> expected = new HashSet<>();
            expected.add("Query");
            expected.add("Mutation");
            expected.add("Person");
            expected.add("String");
            expected.add("Int");
            expected.add("Boolean");
            expected.add("Float");
            expected.add("ID");

            assertEquals(expected, types);
        } catch (IOException e) {
            fail("Introspection could not be executed!", e);
        }
    }

    @Test
    @DisplayName("Test demo graphql query call")
    void testGraphQLQuery() {
        try {
            final JsonNode response = caller.executeQuery(url, query);
            final ObjectMapper mapper = new ObjectMapper();
            Demo result = mapper.readValue(response.toString(), Demo.class);

            Demo expected = new Demo();
            expected.setInfo("Hello World!");

            assertEquals(expected, result);
            assertEquals(expected.hashCode(), result.hashCode());
        } catch (IOException e) {
            fail("Demo query could not be executed!", e);
        }
    }

    @Test
    @DisplayName("Test demo graphql query call against another endpoint")
    void testGraphQLQuery2() {
        try {
            final JsonNode response = caller.executeQuery(url2, query);
            final ObjectMapper mapper = new ObjectMapper();
            Demo result = mapper.readValue(response.toString(), Demo.class);

            Demo expected = new Demo();
            expected.setInfo("Hello JavaScript!");

            assertEquals(expected, result);
        } catch (IOException e) {
            fail("Demo query could not be executed!", e);
        }
    }

    @Test
    @DisplayName("Test with incorrect endpoint and correct query")
    void testWrongEndpoint() {
        Assertions.assertThrows(IOException.class, () ->
                caller.getGraphQLSchema("http://nonExistingEndpoint.com:1234/graphql"));
    }

    @Test
    @DisplayName("Test with incorrect query for correct endpoint")
    void testWrongQuery() {
        Assertions.assertThrows(IOException.class, () ->
                caller.executeQuery(url, "query Demo { fieldIsNotAvailable }"));
    }

    @Test
    @DisplayName("Test with incorrect query for another correct endpoint")
    void testWrongQuery2() {
        Assertions.assertThrows(IOException.class, () ->
                caller.executeQuery(url2, "query Demo { fieldIsNotAvailable }"));
    }

    @Test
    @DisplayName("Test for query generation of complex types")
    void testGenerateQuery() {
        Set<String> selectionSet = new HashSet<>();
        selectionSet.add("partner/hrModel_id");
        selectionSet.add("partner/salesModel_id");
        selectionSet.add("partner/salesModel_name");
        selectionSet.add("partner/hrModel_name");
        selectionSet.add("partner/address");
        selectionSet.add("partner");
        selectionSet.add("department");

        final String expected =
                "query{ \n" +
                        "employees{ \n" +
                        "partner{ \n" +
                        "name\n" +
                        "id\n" +
                        "}\n" +
                        "department\n" +
                        "}\n" +
                        "}";

        Map<String, String> queries = caller.generateQueries("Query", "employees", new HashMap<>(), selectionSet);

        assertEquals(1, queries.keySet().size());
        assertEquals("http://localhost:4012/", queries.keySet().iterator().next());
        assertEquals(expected, queries.values().iterator().next());
    }

    @Test
    @DisplayName("Test for query generation for scalar result types")
    void testSimpleQueryGeneration() {
        Set<String> selectionSet = new HashSet<>();

        final String expected = "query{ \n" +
                "countCustomer\n" +
                "}";

        Map<String, String> queries = caller.generateQueries("Query", "countCustomer", new HashMap<>(), selectionSet);

        assertEquals(1, queries.keySet().size());
        assertEquals(expected, queries.values().iterator().next());
    }

    @Test
    @DisplayName("Isolated test of importing json expressions")
    void testSimpleExpressionImport() {
        final String configPath = "examples/config/expressions.json";
        URL path = this.getClass().getClassLoader().getResource(configPath);
        if (path == null) {
            throw new Error("Could not find expression configuration at " + configPath);
        }
        File file = new File(path.getFile());
        ObjectMapper mapper = new ObjectMapper();
        final Collection<Expression> result = new ArrayList<>();
        try {
            JsonNode expressionsNode = mapper.readTree(file);
            Iterator<JsonNode> expressions = expressionsNode.elements();
            while (expressions.hasNext()) {
                result.add(mapper.readValue(expressions.next().toString(), Expression.class));
            }
        } catch (IOException e) {
            fail("Could not load expressions at " + configPath, e);
        }
        assertEquals(4, result.size());
    }

    @Test
    @DisplayName("Test of import from config with expressions")
    void testExpressionImport() {
        Collection<OutputModel> models = this.loadModels();
        Optional<OutputModel> searchModel = models.stream().filter(current -> current.getName().equals("salesModel"))
                .findAny();
        if (!searchModel.isPresent()) {
            fail("Could not find imported model salesModel");
        }
        OutputModel model = searchModel.get();
        Optional<OutputType> searchType = model.getTypes().stream().filter(type -> type.getName().equals("Query"))
                .findAny();
        if (!searchType.isPresent()) {
            fail("Could not find imported type Query within imported model salesModel");
        }
        OutputType type = searchType.get();
        EqualityExpression result = type.getEqualityExpression();
        assertEquals(2, result.getExpressions().size());
        Iterator<Expression> expressions = result.getExpressions().iterator();
        Expression first = expressions.next();
        assertEquals(ConcatenationExpression.class, first.getClass());
        assertEquals(StringVariable.class, expressions.next().getClass());
    }

    @Test
    @DisplayName("Test translation with IDs not explicitly queried in first level")
    void testAdditionIDsSimple() {
        Set<String> selection = new HashSet<>();
        selection.add("salesModel_date");
        selection.add("customer");
        selection.add("customer/salesModel_name");
        Map<String, String> queries = new GraphQLCallerImpl().generateQueries("Query", "purchases",
                new HashMap<>(), selection);
        assertEquals(1, queries.keySet().size());
        assertTrue(queries.values().iterator().next().contains("id" + System.lineSeparator()));
    }

    private Collection<OutputModel> loadModels() {
        final String pathToConfig = "examples/config/modelsExpressions.json";
        URL path = this.getClass().getClassLoader().getResource(pathToConfig);
        if (path == null) {
            throw new Error("Could not find model configuration at " + pathToConfig);
        }
        File file = new File(path.getFile());
        ObjectMapper mapper = new ObjectMapper();
        final Collection<OutputModel> models = new ArrayList<>();
        try {
            JsonNode modelsJson = mapper.readTree(file);
            modelsJson.elements().forEachRemaining(node -> {
                try {
                    models.add(mapper.readValue(node.toString(), OutputModel.class));
                } catch (IOException e) {
                    throw new Error("Could not load model configuration at " + path.toString(), e);
                }
            });
        } catch (IOException e) {
            throw new Error("Could not load model configuration at " + path.toString(), e);
        }
        return models;
    }

}
