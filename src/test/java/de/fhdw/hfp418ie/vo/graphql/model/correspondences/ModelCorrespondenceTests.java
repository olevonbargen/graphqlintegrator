package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ModelCorrespondenceTests {

    private ModelCorrespondence corr1;
    private ModelCorrespondence corr2;
    private ModelCorrespondence corr3;
    private ModelCorrespondence corr4;
    private Type type;

    @BeforeEach
    void setup() {
        this.corr1 = new ModelCorrespondenceImpl(new ArrayList<>());
        this.corr2 = new ModelCorrespondenceImpl(new ArrayList<>());
        this.corr3 = new ModelCorrespondenceImpl("name", new ArrayList<>());
        this.corr4 = new ModelCorrespondenceImpl("name", new ArrayList<>());
        this.type = TypeFactory.FACTORY.createBaseType("type");
    }

    @Test
    @DisplayName("Test equals of model correspondences")
    void testEquals() {
        assertEquals(this.corr1, this.corr1);
        assertFalse(this.corr1.equals(null));
        assertFalse(this.corr1.equals(this.type));
        assertEquals(this.corr1, this.corr2);
        assertFalse(this.corr1.equals(this.corr3));
        assertEquals(this.corr3, this.corr4);
        this.corr3.getFieldCorrespondences().add(new FieldCorrespondenceImpl());
        assertFalse(this.corr4.equals(this.corr3));
        this.corr1.getRelatedTypes().add(this.type);
        assertNotEquals(this.corr1, this.corr2);
    }

    @Test
    @DisplayName("Test hashcodes of model correspondences")
    void testHashCode() {
        assertEquals(this.corr1.hashCode(), this.corr2.hashCode());
        this.corr1.getRelatedTypes().add(this.type);
        this.corr2.getRelatedTypes().add(this.type);
        assertEquals(this.corr1.hashCode(), this.corr2.hashCode());
    }

    @Test
    @DisplayName("Test toString of model correspondences")
    void testToString() {
        assertEquals(this.corr1.toString(), this.corr2.toString());
    }

}
