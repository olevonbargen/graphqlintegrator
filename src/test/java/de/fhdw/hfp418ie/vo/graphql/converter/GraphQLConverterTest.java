package de.fhdw.hfp418ie.vo.graphql.converter;

import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCallerFactory;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilder;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilderImpl;
import de.fhdw.hfp418ie.vo.graphql.model.fields.Field;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import graphql.schema.GraphQLSchema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class GraphQLConverterTest {

    private static final String url = "http://localhost:4015/graphql/";

    private GraphQLConverter converter;
    private GraphQLSchema schema;

    @BeforeEach
    void setup() {
        GraphQLCallerFactory.initializeForTest();
        this.schema = null;
        this.converter = GraphQLConverter.INSTANCE;
        try {
            this.schema = GraphQLCallerFactory.getInstance().create().getGraphQLSchema(url);
        } catch (IOException ioe) {
            fail("Schema from endpoint " + url + " could not be established", ioe);
        }
    }

    @Test
    @DisplayName("Test of conversion from a simple GraphQL schema")
    void testConverter() {
        Model result = this.converter.convert("name", "endpoint", this.schema).getFirst();

        ModelBuilder expectedBuilder = new ModelBuilderImpl("name", "endpoint");
        expectedBuilder.newType("Query");
        expectedBuilder.newType("Mutation");
        expectedBuilder.newType("Person");
        expectedBuilder.newType("String");
        expectedBuilder.newType("Int");
        expectedBuilder.newType("Boolean");
        expectedBuilder.newType("Float");
        expectedBuilder.newType("ID");

        Model expected = expectedBuilder.buildInitial();

        Optional<Type> mutationSearch = result.getTypes().stream().filter(type -> type.getName().equals("Mutation")).findAny();
        if (!mutationSearch.isPresent()) {
            fail("There is no converted mutation type!");
        }
        Type mutation = mutationSearch.get();
        Optional<Field> newSearch = mutation.getFields().stream().filter(field -> field.getName().equals("newPerson")).findAny();
        if (!newSearch.isPresent()) {
            fail("Mutation newPerson was not converted!");
        }
        Field newPerson = newSearch.get();

        assertEquals(expected.getTypes(), result.getTypes());
        assertEquals(3, mutation.getFields().size());
        assertEquals(2, newPerson.getArguments().size());
    }

    @Test
    @DisplayName("Test of singleton GraphQL converter")
    void testSingletonConverter() {
        GraphQLConverter converter = GraphQLConverterImpl.getINSTANCE();
        assertEquals(converter, GraphQLConverterImpl.getINSTANCE());
    }

}
