package de.fhdw.hfp418ie.vo.graphql.demo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class DemoTests {

    @Test
    @DisplayName("Test of equals from demo")
    void testEquals() {
        Demo demo = new Demo();
        assertEquals(demo, demo);
        assertFalse(demo.equals("no demo"));
        assertFalse(demo.equals(null));
    }

}
