package de.fhdw.hfp418ie.vo.graphql.parser;

import de.fhdw.hfp418ie.vo.graphql.expression.ConcatenationExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.EqualityExpression;
import de.fhdw.hfp418ie.vo.graphql.expression.StringVariable;
import de.fhdw.hfp418ie.vo.graphql.model.correspondences.ModelCorrespondence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class ExpressionParserTests {

    private ModelCorrespondence partner1;
    private ModelCorrespondence partner2;

    @BeforeEach
    void readCorrespondences() {
        DefaultCorrespondenceListener parser1 = new DefaultCorrespondenceListener(
                "examples/correspondence/PersonEmployeeExpressions1.corr",true);
        DefaultCorrespondenceListener parser2 = new DefaultCorrespondenceListener(
                "examples/correspondence/PersonEmployeeExpressions2.corr",true);
        try {
            parser1.parse();
            parser2.parse();
        } catch (IOException e) {
            fail("Could not load the example correspondences!", e);
        }
        Optional<ModelCorrespondence> partner = parser1.getCorrespondences().stream()
                .filter(corr -> corr.getName().equals("Partner")).findAny();
        if (!partner.isPresent()) {
            fail("Model Correspondence Partner was defined in Correspondence file but not found by parser!");
        }
        this.partner1 = partner.get();
        partner = parser2.getCorrespondences().stream()
                .filter(corr -> corr.getName().equals("Partner")).findAny();
        if (!partner.isPresent()) {
            fail("Model Correspondence Partner was defined in Correspondence file but not found by parser!");
        }
        this.partner2 = partner.get();
    }

    @Test
    @DisplayName("Reading parsed simple expression for identification of same instances")
    void readExpressions() {
        assertEquals(1, this.partner1.getFieldCorrespondences().size());
        EqualityExpression equalityExpression = this.partner1.getEqualityExpression();
        assertNotNull(equalityExpression);
        assertEquals(2, equalityExpression.getExpressions().size());
        assertEquals(StringVariable.class, equalityExpression.getExpressions().iterator().next().getClass());
    }

    @Test
    @DisplayName("Reading parsed complex expression for identification of same instances")
    void readComplexExpressions() {
        assertEquals(1, this.partner2.getFieldCorrespondences().size());
        EqualityExpression equalityExpression = this.partner2.getEqualityExpression();
        assertNotNull(equalityExpression);
        assertEquals(2, equalityExpression.getExpressions().size());
        assertEquals(ConcatenationExpression.class, equalityExpression.getExpressions().iterator().next().getClass());
    }

}
