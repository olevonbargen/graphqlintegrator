package de.fhdw.hfp418ie.vo.graphql.model.types;

import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilderImpl;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TypesTests {

    private BaseType baseType;
    private MergedType mergedType;

    @BeforeEach
    void setup() {
        this.baseType = TypeFactory.FACTORY.createBaseType("baseType");
        this.mergedType = TypeFactory.FACTORY.createMergedType("merged", new ArrayList<>(), null);
    }

    @Test
    @DisplayName("Equals of base types")
    void testEqualsBaseType() {
        assertFalse(this.baseType.equals(null));
        assertFalse(this.baseType.equals(this.mergedType));
    }

    @Test
    @DisplayName("Equals of merged types")
    void testEqualsMergedType() {
        assertEquals(this.mergedType, this.mergedType);
        assertFalse(this.mergedType.equals(null));
        assertFalse(this.mergedType.equals(this.baseType));

        Model model = new ModelBuilderImpl("name", "endpoint").buildInitial();
        this.mergedType.setModel(model);
        MergedType type = TypeFactory.FACTORY.createMergedType("merged", new ArrayList<>(), null);
        assertNotEquals(this.mergedType, type);

        type.setModel(model);
        type.getTypes().add(this.baseType);
        assertNotEquals(this.mergedType, type);

        type.setName("changed");
        this.mergedType.getTypes().add(this.baseType);
        assertNotEquals(this.mergedType, type);
    }

    @Test
    void testToStringMerged() {
        assertEquals("MergedType under construction", this.mergedType.toString());

        Model model = new ModelBuilderImpl("name", "endpoint").buildInitial();
        this.mergedType.setModel(model);
        assertEquals("name.merged", this.mergedType.toString());
    }


}
