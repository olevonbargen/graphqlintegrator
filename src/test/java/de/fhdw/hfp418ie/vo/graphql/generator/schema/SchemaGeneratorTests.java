package de.fhdw.hfp418ie.vo.graphql.generator.schema;

import de.fhdw.hfp418ie.vo.graphql.GraphQLMergeFacade;
import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCaller;
import de.fhdw.hfp418ie.vo.graphql.caller.GraphQLCallerFactory;
import de.fhdw.hfp418ie.vo.graphql.converter.GraphQLConverter;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.util.Pair;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class SchemaGeneratorTests {

    private static final String endpoint = "http://localhost:4015/graphql/";

    private Model simpleModel;
    private SchemaGenerator generator;

    @BeforeEach
    void setup() {
        GraphQLCallerFactory.initializeForTest();
        this.generator = SchemaGenerator.DEFAULT_GENERATOR;
        try {
            this.simpleModel = GraphQLConverter.INSTANCE.convert("name", "endpoint",
                    GraphQLCallerFactory.getInstance().create().getGraphQLSchema(endpoint)).getFirst();
        } catch (IOException e) {
            fail("Model from endpoint " + endpoint + " could not be loaded.");
        }
    }

    @Test
    @DisplayName("Simple GraphQL Schema generator test")
    void simpleTest() {
        TypeDefinitionRegistry result = new SchemaParser().parse(
                this.generator.generateSchemaDefinition(this.simpleModel));
        GraphQLSchema schema = new graphql.schema.idl.SchemaGenerator().makeExecutableSchema(result,
                RuntimeWiring.newRuntimeWiring().build());
        assertEquals(simpleModel.getTypes().size(), schema.getAllTypesAsList().stream()
                .filter(graphQLType -> !graphQLType.getName().startsWith("__")).count());
    }

    @Test
    @DisplayName("Generator test with a merged type")
    void mergedTest() {
        GraphQLMergeFacade facade = new GraphQLMergeFacade();
        Pair<Model, String> result = null;
        try {
            result = facade.calculateGraphQLSchemaAfterMerge("examples/correspondence/PersonEmployee.corr", true);
        } catch (IOException ioe) {
            fail("Could not load example correspondences", ioe);
        }
        TypeDefinitionRegistry registry = new SchemaParser().parse(result.getSecond());
        GraphQLSchema schema = new graphql.schema.idl.SchemaGenerator().makeExecutableSchema(registry,
                RuntimeWiring.newRuntimeWiring().build());
        assertEquals(result.getFirst().getTypes().size(), schema.getAllTypesAsList().stream()
                .filter(graphQLType -> !graphQLType.getName().startsWith("__")).count());
    }

}
