package de.fhdw.hfp418ie.vo.graphql.model.fields;

import de.fhdw.hfp418ie.vo.graphql.model.types.BaseType;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class FieldsTests {

    private Field field1;
    private Field field2;

    @BeforeEach
    void setup() {
        BaseType type = TypeFactory.FACTORY.createBaseType("type");
        this.field1 = new ElementBaseFieldImpl(type);
        this.field2 = new ElementBaseFieldImpl(type);
    }

    @Test
    @DisplayName("Test equals of fields")
    void testEquals() {
        assertEquals(this.field1, this.field1);
        assertEquals(this.field1, this.field2);
        assertFalse(this.field1.equals(null));
        assertFalse(this.field2.equals("no field"));
    }

}
