package de.fhdw.hfp418ie.vo.graphql.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhdw.hfp418ie.vo.graphql.GraphQLMergeFacade;
import de.fhdw.hfp418ie.vo.graphql.model.builder.ModelBuilderImpl;
import de.fhdw.hfp418ie.vo.graphql.model.output.OutputModel;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactory;
import de.fhdw.hfp418ie.vo.graphql.model.types.factory.TypeFactoryImpl;
import de.fhdw.hfp418ie.vo.graphql.util.Pair;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class ModelTests {

    @Test
    @DisplayName("Test singleton type factory")
    void testTypeFactory() {
        TypeFactory factory = TypeFactoryImpl.getInstance();
        assertEquals(factory, TypeFactoryImpl.getInstance());
    }

    @Test
    @DisplayName("Test of equals from models")
    void testModelEquals() {
        Model model = new ModelBuilderImpl("name", "endpoint").buildInitial();
        Model model2 = new ModelBuilderImpl("name", "endpoint2").buildInitial();
        assertEquals(model, model);
        assertFalse(model.equals("no model"));
        assertFalse(model.equals(null));
        assertNotEquals(model, model2);
    }

    @Test
    @DisplayName("Test of output generation")
    void testOutput() {
        GraphQLMergeFacade facade = new GraphQLMergeFacade();
        Pair<Model, String> pair = null;
        try {
            pair = facade.calculateGraphQLSchemaAfterMerge("examples/correspondence/PersonEmployee.corr", true);
        } catch (IOException e) {
            fail(e);
        }
        Collection<OutputModel> model = pair.getFirst().generateOutput();
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("demoModel.json");
        try {
            mapper.writeValue(file, model);
        } catch (IOException e) {
            fail(e);
        }
        Collection<OutputModel> result = null;
        try {
            result = Arrays.asList(mapper.readValue(file, OutputModel[].class));
        } catch (IOException e) {
            fail(e);
        }
        assertEquals(result.size(), model.size());
        file.delete();
    }

}
