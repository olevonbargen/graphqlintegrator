package de.fhdw.hfp418ie.vo.graphql.model.correspondences;

import de.fhdw.hfp418ie.vo.graphql.model.fields.ElementBaseFieldImpl;
import de.fhdw.hfp418ie.vo.graphql.model.types.BaseTypeImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class FieldCorrespondenceTests {

    FieldCorrespondence corr1;
    FieldCorrespondence corr2;
    FieldCorrespondence corr3;

    @BeforeEach
    void setup() {
        this.corr1 = new FieldCorrespondenceImpl();
        this.corr2 = new FieldCorrespondenceImpl();
        this.corr3 = new FieldCorrespondenceImpl("name");
    }

    @Test
    @DisplayName("Test equals of field correspondences")
    void testEquals() {
        assertEquals(this.corr1, this.corr1);
        assertFalse(this.corr1.equals(null));
        assertFalse(this.corr1.equals(""));
        assertEquals(this.corr1, this.corr2);
        assertFalse(this.corr1.equals(this.corr3));
        this.corr2.getRelatedFields().add(new ElementBaseFieldImpl(new BaseTypeImpl("type")));
        this.corr3.getRelatedFields().add(new ElementBaseFieldImpl(new BaseTypeImpl("type")));
        assertFalse(this.corr1.equals(this.corr2));
        assertFalse(this.corr3.equals(this.corr2));
    }


    @Test
    @DisplayName("Test hashcodes of model correspondences")
    void testHashCode() {
        assertEquals(this.corr1.hashCode(), this.corr2.hashCode());
        this.corr1.getRelatedFields().add(new ElementBaseFieldImpl(new BaseTypeImpl("type")));
        this.corr2.getRelatedFields().add(new ElementBaseFieldImpl(new BaseTypeImpl("type")));
        assertEquals(this.corr1.hashCode(), this.corr2.hashCode());
    }

}
