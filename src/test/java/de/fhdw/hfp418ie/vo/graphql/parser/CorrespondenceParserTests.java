package de.fhdw.hfp418ie.vo.graphql.parser;

import de.fhdw.hfp418ie.vo.graphql.integration.Integrator;
import de.fhdw.hfp418ie.vo.graphql.integration.IntegratorImpl;
import de.fhdw.hfp418ie.vo.graphql.model.Model;
import de.fhdw.hfp418ie.vo.graphql.model.types.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class CorrespondenceParserTests {

    private CorrespondenceFileParser parser1;
    private CorrespondenceFileParser parser2;

    @BeforeEach
    void readCorrespondences() {
        try {
            this.parser1 = new DefaultCorrespondenceListener(
                    "examples/correspondence/PersonEmployee.corr", true);
            this.parser2 = new DefaultCorrespondenceListener(
                    "examples/correspondence/PersonEmployee2.corr", true);
            this.parser1.parse();
            this.parser2.parse();
        } catch (IOException e) {
            fail("Could not load the example correspondences!", e);
        }
    }

    @Test
    @DisplayName("Correlate two models with one correspondence with alias")
    void testSimpleCorrespondance() {
        assertEquals(2, this.parser1.getModels().size());
        assertEquals(7, this.parser1.getCorrespondences().size());

        Integrator integrator = new IntegratorImpl();
        Model mergedModel = integrator.calculateModelIntegration("Integrated", "endpoint",
                this.parser1.getModels(), this.parser1.getCorrespondences());

        assertEquals(8, mergedModel.getTypes().size());

        Optional<Type> found = mergedModel.getTypes().stream().filter(type -> type.getName().equals("Query")).findAny();
        if (!found.isPresent()) {
            fail("Found no query type in merged model");
        }
        final Type mergedQuery = found.get();

        found = mergedModel.getTypes().stream().filter(type -> type.getName().equals("Partner")).findAny();
        if (!found.isPresent()) {
            fail("Merged type Partner (with Correspondences) not found in merged model!");
        }
        final Type partnerType = found.get();

        assertEquals(8, mergedQuery.getFields().size());
        assertEquals(5, partnerType.getFields().size());
    }


    @Test
    @DisplayName("Correlate two models with one correspondence without alias")
    void testSimpleCorrespondanceWithoutAlias() {
        assertEquals(2, this.parser2.getModels().size());
        assertEquals(7, this.parser2.getCorrespondences().size());

        Integrator integrator = new IntegratorImpl();
        Model mergedModel = integrator.calculateModelIntegration("Integrated", "endpoint",
                this.parser2.getModels(), this.parser2.getCorrespondences());

        assertEquals(8, mergedModel.getTypes().size());

        Optional<Type> found = mergedModel.getTypes().stream().filter(type -> type.getName().equals("Query")).findAny();
        if (!found.isPresent()) {
            fail("Found no query type in merged model");
        }
        final Type mergedQuery = found.get();

        found = mergedModel.getTypes().stream().filter(type ->
                type.getName().equals("employeeModel.Employee_personModel.Person")).findAny();
        if (!found.isPresent()) {
            fail("Merged type Partner (with Correspondences) not found in merged model!");
        }
        final Type partnerType = found.get();

        assertEquals(8, mergedQuery.getFields().size());
        assertEquals(6, partnerType.getFields().size());
    }
}
