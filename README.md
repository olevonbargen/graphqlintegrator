# Integration tool for GraphQL endpoints

<img src="./icons/distributed.svg" width="230px" height="230px">

[![pipeline status](https://gitlab.com/olevonbargen/graphqlintegrator/badges/master/pipeline.svg)](https://gitlab.com/olevonbargen/graphqlintegrator/commits/master)
[![coverage report](https://gitlab.com/olevonbargen/graphqlintegrator/badges/master/coverage.svg)](https://gitlab.com/olevonbargen/graphqlintegrator/commits/master)

## Documentation
This tool was developed as prototype for the master thesis "Integration of Web Services and their data models with 
special regard to GraphQL" from Ole von Bargen.
Therefore this thesis contains most of the background information about the development of this tool.

The thesis was written as part of the study course Information Engineering at the Fachhochschule für die 
Wirtschaft Hannover (HFP418IE) during a semester abroad at the Høgskulen på Vestlandet in Bergen, Norway.

## Structure

The project is structured following the best practice of Gradle Java projects.
Within the src directory you can find the following three subdirectories.

- `demoEndpoints`: In this directory you can find three demo GraphQL endpoints, written in JavaScript. These endpoints 
are used for the demo described in the following Getting Started.
- `main`: Here you can find the implementation of the integration prototype.
- `test`: In this directory there are JUnit 5 test cases for automatic component tests.

## Initial setup

This tool gets developed with Java 8, JUnit 5 and gets build with Gradle.
We will show how to setup IntelliJ as IDE in the following steps. 
However you can use other IDEs like Eclipse as well with similar steps.

1. First of all you need to clone the repository by choosing a directory and executing the following command 
`git clone git@gitlab.com:olevonbargen/graphqlintegrator.git`
1. Now open IntelliJ and choose `File` - `New` - `Project from Existing Sources` and choose the top level 
directory of the cloned project `graphqlintegrator`.

1. During the import IntelliJ will recognize the project as Gradle Project which is correct.
    
1. After loading the project IntelliJ will build the project based on the gradle build file.

1. After the build you should be able to execute the JUnit test cases via `gradle check` from the project root. 
If all tests pass you successfully imported the project.

## Getting Started

This section requires the initial setup described above. 
Without the setup steps you will not be able to follow along with the next steps.

1. Start the demo endpoints from your project root
    1. `cd src/demoEndpoints/endpoint1` to switch to the first endpoints directory
    1. `npm i && npm start` to install the dependencies and start the first endpoint
    1. `cd ../endpoint2` to switch to the second endpoints directory 
    1. `npm i && npm start` to install the dependencies and start the second endpoint
    1. `cd ../endpoint3` to switch to the third endpoints directory
    1. `npm i && npm start` to install the dependencies and start the third endpoint
1. Edit the file `src/main/java/de/fhdw/hfp418ie/vo/graphql/caller/GraphQLCaller.java` with a text editor of your 
choice and change the field INSTANCE from `new GraphQLCallerMock()` to `new GraphQLCallerImpl()` to prevent using 
the mock which was designed solely for the JUnit test cases.
1. Edit the file `src/main/java/de/fhdw/hfp418ie/vo/graphql/Main.java` and specify within the field `pathForGeneration` 
a path of your choice which has to be writable for the application. If you like you can also specify a default root 
package for the generated application and a name for the generated project here.
1. Build the project by using gradle with `gradle assemble` within the project root
1. Run the demo by using gradle with `gradle run` within the project root.
1. Afterwards switch to the chosen path for the generation and switch to the directory with your project name within 
that path.
1. Execute `gradle bootRun` for starting the generated endpoint.
1. After the startup the console should show the line `Started Application in XXXXX seconds` - 
your endpoint is ready now!
1. Open a browser of your choice and go to `https://localhost:4011` to open the GraphQL Playground (an application 
from your JavaScript endpoints which enables you to easily send requests to a GraphQL endpoint).
1. Change the endpoint against which you are sending requests by setting it to `http://localhost:4020/graphql` 
next to the button `HISTORY`.
1. Check the current schema of your GraphQL endpoint by clicking on the button `SCHEMA` on the right side of the screen.
1. If the first entry shows the type `Employee` with the four fields `salary`, `department`, `id` and `partner` than 
you are connected with your generated integrated endpoint!
1. Write the following query in the left side of your screen: `query { endpointInfo }` and click the Play Icon in 
the middle.
1. If you see a response with two String (one from endpoint 1 and one from endpoint 2) you successfully queried the 
integrated endpoint with the delegation to two of the three underlying endpoints! 


<sub><sub>Icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></sub></sub>
